
#include "AnalyzeTEL.h"
#include "TRootanaEventLoop.hxx"

#include "TStyle.h"
 
AnalyzeTEL::AnalyzeTEL(): TCanvasHandleBase("TEL data")
{
#if 0
   fHtrigLat     = new TH1D("VTRtrigLat", "Trigger latency, usec", 100, 0, 100);
   fHreadoutTime = new TH1D("VTRreadTime", "Readout time, usec", 100, 0, 10000);
   fHbusyTime = new TH1D("VTRbusyTime", "Busy time, usec", 100, 0, 10000);
   fHddddTime = new TH1D("VTRddddTime", "Time between events, sec", 100, 0, 0.2);
   fGtrigLat  = new TGraph();
   fGbusyTime = new TGraph();

   fGtrigLat->SetTitle("Maximum Triggerlatency vs Event Number, usec");
   fGbusyTime->SetTitle("Maximum Busy time vs Event Number, usec");

   fFirstEventNo = 0;
   fMaxTrigLat   = 0;
   fMaxBusyTime  = 0;
#endif
}

AnalyzeTEL::~AnalyzeTEL()
{
   //
}

void AnalyzeTEL::SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display)
{
  // Now create my embedded canvas, along with the various buttons for this canvas.
}

/// Reset the histograms for this canvas
void AnalyzeTEL::ResetCanvasHistograms()
{
#if 0
   fHtrigLat->Reset();
   fHreadoutTime->Reset();
   fHbusyTime->Reset();
   fHddddTime->Reset();
   //fGtrigLat->Reset();
   //fGbusyTime->Reset();
#endif
}

extern bool gKludge1;

#define NUM_TEL 4

/// Update the histograms for this canvas.
void AnalyzeTEL::UpdateCanvasHistograms(TDataContainer& dataContainer)
{
   // FIXME get midas data bank here

   int size;
   uint16_t *tel_ptr;

   for (int itel = 0; itel < NUM_TEL; itel++) {
     char bankname[5];
     bankname[0] = 'T';
     bankname[1] = 'E';
     bankname[2] = 'L';
     bankname[3] = '0' + itel;
     bankname[4] = 0;

     size = dataContainer.GetMidasData().LocateBank(NULL, bankname, (void**)&tel_ptr);
     if (!tel_ptr) return;

     printf("Processing bank %s!\n", bankname);

     for (int i=0; i<16; i++) {
       printf("tel %d: 0x%04x\n", i, tel_ptr[i]);
     }
   }

#if 0
   uint32_t eventNo   = vtr_ptr[1];
   uint32_t trigTimeClk  = vtr_ptr[2];
   uint32_t beginTimeClk = vtr_ptr[3];
   uint32_t endTimeClk   = vtr_ptr[4];

   double freq = 20E6; // 20 MHz timestamp clock

   double trigLatency = (beginTimeClk - trigTimeClk)/freq;
   double readoutTime = (endTimeClk - beginTimeClk)/freq;
   double busyTime = (endTimeClk - trigTimeClk)/freq;

   double betweenEventsTime = (trigTimeClk - fPrevTrigTimeClk)/freq;

   printf("event %d, time %d, between %f, trig latency %f, readout time %f, busy time %f\n", eventNo, trigTimeClk, betweenEventsTime, trigLatency, readoutTime, busyTime);

   fHtrigLat->Fill(trigLatency*1E6);
   fHreadoutTime->Fill(readoutTime*1E6);
   fHbusyTime->Fill(busyTime*1E6);

   if (fFirstEventNo == 0) {
      fFirstEventNo = eventNo;
   }

   if (trigLatency > fMaxTrigLat) {
      fGtrigLat->SetPoint(fGtrigLat->GetN(), eventNo - fFirstEventNo, trigLatency*1E6);
      //printf("******* trigLatency %f -> %f, event %d, num %d\n", fMaxTrigLat, trigLatency, eventNo, fGtrigLat->GetN());
      fMaxTrigLat = trigLatency;
   }

   if (busyTime > fMaxBusyTime) {
      fGbusyTime->SetPoint(fGbusyTime->GetN(), eventNo - fFirstEventNo, busyTime*1E6);
      fMaxBusyTime = busyTime;
   }

   if (fPrevEventNo + 1 == eventNo)
      fHddddTime->Fill(betweenEventsTime);

   fPrevEventNo  = eventNo;
   fPrevTrigTimeClk = trigTimeClk;
#endif
}
  
/// Plot the histograms for this canvas
void AnalyzeTEL::PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas)
{
#if 0
  if (!gKludge1)
    return;

   TCanvas* c1 = embedCanvas->GetCanvas();
   gStyle->SetOptStat(111111);
   c1->Clear();
   c1->Divide(3,2);
   c1->cd(1);
   gPad->SetLogy(true);
   fHtrigLat->Draw();
   c1->cd(2);
   gPad->SetLogy(true);
   fHreadoutTime->Draw();
   c1->cd(3);
   gPad->SetLogy(true);
   fHbusyTime->Draw();
   c1->cd(4);
   //gPad->SetLogy(true);
   fGtrigLat->Draw("AL*");
   c1->cd(5);
   //gPad->SetLogy(true);
   fGbusyTime->Draw("AL*");
   c1->cd(6);
   gPad->SetLogy(true);
   fHddddTime->Draw();
   
   c1->Modified();
   c1->Update();
#endif
}
