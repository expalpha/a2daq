#ifndef ProcessTEL_h
#define ProcessTEL_h

#include "TH1D.h"
#include "TGraph.h"
#include "TCanvasHandleBase.hxx"

class AnalyzeTEL : public TCanvasHandleBase
{
public:
   AnalyzeTEL();
   ~AnalyzeTEL();

  /// Reset the histograms for this canvas
  void ResetCanvasHistograms();
  
  /// Update the histograms for this canvas.
  void UpdateCanvasHistograms(TDataContainer& dataContainer);
  
  /// Plot the histograms for this canvas
  void PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas);

  void SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display);

private:
};

#endif
