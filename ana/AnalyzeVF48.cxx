#include "AnalyzeVF48.h"

#include "TGNumberEntry.h"
#include <TGLabel.h>

#include "TRootanaEventLoop.hxx"

#define NUM_VF48 8
 
AnalyzeVF48::AnalyzeVF48(): TCanvasHandleBase("VF48"){

  fUnpVF48 = new UnpackVF48();
  fUnpVF48->SetNumModules(NUM_VF48);
  //fUnpVF48->SetModulesMask(0x1); // 1 modules

  int maxsamples = 0;

  for (int i=0; i<NUM_VF48; i++) {
    int v = 128; // TRootanaEventLoop::Get().GetODB()->odbReadInt("/Equipment/VME/Settings/VF48_NumSamples", i);
    printf("VF48 %d num samples %d\n", i, v);
    fUnpVF48->SetNumSamples(i, v);
    if (v > maxsamples)
      maxsamples = v;
  }

  for(int i = 0; i < kMaxWf; i++){
    char name[100];
    char title[100];
    sprintf(name,"vfw%i",i);
    sprintf(title,"Waveform %d",i);
    fWaveform[i] = new TH1D(name, title, maxsamples, 0, maxsamples);
  }
}

AnalyzeVF48::~AnalyzeVF48() {
   delete fUnpVF48;
   fUnpVF48 = NULL;
}

void AnalyzeVF48::SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display){


  // Now create my embedded canvas, along with the various buttons for this canvas.
  
  TGHorizontalFrame *labelframe = new TGHorizontalFrame(compFrame,200,40);
  
  fBankCounterButton = new TGNumberEntry(labelframe, 0, 9,999, TGNumberFormat::kNESInteger,
					      TGNumberFormat::kNEANonNegative, 
					      TGNumberFormat::kNELLimitMinMax,
					      0, 31);

  fBankCounterButton->Connect("ValueSet(Long_t)", "TRootanaDisplay", display, "UpdatePlotsAction()");
  fBankCounterButton->GetNumberEntry()->Connect("ReturnPressed()", "TRootanaDisplay", display, "UpdatePlotsAction()");
  labelframe->AddFrame(fBankCounterButton, new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5));
  TGLabel *labelMinicrate = new TGLabel(labelframe, "ADC Channel (0-31)");
  labelframe->AddFrame(labelMinicrate, new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5));

  compFrame->AddFrame(labelframe, new TGLayoutHints(kLHintsCenterX,2,2,2,2));


}



/// Reset the histograms for this canvas
void AnalyzeVF48::ResetCanvasHistograms(){

   for(int i = 0; i < kMaxWf; i++)
      fWaveform[i]->Reset();
}

void AnalyzeVF48::DrawWaveform(int iwf, VF48event* eve, int module, int chan)
{
   VF48module *m = eve->modules[module];
   if (m) {
      VF48channel *c = &m->channels[chan];
      int n = c->numSamples;
      if (n > 0) {
	fWaveform[iwf]->SetMinimum(0);
	fWaveform[iwf]->SetMaximum(1000);

         for (int i=0; i<n; i++) {
            int a = c->samples[i];
            fWaveform[iwf]->SetBinContent(i+1, a);
         }
      }
   }
}

bool gKludge1 = false;
  
/// Update the histograms for this canvas.
void AnalyzeVF48::UpdateCanvasHistograms(TDataContainer& dataContainer){

  gKludge1 = true;

  // FIXME get midas data bank here

   int size;
   void *vf48_ptr;

   for (int ivf48 = 0; ivf48 < NUM_VF48; ivf48++) {
     char bankname[5];
     bankname[0] = 'V';
     bankname[1] = 'F';
     bankname[2] = 'A';
     bankname[3] = '0' + ivf48;
     bankname[4] = 0;

     size = dataContainer.GetMidasData().LocateBank(NULL, bankname, &vf48_ptr);
     if (vf48_ptr)
       {
	 printf("Processing bank %s!\n", bankname);

	 if (0) {
	   uint32_t *u32 = (uint32_t*)vf48_ptr;
	   for (int i=0; i<20; i++) {
	     printf("dump %d: 0x%08x\n", i, u32[i]);
	   }
	 }
	 
	 fUnpVF48->UnpackStream(ivf48, vf48_ptr, size);

	 //gKludge1 = false;
       }
   }

   VF48event *vf48eve = fUnpVF48->GetEvent();
   if (vf48eve) {
      printf("ZZZ!\n");
      vf48eve->PrintSummary();

      for (int i=0; i<16; i++)
	DrawWaveform(i, vf48eve, 0, i);

      fWaveformUpdated = true;

      gKludge1 = false;

      delete vf48eve;
   }
}
  
/// Plot the histograms for this canvas
void AnalyzeVF48::PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas){

  //if (!gKludge1)
  //return;

  printf("AnalyzeVF48::PlotCanvas!\n");

  TCanvas* c1 = embedCanvas->GetCanvas();

  int whichbank = fBankCounterButton->GetNumberEntry()->GetIntNumber();

  if (whichbank == 16) {

    if (fWaveformUpdated) {
      printf("Draw begin...");
      fflush(stdout);
      c1->Clear();
      c1->Divide(4, 4);
      for (int i=0; i<16; i++) {
	c1->cd(i+1);
	fWaveform[i]->Draw();
      }
      fWaveformUpdated = false;
      c1->Modified();
      c1->Update();
      printf("Draw end\n");
      fflush(stdout);
    }
  } else if (whichbank >= 0 && whichbank < kMaxWf) {
    c1->Clear();
    fWaveform[whichbank]->Draw();
    c1->Modified();
    c1->Update();
  }
}
