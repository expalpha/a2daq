
#include "AnalyzeVTR.h"
#include "TRootanaEventLoop.hxx"

#include "TStyle.h"
 
AnalyzeVTR::AnalyzeVTR(): TCanvasHandleBase("VTR data")
{
   fHtrigLat     = new TH1D("VTRtrigLat", "Trigger latency, usec", 100, 0, 100);
   fHreadoutTime = new TH1D("VTRreadTime", "Readout time, usec", 100, 0, 10000);
   fHbusyTime = new TH1D("VTRbusyTime", "Busy time, usec", 100, 0, 10000);
   fHddddTime = new TH1D("VTRddddTime", "Time between events, sec", 100, 0, 0.2);
   fGtrigLat  = new TGraph();
   fGbusyTime = new TGraph();

   fGtrigLat->SetTitle("Maximum Triggerlatency vs Event Number, usec");
   fGbusyTime->SetTitle("Maximum Busy time vs Event Number, usec");

   fFirstEventNo = 0;
   fMaxTrigLat   = 0;
   fMaxBusyTime  = 0;
}

AnalyzeVTR::~AnalyzeVTR()
{
   //
}

void AnalyzeVTR::SetUpCompositeFrame(TGCompositeFrame *compFrame, TRootanaDisplay *display)
{
  // Now create my embedded canvas, along with the various buttons for this canvas.
}

/// Reset the histograms for this canvas
void AnalyzeVTR::ResetCanvasHistograms()
{
   fHtrigLat->Reset();
   fHreadoutTime->Reset();
   fHbusyTime->Reset();
   fHddddTime->Reset();
   //fGtrigLat->Reset();
   //fGbusyTime->Reset();
}

extern bool gKludge1;

/// Update the histograms for this canvas.
void AnalyzeVTR::UpdateCanvasHistograms(TDataContainer& dataContainer)
{
   // FIXME get midas data bank here

   int size;
   uint32_t *vtr_ptr;

   size = dataContainer.GetMidasData().LocateBank(NULL, "VTRI", (void**)&vtr_ptr);
   if (!vtr_ptr) return;

   printf("Processing bank VTRI!\n");

   uint32_t eventNo   = vtr_ptr[1];
   uint32_t trigTimeClk  = vtr_ptr[2];
   uint32_t beginTimeClk = vtr_ptr[3];
   uint32_t endTimeClk   = vtr_ptr[4];

   double freq = 20E6; // 20 MHz timestamp clock

   double trigLatency = (beginTimeClk - trigTimeClk)/freq;
   double readoutTime = (endTimeClk - beginTimeClk)/freq;
   double busyTime = (endTimeClk - trigTimeClk)/freq;

   double betweenEventsTime = (trigTimeClk - fPrevTrigTimeClk)/freq;

   printf("event %d, time %d, between %f, trig latency %f, readout time %f, busy time %f\n", eventNo, trigTimeClk, betweenEventsTime, trigLatency, readoutTime, busyTime);

   fHtrigLat->Fill(trigLatency*1E6);
   fHreadoutTime->Fill(readoutTime*1E6);
   fHbusyTime->Fill(busyTime*1E6);

   if (fFirstEventNo == 0) {
      fFirstEventNo = eventNo;
   }

   if (trigLatency > fMaxTrigLat) {
      fGtrigLat->SetPoint(fGtrigLat->GetN(), eventNo - fFirstEventNo, trigLatency*1E6);
      //printf("******* trigLatency %f -> %f, event %d, num %d\n", fMaxTrigLat, trigLatency, eventNo, fGtrigLat->GetN());
      fMaxTrigLat = trigLatency;
   }

   if (busyTime > fMaxBusyTime) {
      fGbusyTime->SetPoint(fGbusyTime->GetN(), eventNo - fFirstEventNo, busyTime*1E6);
      fMaxBusyTime = busyTime;
   }

   if (fPrevEventNo + 1 == eventNo)
      fHddddTime->Fill(betweenEventsTime);

   fPrevEventNo  = eventNo;
   fPrevTrigTimeClk = trigTimeClk;
}
  
/// Plot the histograms for this canvas
void AnalyzeVTR::PlotCanvas(TDataContainer& dataContainer, TRootEmbeddedCanvas *embedCanvas)
{
  printf("AnalyzeVTR::PlotCanvas!\n");

  if (!gKludge1)
    return;

   TCanvas* c1 = embedCanvas->GetCanvas();
   gStyle->SetOptStat(111111);
   c1->Clear();
   c1->Divide(3,2);
   c1->cd(1);
   gPad->SetLogy(true);
   fHtrigLat->Draw();
   c1->cd(2);
   gPad->SetLogy(true);
   fHreadoutTime->Draw();
   c1->cd(3);
   gPad->SetLogy(true);
   fHbusyTime->Draw();
   c1->cd(4);
   //gPad->SetLogy(true);
   fGtrigLat->Draw("AL*");
   c1->cd(5);
   //gPad->SetLogy(true);
   fGbusyTime->Draw("AL*");
   c1->cd(6);
   gPad->SetLogy(true);
   fHddddTime->Draw();
   
   c1->Modified();
   c1->Update();
}
