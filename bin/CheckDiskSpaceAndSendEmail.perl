#!/usr/bin/perl
# Available under BSD License. See url for more info:
# http://www.cyberciti.biz/tips/howto-write-perl-script-to-monitor-disk-space.html
use strict;
use warnings;
use Filesys::Df;

# file system to monitor
my $dir = "/home1";

# warning level
my $warning_level=5;

# email setup
my $to='alpha-admins@cern.ch';
#my $to='eoin.butler@cern.ch';
my $from='root@alphadaq.cern.ch';
my $subject='alphadaq -- Low Disk Space';

# get df
#my ($fs_type, $fs_desc, $used, $avail, $fused, $favail) = df $dir;
my $ref = df($dir);
my $used = $ref->{used};
my $avail = $ref->{bavail};


# calculate
my $df_free = (($avail) / ($avail+$used)) * 100.0;

# compare
if ($df_free < $warning_level) {
my $out = sprintf("WARNING Low Disk Space on $dir : %0.2f%%\n",$df_free);
printf("WARNING Low Disk Space on $dir  %0.2f%% \n",$df_free);

# send email using UNIX/Linux sendmail
open(MAIL, "|/usr/sbin/sendmail -t");
#open(MAIL, ">", "test");

## Mail Header
print MAIL "To: $to\n";
print MAIL "From: $from\n";
print MAIL "Subject: $out";

## Mail Body
print MAIL $out;

close(MAIL);
}
