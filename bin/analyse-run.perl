#!/usr/bin/perl -w

use Cwd 'abs_path';
#use File::chdir;

$nsls = "/bin/ls";

# Find target midas file
$runNumber = $ARGV[0];
$localMidasDirectory = ("/data3/alpha/current"); 
$serverMidasDirectory = ("/midasdata");

# first check if it's already in the remote directory
$serverMidasFile = "$serverMidasDirectory/run" .  $runNumber . ".mid.gz";
my $cmd = "ls $serverMidasFile";
system $cmd;
my $ls = `$cmd 2>&1`;

if ($ls =~ /No such file or directory/)
{
	print "Midas file for run $serverMidasFile not found ... \n" ;
	$serverMidasFile = "$serverMidasDirectory/run" .  $runNumber . "sub00000.mid.gz";
	$cmd = "ls $serverMidasFile";
	system $cmd;
	$ls = `$cmd 2>&1`;
	
	if ($ls =~ /No such file or directory/) # it's not on the remote filesystem. Check the local.
	{
		$targetMidasFile = $localMidasDirectory . "/run" . $runNumber . "sub00000.mid.gz";

		 $cmd = "ls $targetMidasFile";
		system $cmd;

		$ls = `$cmd 2>&1`;

		if ($ls =~ /No such file or directory/)
		{
			print "Midas file for run $targetMidasFile not found ... \n" ;

			$targetMidasFile = $localMidasDirectory . "/run" . $runNumber . ".mid.gz";
			$cmd = "ls $targetMidasFile";
			system $cmd;

			my $ls = `$cmd 2>&1`;
			
			if ($ls =~ /No such file or directory/)
			{
				print "Midas file for run $targetMidasFile not found ... \n" ;
				exit 0;
			}
			$serverMidasFile = "$serverMidasDirectory/run" .  $runNumber . ".mid.gz";
		}
		else
		{
			$serverMidasFile = "$serverMidasDirectory/run" .  $runNumber . "sub00000.mid.gz";
		}

		# Copy midas file to alphafrm02:/midasdata if necessary
		ServerCopy($targetMidasFile);
	}

}

# ONLY RUN ALPHASTRIPS IF THE FILE DOESN'T EXIST

$checkStripsFile = "/analysisdata/alphaStrips/alphaStrips" . $runNumber . "offline.root";
$cmd = "ls $checkStripsFile";
system $cmd;

my $ls = `$cmd 2>&1`;

if ($ls =~ /No such file or directory/)
{
	print "Strips file for run $checkStripsFile  not found ... \n" ;
	# Run alphaStrips
	my $cmd2 = "cd /home/alpha/alphaStrips; ./alphaStrips.exe $serverMidasFile"; 
	print "$cmd2\n";
	system $cmd2;
}
else
{
	print "Don't need to run alphaStrips - file exists....\n";
}



# Run alphaAnalysis
my $cmd3 = "cd /home/alpha/alphaAnalysis; ./alphaAnalysis.exe $serverMidasFile"; 
system $cmd3;


exit 0;

sub ServerCopy
{
	$| = 1;

	my $KiB = 1024;
	my $MiB = 1024*1024;
	my $GiB = 1024*1024*1024;

	my $file = shift @_;
	my @file = split(/\//, $file);
	my $fname = pop @file;

	my $size = -s $file;

	# these escape some special characters
	
	$fname =~ s/ /\\ /g;
	$file =~ s/ /\\ /g;
	
	$fname =~ s/&/\\&/g;
	$file =~ s/&/\\&/g;
	
	$fname =~ s/\(/\\\(/g;
	$file =~ s/\(/\\\(/g;
	
	$fname =~ s/\)/\\\)/g;
	$file =~ s/\)/\\\)/g;

	$fname =~ s/\+/\\\+/g;
	$file =~ s/\+/\\\+/g;
	
	$fname =~ s/\'/\\\'/g;
	$file =~ s/\'/\\\'/g;
	
	$fname =~ s/\>/\\\>/g;
	$file =~ s/\>/\\\>/g;

	$fname =~ s/\</\\\</g;
	$file =~ s/\</\\\</g;
	
	my $isThere = checkFile($fname, $size);
	#print "isThere $isThere\n";
	if ($isThere==1)
	  {
	    # nothing to do
	    print "File $fname already on alphafrm02!\n";
	    return 0;
	  }
	  
	my $dfile = "/midasdata/" . $fname;
	  
	print "Copy $fname $size bytes $file to $dfile\n";

	my $cmd = "cp $file $dfile";
	system $cmd;

	my $check = checkFile($fname, $size);
	#print "check $check\n";
	if ($check<=0)
	  {
	    print "Cannot confirm that file was copied!\n";
	    return 1;
	  }

	return 0;
}

sub checkFile
  {
  # 0 = does not exist
  # 1 = exists, and size is identical
  # -1 = exists, but size is different
  
    my $file = shift @_;
    my $size = shift @_;

    $cmd = "$nsls -l /midasdata/$file";
    my $ls = `$cmd 2>&1`;

    return 0 if ($ls =~ /No such file or directory/);

    my @ls_return = split(/\s+/, $ls);
	my $xsize = $ls_return[4];

    return 1 if ($size == $xsize);
    
    return -1;
  }
