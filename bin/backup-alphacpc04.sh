#!/bin/sh
#
# $Id: backup-alphacpc04.sh 817 2007-06-20 14:15:22Z olchansk $
#
# $Log: backup-alphacpc04.sh,v $
# Revision 1.2  2007/06/11 18:24:40  alpha
# KO - latest version of scripts for backup up alpha stuff
#
# Revision 1.1  2006/07/18 14:53:58  alpha
# KO- backup script for copying alphacpc05 files to alphacpc04
#
#

cd /backups/alphacpc05 || exit 1

/bin/mv backup.3   backup.tmp
/bin/mv backup.2   backup.3
/bin/mv backup.1   backup.2
/bin/mv backup.0   backup.1
/bin/mv backup.tmp backup.0
/bin/mkdir -p backup.0
/bin/cp -dflpRu backup.1/* backup.0/
/bin/touch backup.0
#/usr/bin/rsync -av -e ssh --delete alphacpc05:/root alphacpc05:/etc alphacpc05:/var alphacpc05:/home backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/root backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/etc  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/var  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/boot backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/opt  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/usr/local backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc05:/home backup.0/

#/usr/bin/rsync -av --delete /data/alpha/history /backups/alpha_history

#end file
