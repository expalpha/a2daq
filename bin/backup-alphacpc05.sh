#!/bin/sh
#
# $Id: backup-alphacpc05.sh 1019 2007-10-25 14:16:57Z olchansk $
#
# $Log: backup-alphacpc05.sh,v $
# Revision 1.1  2007/06/11 18:24:40  alpha
# KO - latest version of scripts for backup up alpha stuff
#
# Revision 1.1  2006/07/18 14:53:58  alpha
# KO- backup script for copying alphacpc05 files to alphacpc04
#
#
# Use this crontab entry:
# 05  8 * * * /home/alpha/online/bin/backup-alphacpc05.sh

cd /backups/alphacpc09 || exit 1

/bin/mv backup.3   backup.tmp
/bin/mv backup.2   backup.3
/bin/mv backup.1   backup.2
/bin/mv backup.0   backup.1
/bin/mv backup.tmp backup.0
/bin/mkdir -p backup.0
/bin/cp -dflpRu backup.1/* backup.0/
/bin/touch backup.0
/usr/bin/rsync -av -e ssh --delete alphacpc09:/root backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/etc  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/var  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/boot backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/opt  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/usr/local backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/home  backup.0/
/usr/bin/rsync -av -e ssh --delete alphacpc09:/home2 backup.0/

#end file
