#!/bin/sh -x

rsync -azv --progress --delete-after /home/alpha/online/elog alpha@alpha00.triumf.ca:/z6tb/cern_backups/alphadaq-elog-history/
rsync -azv --copy-links --progress --delete-after /alpha/data39a/alpha/history* alpha@alpha00.triumf.ca:/z6tb/cern_backups/alphadaq-elog-history/
rsync -azv --copy-links --progress --delete-after /alpha/data39a/alpha/alpha_online_elog_logbooks alpha@alpha00.triumf.ca:/z6tb/cern_backups/alphadaq-elog-history/ --exclude '*.png.png' --exclude '*.jpg.png' --exclude '*.PNG.png' --exclude '*.gif.png' --exclude '*.JPG.png' --delete-excluded

#
