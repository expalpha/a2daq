#!/bin/sh

#rsync -av --exclude '*.mid' --delete -e ssh /home/midas /home/alpha olchansk@ladd00.triumf.ca:/home/olchansk/alpha/alphacpc05/
#rsync -avz --exclude 'alpha/online/labview' --exclude '*.hst' --exclude '*.mid' --delete -e ssh /home/alpha /home/alphacdr olchansk@ladd00.triumf.ca:/home/olchansk/alpha/alphacpc09/
#rsync -avz --exclude 'alpha/online/labview' --exclude 'alpha/online/history' --exclude 'alpha/online/elog' --exclude 'alphacdr/.ssh' --delete -e ssh /home/alpha /home/alphacdr olchansk@ladd00.triumf.ca:/home/olchansk/alpha/alphacpc09/
#rsync -avz --progress --exclude 'alpha/online/labview' --exclude 'alpha/online/history' --exclude 'alpha/online/elog' --exclude 'alphacdr/.ssh' --exclude 'alphacdr/.Xauthority' --delete-after /home/alpha /home/alpha1 /home/alphacdr alpha@iris02.triumf.ca:/ladd/iris_data2/alpha/alphadaq-home
#rsync -avz --progress --exclude 'alpha/online/elog/logbooks-moved' --exclude 'alphacdr/.ssh' --exclude 'alphacdr/.Xauthority' --delete-after /home/alpha /home/alpha1 /home/alphacdr alpha@iris02.triumf.ca:/ladd/iris_data2/alpha/alphadaq-home

#rsync -avz --progress --exclude 'alphacdr/.ssh' --exclude 'alphacdr/.Xauthority' --delete-after /home/alpha /home/alpha1 /home/alphacdr alpha@iris02.triumf.ca:/ladd/iris_data2/alpha/alphadaq-home

rsync -avz --progress --exclude 'alpha/online/bin/google_speech/cache' --exclude 'tree*.root' --exclude 'alphacdr/.ssh' --exclude 'alphacdr/obsolete' --exclude 'alphacdr/.local' --exclude 'alphacdr/.keytab' --exclude 'alphacdr/.eos*' --exclude 'alphacdr/.gnupg' --exclude 'alphacdr/.Xauthority' --exclude 'alpha/.cache' --exclude 'alphacdr/.dbus' --exclude 'alphacdr/.config' --exclude 'alphacdr/.emacs.d' --exclude 'alphacdr/*keytab*' --exclude 'alphacdr/*/*keytab*' --delete-after --delete-excluded /home/alpha /home/alpha1 /home/alphacdr alpha@alpha00.triumf.ca:/z6tb/cern_backups/alphadaq-home

#
