#!/bin/bash

ls -l $directory"/backup.snar"
echo "backing up "$directory 

if [ $? -ne 0 ]
then
echo "no snar file"
#/usr/sbin/sendmail alpha-admins@cern.ch < errormail.txt
exit 1
fi

mkdir "/tmp"$directory
filename="/tmp"$directory"/backup."$mydate".tar.gz";
echo "Backing up into output file" $filename

tar -c -f $filename -g $directory/backup.snar $directory
