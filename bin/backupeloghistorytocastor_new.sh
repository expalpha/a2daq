#!/bin/bash

#eoin.butler@cern.ch October 2, 2012
#Does an incremental backup using tar and copies the tar files to CASTOR
# use flag -r to do a non-incremental backup

#simone.stracka@cern.ch January 24, 2013
#Fixes non functioning transfer to CASTOR

#eoin.butler@cern.ch April 25, 2013
#attempt to really fix kinit problem by using keytab


mydate=`date +%Y%m%d`
filenameBase="backup."$mydate".tar.gz";
errors=0;

/bin/rm /home/alpha/online/logs/backuplog.log
exec > >(tee /home/alpha/online/logs/backuplog.log)
exec 2>&1

reset=0
while [ $# -gt 0 ]
do
    case "$1" in
    (-r) reset=1;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*)  break;;
    esac
    shift
done

#ssh alphacdr@alphacpc09 "kinit alphacdr@CERN.CH -k -t /home/alphacdr/alphacdr.keytab"

backupdirectory ()
{
	thiserror=0;
	echo
	echo "###########################################" 
	echo "INFO   Backing up "$directory 
	echo "###########################################"

	cp $directory/backup.snar $directory/backup.snar.old
	if [ $? -ne 0 ]
		then
		echo "couldn't stat snar file in " $directory
		thiserror=1
		errors=1
		return 0

	fi
	
	if [ $reset -ne 0 ]
		then
		echo "removing the snar file <- THIS BACKUP IS NON INCREMENTAL"
		/bin/rm $directory/backup.snar # do a non-incremental backup
		filenameBase="backupFULL."$mydate".tar.gz";
	fi

	mkdir -p "/data39b/alpha/castor_backup_tmpdir"$directory
	filename="/data39b/alpha/castor_backup_tmpdir"$directory"/"$filenameBase;

	echo "INFO   Backing up into output file" $filename

	tar -c -z -f $filename -g $directory/backup.snar $directory;
	
    previous_dir=`pwd`
    bkup_tmp="/data39b/alpha/castor_backup_tmpdir2"
	mkdir $bkup_tmp
	cd $bkup_tmp
	tar -x -z -f $filename 
	/bin/rm $filename
	tar -c -z -f $filename *
	cd $previous_dir
	/bin/rm -r $bkup_tmp
		
	filesize=$(stat -c%s "$filename");
	
	
	
	if [ $filesize -lt 1024 ]
		then
		echo "###########################################" 
		echo "File to send to CASTOR too small ("$filesize") - skipping (will send the next time)"
		echo "###########################################"
		cp $directory/backup.snar.old $directory/backup.snar
		else
	
		ssh alphacdr@alphadaq "kinit -R && nsls -l $remotedirectory/$filenameBase"
		
		if [ $? -eq 0 ]
			then
			echo "###########################################" 
			echo "File of same name exists in CASTOR - not overwriting"
			echo "###########################################"
			thiserror=1;	

			else
			ssh alphacdr@alphadaq "kinit -R && rfcp $filename $remotedirectory"
			ssh alphacdr@alphadaq "kinit -R && nsls -l $remotedirectory/$filenameBase"
			
			if [ $? -ne 0 ]
				then
				echo "###########################################" 
				echo "ERROR  Backup of "$directory" not confirmed in castor"
				echo "###########################################" 
				thiserror=1;

				else
				echo "###########################################" 
				echo "INFO   Backup of "$directory" succeeded"
				echo "###########################################" 

			fi		
		fi	
	fi

	if [ $thiserror -gt 0 ]
		then
		mv $directory/backup.snar.old $directory/backup.snar
		errors=1;
	fi

	
	/bin/rm $filename # clean up after ourselves
	
}

### Add directories here

###### ALPHA elog ######
directory="/home/alpha/online/elog/logbooks/ALPHA";
remotedirectory="/castor/cern.ch/alpha/elog/alpha";
backupdirectory
########################

###### Datalog elog ######
directory="/home/alpha/online/elog/logbooks/DataLog";
remotedirectory="/castor/cern.ch/alpha/elog/datalog";
backupdirectory
########################

###### Sequencer elog ######
directory="/home/alpha/online/elog/logbooks/SequencerEvents";
remotedirectory="/castor/cern.ch/alpha/elog/sequencer";
backupdirectory
########################

###### History  ######
directory="/home/alpha/online/history";
remotedirectory="/castor/cern.ch/alpha/history";
backupdirectory
########################

if [ $errors -ne 0 ]
then
	elogsubject="Elog\ backup\ had\ errors,\ please\ check!";
	suppress=0
else
	if [ $reset -ne 0 ]
	then
		elogsubject="FULL\ Elog\ backup\ finished\ sucessfully";
	else
		elogsubject="Elog\ backup\ finished\ sucessfully";
	fi
	suppress=1
fi
echo $elogsubject
sleep 120 # try to see if waiting makes the backup file upload correctly EB 14.10.2014
ssh alpha@alphacpc05 elog -h localhost -p 8080 -l Datalog -a "Suppress="$suppress -a "Type=SoftwareLog" -a "Purpose=Backups" -a "Author=alphadaq" -a "Subject="$elogsubject -f /home/alpha/online/logs/backuplog.log $elogsubject
