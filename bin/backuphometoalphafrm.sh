#!/bin/bash

mydate=`date +%Y%m%d`

ls -l /alpha/alphaBACKUP/backup.snar

if [ $? -ne 0 ]
then
echo "no snar file"
/usr/sbin/sendmail ebutler@cern.ch < mountfailmail.txt
exit 1
fi

filename="/alpha/alphaBACKUP/backup."$mydate".tar.gz";

echo "Backing up into output file" $filename

tar -c -f $filename -g /alpha/alphaBACKUP/backup.snar /home/alpha

/usr/sbin/sendmail ebutler@cern.ch < successmail.txt
