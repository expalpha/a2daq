#!/usr/bin/perl -w

# check we are on alphavme02

my $cmd = "echo \$HOSTNAME";
my $hostname = `$cmd`; 
print $hostname;

if($hostname =~ "alphavme02")
{
    printf("Check that we are on alphavme02... \033[1;32m[OK]\033[1;0m\n");
} else {
    printf("Check that we are on alphavme02... \033[1;31m[FAIL]\033[0m\n");
    exit;
}

# create output log file

# ... get run number
my $runNumber = `/home/alpha/packages/midas/linux-m32/bin/odbedit -e alpha -h alphacpc09 -d Runinfo -c \'ls -v \"run number\"\'`;
chomp( $runNumber );
printf("Found Run Number %d from the mserver\n", $runNumber);
printf ("\033[1;31mHit Return \033[0mto continue, Ctrl-C to abort or enter a new number ");
my $runNumber2 = <>;
chomp( $runNumber2 );
if($runNumber2 > 100) {
	$runNumber = $runNumber2;
}

printf("Using Run Number %d \n", $runNumber);

# ... get the time
my $theTime = Get_Time();

# ... create log name
my $readDir = "/home/alpha/online/va1taLogs/";
my $readLog = $readDir . "detector_register_read_load_run" . $runNumber . "_" . $theTime . ".log";
printf("Output log file = $readLog \n");
open(my $out, ">",  $readLog ) or die "Can't open $readLog!";

# run va1ta readback
$cmd = "cd /home/alpha/online/src; ./calpulse.exe --readall";
my @readOutput = `$cmd`;

my $countSuccess = 0;
my $countErrors = 0;

foreach $line (@readOutput)
{
    printf( "$line" );
    print $out $line;
    if( $line =~ "SUCCESS" ){
        $countSuccess+=1;
    }
    if( $line =~ "ERROR" ){
        $countErrors+=1;
    }
} 

# load the registers
$cmd = "cd /home/alpha/online/src; ./calpulse.exe --muxmode --cycle 0";
my @writeOutput = `$cmd`;

my $countWriteSuccess = 0;
my $countWriteErrors = 0;

foreach $line (@writeOutput)
{
    printf( "$line" );
    print $out $line;
    if( $line =~ "SUCCESS" ){
        $countWriteSuccess+=1;
    }
    if( $line =~ "ERROR" ){
        $countWriteErrors+=1;
    }
} 


printf("\nSummary - run %d ****************************************************************\n", $runNumber);
if($countErrors>0){
	printf("Read back %d out of 59 modules SUCCESSFULLY with \033[1;31m%d BIT-ERRORS!\033[0m \n", $countSuccess, $countErrors );
}
else{
	printf("Read back %d out of 59 modules SUCCESSFULLY with \033[1;32m%d BIT-ERRORS!\033[0m \n", $countSuccess, $countErrors );
}
if($countWriteErrors>0){
	printf("Written registers of %d out of 59 modules SUCCESSFULLY with \033[1;31m%d BIT-ERRORS!\033[0m \n", $countWriteSuccess, $countWriteErrors );
}
else{
	printf("Written registers of %d out of 59 modules SUCCESSFULLY with \033[1;32m%d BIT-ERRORS!\033[0m \n", $countWriteSuccess, $countWriteErrors );
}

printf("********************************************************************************\n");



## Subroutines ===========================================================================

sub Get_Time {
    @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
    @weekDays = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
    ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek ) = localtime();
    $year = 1900 + $yearOffset;
    $theTime = "$hour:$minute:$second\_$weekDays[$dayOfWeek]\_$months[$month]\_$dayOfMonth\_$year";
    return $theTime;
}
