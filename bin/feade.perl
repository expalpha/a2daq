#!/usr/bin/perl -w
#
# $Id: feade.perl,v 1.3 2007/06/20 13:06:43 alpha Exp $
#

my $input = shift @ARGV;
my $tag   = shift @ARGV;
my $dest  = shift @ARGV;

die "AD data file $input does not exist or is not readable: $!" if ! -r $input;
die "Tag is not defined" if ! defined $tag;

use IO::Socket::INET;

$| = 1;

my $prevage;

while (1)
  {
    # wait until file is updated

    my $age;
    while (1) {
      $age = (-M $input);
      #print "age $prevage $age\n";

      last if ((!defined $prevage) || ($age < $prevage));

      sleep(1);
    }

    $prevage = $age;

    # open the file

    open(IN,$input) || die "Cannot read $input: $!";

    #
    # example data file:
    #
    # Time_ms FTA.TFA9012 AQN ADE.USER.SEG_4  FTA.TFA9053 AQN ADE.USER.SEG_4  DI.TFA6006 AQN ADE.USER.SEG_4   DR.TFA5302 AQN ADE.USER.SEG_4   DE.TFA7049 AQN ADE.USER.SEG_4
    # 1181512826911   1478    1397    876     95.9    2.395
    # 1181512927712   1494    1425    888     97.2    2.56
    #
    
    # skip old data

    my $last;
    while (my $in = <IN>) {
      $last = $in;
    }

    #print "Last [$last]\n";

    #
    # example of currupted data:
    #
    # 1181599355313   666.525 1.3e-07 5.3e-08 1.8e-08 163.549 3.83429 0.0854506 -122.578 1553 data buffer corrupted -2.52445 -0.332647 3
    #
    # another example, not so obvious corruption: (notice the value 1.23693e-27)
    #
    # 1181599335356   666.525 1.2e-07 5.3e-08 1.8e-08 163.549 3.83429 0.0897231       -122.578        134.587 -2.52384        -0.332036       3
    # 1181599340375   667.135 1.3e-07 5.3e-08 1.8e-08 163.549 3.8349  0.086061        1.23693e-27     134.587 -2.52445        -0.333257       3
    # 1181599345381   666.525 1.3e-07 5.3e-08 1.8e-08 163.549 3.83368 0.0891127       -122.578        134.587 -2.52323        -0.332036       3



    next if $last =~ /data buffer corrupted/;

    $last=~s/Pow.conv. specif. interf. malfunctioning/-99/g; # search and replace error comment with -99
    $last=~s/1553 RTI has nothing to send/-99/g;             # search and replace error comment with -99
    $last=~s/1553 data buffer corrupted/-99/g;               # search and replace error comment with -99
	$last=~s/Server '[A-Za-z0-9_]*' is down or unreachable/-99/g; # search and replace error comment with -99
	    $last=~s/,/ /g; #replace commas with spaces (workaround, EButler, elog:ALPHA/13193)

    my @data = split(/\s+/, $last);
    my $ts = shift @data;
    my $nts = $ts/1000.0; # convert AD ms timestamp to seconds

#    my $element;
#    my $index;
#    foreach $element (@data)
#    {
#	print $index;
#	print " ";
#	print $element;
#	print "\n";
#	$index++;
#    }

    my $data = "$tag $nts ".join(" ", @data)."\n";

    #print "sending data: [$data]\n";
    
    if ($dest) {
      my $s = IO::Socket::INET->new(PeerAddr => $dest);
      if (! defined $s) {
        die "Cannot connect to $dest: $!";
        # NOT REACHED
        sleep(10);
        next;
      }
        
      print $s $data;
      $s->close;
    }
  }

#end
