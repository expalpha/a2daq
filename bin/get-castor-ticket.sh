#!/bin/sh

case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "This script should be executed on alphadaq"
    exit 1
    ;;
esac
#kinit -k -t /etc/alphacdr.keytab alphacdr
kinit alphacdr@CERN.CH -k -t /etc/alphacdr.keytab
klist

#end file
