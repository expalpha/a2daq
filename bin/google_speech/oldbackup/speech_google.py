#!/usr/bin/python

import sys
import os
import md5
import shutil
from GoogleTTS import *

cache_folder = '/home/alpha/online/bin/google_speech/cache'
player_command = '/home/alpha/online/bin/google_speech/mpg123 -q'
backup_command = 'festival --tts'

def use_backup_command(phrase_to_speak):
	print 'using backup command'
	
	os.system('echo '+sanitize_input(phrase_to_speak)+' | '+backup_command)
	
def sanitize_input(input_text):
	allowed_chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890, '
	
	output_text=''
	for c in input_text:
		if c in allowed_chars:
			output_text+=c
		else:
			output_text+=' '
	
	return output_text

if __name__ == "__main__":
	print('type speech, hit return to talk. type quit to exit');
	while(1):
		
		try:
			phrase_to_speak = raw_input('speech> ') # a prompt
		except EOFError:
			print 'caught EOF, exiting'
			break
		
		# format the string a bit
		phrase_to_speak = phrase_to_speak.lower() # all lower case
		if(len(phrase_to_speak) > 500 ): # chop to a maximum length
			phrase_to_speak = phrase_to_speak[0:499]
		phrase_to_speak = sanitize_input(phrase_to_speak) # sanitize input to only alphanumerics
		
		if(phrase_to_speak.startswith('quit') or phrase_to_speak.startswith('exit')):
			break
			
		print 'speaking: '+phrase_to_speak
		
		# hash the phrase to make a cached file so that we don't have to continously query google
		if( not(os.path.isdir(cache_folder) ) ):
			os.mkdir(cache_folder)
                hashed_phrase = md5.new(phrase_to_speak).hexdigest()
		cache_filename = cache_folder + '/' + hashed_phrase + '.mp3'
		print('cache filename: '+cache_filename)
		
		if( not(os.path.exists(cache_filename)) or os.stat(cache_filename).st_size<=0 ):
			#download the phrase from google tts
			print 'downloading mp3 from google'
			audio_extract(input_text=phrase_to_speak, args={'output':cache_filename} )
			
			if( os.system(player_command+' '+cache_filename) >0 or os.stat(cache_filename).st_size<=0 ):
				print 'error playing back using command '+player_command
				use_backup_command(phrase_to_speak)
		else:
			print 'using cached mp3 file'
			
			if( os.system(player_command+' '+cache_filename) >0 ):
				print 'error playing back using command '+player_command
				use_backup_command(phrase_to_speak)




