#!/usr/bin/perl -w

for (my $i=0; $i<@ARGV; $i+=2)
  {
    my $mname = $ARGV[$i+0]; # midas name for odb -c sh mname
    my $sname = $ARGV[$i+1]; # system name for killall -KILL sname
    my $cmd;

    run("odbedit -c \"sh $mname\"");
    sleep(1);

    my $pid = getpid($sname);
    next if ! $pid;

    run("kill $pid");
    sleep(2);

    for (my $j=0; $j<5; $j++)
      {
	my $pid = getpid($sname);
	next if ! $pid;

	run("kill -KILL $pid");
	sleep(1);
      }
  }

sub getpid
  {
    my $sname = shift @_;
    my $pid = `/sbin/pidof $sname`;
    return unless $pid =~ /^\d+/;
    chop $pid;
    return $pid;
}

sub run
  {
    my $cmd = shift @_;
    print "Run: $cmd\n";
    system $cmd;
  }
    
#end
