#!/bin/sh

cd /home/alpha/online

case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "The kill_daq script should be executed on alphadaq"
    exit 1
    ;;
esac

kill_client.perl analyzer    analyzer
kill_client.perl fevme       fevme.exe
kill_client.perl felabview   felabview.exe
kill_client.perl fesequencer fesequencer.exe
kill_client.perl fesequencer2 fesequencer2.exe
kill_client.perl logger      xmlogger
kill_client.perl none   mserver
kill_client.perl none   logfile
kill.feade
kill_client.perl Speaker     mlxspeaker
kill_client.perl mhttpd xmhttpd

#end file
