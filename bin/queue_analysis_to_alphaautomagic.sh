#!/bin/bash

case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "The start_daq script should be executed on alphadaq"
    exit 1
    ;;
esac


export SUBMIT_JOB=0
LOOP_COUNT=0
MIDAS_EXPT_NAME="alpha"
while [ $SUBMIT_JOB -eq 0 ]; do
  Run_number=`odbedit -e ${MIDAS_EXPT_NAME} -c 'ls "/Runinfo/Run number"'`
  echo "ODB query: ${Run_number}" #>> ~/online/bin/AnalysisQueue.log 2>&1
  number=`echo $Run_number | awk '{print $3}'`
  echo "Run number extracted: ${number}."  #>> ~/online/bin/AnalysisQueue.log 2>&1
  case ${number#[-+]} in
  *[!0-9]* ) 
    echo "Error extracting run number... trying again"#>> ~/online/bin/AnalysisQueue.log 2>&1
    sleep 1 
    LOOP_COUNT=$[ $LOOP_COUNT+1 ]
    if [ $LOOP_COUNT -gt 60 ]; then
      echo "Failed to get run number after 60 attempts"#>> ~/online/bin/AnalysisQueue.log 2>&1
      exit
    fi
  ;;
  * )
    SUBMIT_JOB=1
    echo "Run number extracted: ${number}"  #>> ~/online/bin/AnalysisQueue.log 2>&1
    #break
  ;;
  esac
done

echo "Queing run \"$number\" to alpha@alphaautomagic2" #>> ~/online/bin/AnalysisQueue.log 2>&1
ssh alpha@alphaautomagic2 touch ~/watch_me/${number}  #>> ~/online/bin/AnalysisQueue.log 2>&1

#ssh alphacdr@alphadaq 
#ssh -t alphacdr@alphadaq ssh alphaautoslow4 "bash Lazy_analysis.sh ${number} &> /dev/null "
#ssh alphacdr@alphaautoslow4 "bash Lazy_analysis.sh ${number} &> /dev/null" | ssh alphacdr@alphadaq
