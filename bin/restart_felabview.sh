#!/bin/bash

ps aux | grep felabview.exe | grep -v grep | grep -v alphagdaq.cern.ch | grep -v logfile
PID=$(ps aux | grep felabview.exe | grep -v grep | grep -v alphagdaq.cern.ch | grep -v logfile | awk '{ print $2 }')
kill -9 $PID
ps aux | grep felabview.exe | grep -v grep | grep -v alphagdaq.cern.ch | grep -v logfile

cd ~/online
sleep 5
logfile --once --name felabview /home/alpha/online/bin/felabview.exe -D
ps aux | grep felabview.exe | grep -v grep | grep -v alphagdaq.cern.ch | grep -v logfile
