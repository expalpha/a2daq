#!/bin/sh -x

cd /home/alpha/online

case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "The start_alpha1 script should be executed on alphadaq"
    exit 1
    ;;
esac

#kill_daq.sh

MIDAS_EXPT_NAME=alpha1
export MIDAS_EXPT_NAME

$MIDASSYS/linux/bin/odbedit -c clean

# start ALPHA1 history access
$MIDASSYS/linux/bin/mhttpd -p 8071 -e alpha1 -D -a localhost -a alphacpc05.cern.ch
sleep 1
$MIDASSYS/linux/bin/mhttpd -p 8072 -e alpha1 -D -H -a localhost -a alphacpc05.cern.ch

echo
echo Web server for ALPHA1 history access was started.
echo 


#end file
