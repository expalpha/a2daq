#!/bin/sh

cd /home/alpha/online

case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "The felabview autossh tunnel script should be executed on alphadaq"
    exit 1
    ;;
esac

logfile --once --name autossh_for_labview autossh -M 0 -a -k -n -T -x -4 -v -N -R 0.0.0.0:12021:alphadaq:12021 -o "ExitOnForwardFailure=yes" -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3" alphacpc09 &

echo
echo autossh tunnel for felabview on alphacpc09 was started
echo 

#end file
