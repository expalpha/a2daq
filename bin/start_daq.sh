#!/bin/sh

cd /home/alpha/online

case `hostname` in
alphadaq*)
    echo "Good, we are on alphadaq"
    ;;
*)
    echo "The start_daq script should be executed on alphadaq"
    exit 1
    ;;
esac

#kill_daq.sh

odbedit -c clean
#odbedit -c "rm /Analyzer/Trigger/Statistics"
#odbedit -c "rm /Analyzer/Scaler/Statistics"
mhttpd -D
#/home/alpha/packages/elog/elogd -D -k -x -c /home/alpha/online/elog/elogd.cfg

#
# ALPHA1 stuff moved to start_alpha1.sh
#
## start ALPHA1 history access
##mhttpd -p 8071 -e alpha1 -D
##mhttpd -p 8072 -e alpha1 -D -H

# start ALPHA stuff
mhttpd -p 8081 -D -a localhost -a alphacpc09.cern.ch -a alphacpc39.cern.ch -a alphadaq.cern.ch -a alphacpc05.cern.ch
sleep 1
mhttpd -p 8082 -D -H -a localhost -a alphacpc09.cern.ch -a alphacpc39.cern.ch -a alphadaq.cern.ch -a alphacpc05.cern.ch
sleep 2
mserver -p 6061 -D -a localhost -a alphacpc09.cern.ch -a alphavme01.cern.ch -a alphavme04.cern.ch -a alphacpc05.cern.ch -a alphacpc39.cern.ch -a alphadaq.cern.ch
mlogger -D
#logfile        --name Speaker     mlxspeaker &
#logfile --once --name felabview   felabview.exe   &
#logfile --once --name fesequencer2 fesequencer2.exe &
#logfile --once --name fevme     ssh alphavme02 fevme.exe     &
#logfile --once --name fesy127   ssh alphavme02 fesy127.exe   &

$HOME/online/bin/start_feade.sh

#autossh for labview removed May 18th 2015
#$HOME/online/bin/start_autossh_for_labview.sh

echo
echo ALPHA MIDAS was started
echo 

mhttpd -D
#end file
