!/bin/bash

BANK_NAME="TEST"

for i in `seq 1 10`; do
  for j in 1 2; do #Send each command twice (so that we can see steps on histories 
    TIME=`date +"%s.%6N"`
    echo "Sending: ${BANK_NAME} ${TIME} 5.${i}"
    echo "${BANK_NAME}  ${TIME} 5.${i}" > /dev/tcp/alphadaq/12021
    sleep 0.01
  done
done

#Link to history page: https://alphacpc05.cern.ch/HS/Tests/speedy

#mhist -e labview/BMON -v BMON -i 1
echo "
mhist report
"
mhist -e labview/${BANK_NAME} -v ${BANK_NAME}  -i 0:1 -t 0 -b
echo "
dat file:
"

grep BMON ../labview/`ls -rt ../labview | tail -n 1` | tail -n 20
