/********************************************************************\

  Name:         felabview.cxx
  Created by:   K.Olchanski

  Contents:     ALPHA data collector for Labview data

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <ctype.h>

#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "evid.h"


/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   const char *frontend_name = "felabview_worker_";
/* The frontend file name, don't change it */
   const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 200*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 500*1024;

  extern INT run_state;
  extern HNDLE hDB;

/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_event(char *pevent, INT off);
  int openListenSocket(int port);

/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {

    {"Labview_worker_%02d",               /* equipment name */
     { EVID_LABVIEW, (1<<EVID_LABVIEW), /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      LAM_SOURCE(0, 0xFFFFFF),                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS|RO_ODB,       /* when to read this data */
      1,                      /* poll for this many ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_event,      /* readout routine */
     NULL, NULL,
     NULL,       /* bank list */
    }
    ,
    
    {""}
  };
  
#ifdef __cplusplus
}
#endif
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

extern INT frontend_index;
const char* listen_to_host;
#include "utils.cxx"

static int gHaveRun = 0;
static int gRunNumber = 0;

#include <vector>
#include <string>

std::vector<std::string> allowed_hosts;

std::vector<std::string> labview_banks;
std::vector<std::string> labview_strings;

/*-- Frontend Init -------------------------------------------------*/

INT readLabviewBanks()
{

  printf("Reading Labview Banks \n");

  char odb_addr[100];
  sprintf(odb_addr,"/Equipment/labview_worker_%02d/Settings/BankNames", frontend_index);

  HNDLE hKey, hDB;
    
  cm_get_experiment_database(&hDB,NULL); //get a handle to the odb
  db_find_key(hDB,0,odb_addr,&hKey);

  INT idx = 0;
  bool done = false;
  while (!done)
    {
      INT result;
      HNDLE subKey;
      KEY theSubKey;
      result = db_enum_key(hDB, hKey, idx, &subKey );
      if(result == DB_SUCCESS)
        {
          // Have a subkey. This should be a bank name
          
          if(db_get_key(hDB, subKey, &theSubKey) == DB_SUCCESS)
            {
              char fixname[32];          
              char fullkeypath[255];
              char bname[5];

              strncpy(fixname, theSubKey.name, 32);
              
              fullkeypath[0] = '\0';
              strcpy(fullkeypath,odb_addr);
              strcat(fullkeypath,"/");
              strcat(fullkeypath, fixname);
              
              const char* s = odbReadString(fullkeypath, 0, NULL, 250);

              bname[0]='\0';
              if(s[0] == '\0')
                {
                  // printf("empty... \n");
                  strncpy(bname, fixname, 4);
                  bname[4]='\0';
                }
              else
                {
                  // printf("not empty %s \n", s);
                  strncpy(bname, s, 4);
                  bname[4]='\0';
                }

              if(fixname[0] != '#')
                {
                  labview_strings.push_back(fixname);
                  labview_banks.push_back(bname);
                }


            }
              else
                return(DB_INVALID_HANDLE);

              done = false;
              idx++;
            }
          else if( result == DB_NO_MORE_SUBKEYS)
            {
              // we are done.

              for(int i=0; i < labview_strings.size(); i++)
                {
                  printf("vec LabviewString -> Bank [%d]: %s -> '%s' \n", i, labview_strings[i].c_str(), labview_banks[i].c_str());
                }

              done = true;
            }
          else
            {
              // an error case.
              done = true;
              return(result);
            } 
        }

      printf("** Done reading labview banks\n");
      return(FE_SUCCESS);
    }


INT frontend_init()
{
  int status;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  listen_to_host=odbReadString("/Equipment/labview/worker_ports/hosts",frontend_index,NULL,200);


  char host_front_end_name[256];
  strcpy(host_front_end_name,"felabview_");
  strcat(host_front_end_name,listen_to_host);

  //Rename frontend... but this isn't good...
  char odb_path[256];
  sprintf(odb_path,"/Equipment/Labview_worker_00/Variables/Host",frontend_index);
  
  
  odbWriteString(odb_path,0,host_front_end_name,32);

  gRunNumber = odbReadInt("/runinfo/run number",0,0);
  printf("Run number %d\n", gRunNumber);

  allowed_hosts.push_back("localhost");
  allowed_hosts.push_back(listen_to_host);

  if (1) {
    std::string s = "";
    if (allowed_hosts.size()>30)
    {
		cm_msg(MINFO, "frontend_init", "%d allow hosts", allowed_hosts.size());
	}
	else
	{
    for (unsigned i=0; i<allowed_hosts.size(); i++) {
      if (i>0)
        s += ", ";
      s += allowed_hosts[i];
    }
    
    cm_msg(MINFO, "frontend_init", "Allowed hosts: %s", s.c_str()); 
   }
  }
  readLabviewBanks();

  status = openListenSocket(odbReadInt("/Equipment/labview/worker_ports/ports",frontend_index,0));

  if (status != FE_SUCCESS)
    return status;

  return FE_SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  gHaveRun = 0;

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  gHaveRun = 1;
  gRunNumber = run_number;
  printf("begin run %d\n",run_number);

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  static bool gInsideEndRun = false;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  gHaveRun = 0;
  gRunNumber = run_number;
  printf("end run %d\n",run_number);

  gInsideEndRun = false;

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gHaveRun = 0;
  gRunNumber = run_number;

  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gHaveRun = 1;
  gRunNumber = run_number;

  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  printf("frontend_loop!\n");
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

const int kMaxSockets = 50;
const int kBufSize = 100*1024;

struct LabviewSocket
{
  int fd;
  char buf[kBufSize];
  int  bufwptr;
  static int ofd;
  bool hasNewData;
  bool isOpen;
  struct sockaddr_in addrinfo;

  LabviewSocket(int socket) // ctor
  {
    fd = socket;
    bufwptr = 0;
    hasNewData = true;
    isOpen      = true;
    //ofd = 0;
/*
    if (true && ofd <= 0)
      {
        time_t t = time(0);
        char fname[1024];
        sprintf(fname,"/home/alpha/online/labview/labview%d.dat",(int)t);
	if( !strcmp(odbReadString("/experiment/name", 0, NULL, 250),"agdaq") )
          sprintf(fname,"/home/agdaq/online/labview/labview%d.dat",(int)t);
        ofd = open(fname,O_WRONLY|O_CREAT|O_LARGEFILE,0777);
        printf("Writing labview data to %s\n",fname);
      }*/
  }
  void Print()
  {
    printf("Socket:%d Open:%d Buffer:%s\n",fd,(int)isOpen,buf);
  }

  void closeSocket() // close socket
  {
    if (!isOpen)
      return;

    //printf("Socket %d closed\n",fd);

    if (fd > 0)
      close(fd);
    fd = 0;

#if 0
    if (ofd > 0)
      close(ofd);
    ofd = 0;
#endif

    isOpen = false;
    hasNewData = true;
  }

  ~LabviewSocket() // dtor
  {
    if (isOpen)
      closeSocket();
    //printf("Socket %d deleted\n",fd);
    bufwptr = 0;
    hasNewData = false;
  }

  int tryRead()
  {
    int avail = kBufSize - bufwptr;
    int rd = read(fd,buf+bufwptr,avail);

    //printf("read from socket %d yelds %d\n",fd,rd);

    if (rd == 0) // nothing to read, socket closed at the other end
      return 0;
    if (rd < 0) // error, close socket
      return -1;
    if (ofd > 0)
      {
        char hdr[1024];
        sprintf(hdr,"\n=== time: %d, socket: %d\n",(int)time(NULL),fd);
        write(ofd,hdr,strlen(hdr));
        write(ofd,buf+bufwptr,rd);
      }
    bufwptr += rd;
	socklen_t addrinfolen = sizeof(addrinfo);
	int status = getpeername(fd, (sockaddr*)(&addrinfo), &addrinfolen);
	if(status<0)
	printf("Getsockname error\n");
    buf[bufwptr] = 0;
    hasNewData = true;
    return rd;
  }

  void flush()
  {
    bufwptr = 0;
    hasNewData = false;
  }
};

int LabviewSocket::ofd = -1;

int gNumSockets = 0;
LabviewSocket* gSockets[kMaxSockets];

int gListenSocket = 0;
static int gPtr = 0;

int openListenSocket(int port)
{
  gListenSocket = socket(PF_INET,SOCK_STREAM,0);
  
  if (gListenSocket <= 0)
    {
      cm_msg(MERROR,"openListenSocket","Cannot create socket, errno %d (%s)",errno,strerror(errno));
      return FE_ERR_HW;
    }

  int on = 1;
  int status = setsockopt(gListenSocket,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
  if (status != 0)
    {
      cm_msg(MERROR,"openListenSocket","Cannot setsockopt(gListenSocket,SOL_SOCKET,SO_REUSEADDR), errno %d (%s)",errno,strerror(errno));
      return FE_ERR_HW;
    }

  struct sockaddr_in xsockaddr;
  xsockaddr.sin_family = AF_INET;
  xsockaddr.sin_port = htons(port);
  xsockaddr.sin_addr.s_addr = INADDR_ANY;

  status = bind(gListenSocket,(sockaddr*)(&xsockaddr),sizeof(xsockaddr));
  if (status != 0)
    {
      cm_msg(MERROR,"openListenSocket","Cannot bind() to port %d, errno %d (%s)",port,errno,strerror(errno));
      return FE_ERR_HW;
    }

  status = listen(gListenSocket,10);
  if (status != 0)
    {
      cm_msg(MERROR,"openListenSocket","Cannot listen() to port %d, errno %d (%s)",port,errno,strerror(errno));
      return FE_ERR_HW;
    }

  cm_msg(MINFO,"OpenListenSocket","Listening for connections on TCP port %d",port);

  return FE_SUCCESS;
}

#include <netdb.h>

void tryAccept()
{
  if (gNumSockets >= kMaxSockets - 2)
    {
      cm_msg(MERROR,"openListenSocket","Cannot accept() new connection, too many connections: %d, maximum %d", gNumSockets, kMaxSockets);
      for (int i=0; i<gNumSockets; i++)
         gSockets[i]->Print();
      return;
    }

  struct sockaddr_in xsockaddr;
  socklen_t xsockaddrlen = sizeof(xsockaddr);
  int socket = accept(gListenSocket,(sockaddr*)(&xsockaddr),&xsockaddrlen);
  if (socket <= 0)
    {
      cm_msg(MERROR,"openListenSocket","Cannot accept() new connection, errno %d (%s)",errno,strerror(errno));
      return;
    }

  /* check access control list */ 
  if (allowed_hosts.size() > 0) { 
    int allowed = FALSE; 
    struct hostent *remote_phe; 
    char hname[256]; 
    struct in_addr remote_addr; 
 
    /* save remote host address */ 
    memcpy(&remote_addr, &(xsockaddr.sin_addr), sizeof(remote_addr)); 
 
    remote_phe = gethostbyaddr((char *) &remote_addr, 4, PF_INET); 
 
    if (remote_phe == NULL) { 
      /* use IP number instead */ 
      strlcpy(hname, (char *)inet_ntoa(remote_addr), sizeof(hname)); 
    } else 
      strlcpy(hname, remote_phe->h_name, sizeof(hname)); 
 
    /* always permit localhost */ 
    if (strcmp(hname, "localhost.localdomain") == 0) 
      allowed = TRUE; 
    if (strcmp(hname, "localhost") == 0) 
      allowed = TRUE; 
 
    if (!allowed) { 
      for (unsigned i=0 ; i<allowed_hosts.size(); i++) 
        if (strcmp(hname, allowed_hosts[i].c_str()) == 0) { 
          allowed = TRUE; 
          break; 
        } 
    } 
 
    if (!allowed) { 
      static int max_report = 10;  
      if (max_report > 0) {  
        max_report--;  
        if (max_report == 0)  
          cm_msg(MERROR, "tryAccept", "rejecting connection from unallowed host \'%s\', this message will no longer be reported", hname);  
        else  
          cm_msg(MERROR, "tryAccept", "rejecting connection from unallowed host \'%s\'", hname);  
      }  
      close(socket); 
      return;
    } 
  } 

  for (int i=0; i<gNumSockets; i++)
    if (gSockets[i] == NULL)
      {
        gSockets[i] = new LabviewSocket(socket);
        return;
      }

  gSockets[gNumSockets] = new LabviewSocket(socket);
  gNumSockets ++;
}

bool trySelect(double xtimeout = 0.0001)
{
  fd_set rset, wset, eset;
  struct timeval timeout;

  int maxsocket = 0;

  timeout.tv_sec  = (int)xtimeout;
  timeout.tv_usec = (int)((xtimeout - timeout.tv_sec)*1000000.0);

  //printf("timeout %d sec, %d usec\n", timeout.tv_sec, timeout.tv_usec);

  FD_ZERO(&rset);
  FD_ZERO(&wset);
  FD_ZERO(&eset);

  FD_SET(gListenSocket,&rset);
  FD_SET(gListenSocket,&eset);

  if (gListenSocket > maxsocket)
    maxsocket = gListenSocket;

  for (int i=0; i<gNumSockets; i++)
    if (gSockets[i] && gSockets[i]->isOpen)
      {
        FD_SET(gSockets[i]->fd,&rset);
        FD_SET(gSockets[i]->fd,&eset);
        if (gSockets[i]->fd > maxsocket)
          maxsocket = gSockets[i]->fd;
      }

  int ret = select(maxsocket+1,&rset,&wset,&eset,&timeout);

  //printf("try select: FD_SETSIZE %d, maxsocket %d, ret %d!\n", FD_SETSIZE, maxsocket, ret);

  if (ret < 0)
    {
      if (errno == EINTR) // Interrupted system call
        return false;

      cm_msg(MERROR,"trySelect","select() returned %d, errno %d (%s)",ret,errno,strerror(errno));
      return false;
    }

  if (FD_ISSET(gListenSocket,&rset))
    tryAccept();

  for (int i=0; i<gNumSockets; i++)
    if (gSockets[i])
      if (FD_ISSET(gSockets[i]->fd,&rset) || FD_ISSET(gSockets[i]->fd,&eset))
        {
          int ret = gSockets[i]->tryRead();
          if (ret <= 0)
            {
              //delete gSockets[i];
              //gSockets[i] = 0;
              gSockets[i]->closeSocket();
              continue;
            }
        }

  bool hasNewData = false;

  for (int i=0; i<gNumSockets; i++)
    if (gSockets[i])
      hasNewData |= gSockets[i]->hasNewData;

  //printf("try select: have new data: %d\n",hasNewData);

  return hasNewData;
}

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  //printf("poll_event %d %d %d!\n",source,count,test);

  for (int i = 0; i < count; i++)
    {
      int lam = trySelect();
      printf("trySelect %d\n", lam);
      if (lam)
        if (!test)
          return lam;
    }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
     break;
   case CMD_INTERRUPT_DISABLE:
     break;
   case CMD_INTERRUPT_ATTACH:
     break;
   case CMD_INTERRUPT_DETACH:
     break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

static const int kMaxBanks = 200;
static char gBankName[kMaxBanks][10];
static int  gBankSize[kMaxBanks];

static int checkBankSize(const char*name, int size, const char* str, double data[])
{
  for (int i=0; i<kMaxBanks; i++)
    {
      if (gBankSize[i] ==0) /* end of bank array */
        {
          printf("Bank %s, new size: %d, labview data [%s]\n", name, size, str);
          strlcpy(gBankName[i], name, 10);
          gBankSize[i] = size;
        }

      if (strcmp(name, gBankName[i]) == 0)
        {
          if (size != gBankSize[i])
            {
              printf("Bank %s, size changed from %d to %d, labview data [%s], maybe padding with zeroes\n", name, gBankSize[i], size, str);

              if (data)
                for (int j=size; j<gBankSize[i]; j++)
                  data[j] = 0;

              return gBankSize[i];
            }

          return size;
        }
    }

  return size;
}

bool decodeData(char*pevent,char*buf,int bufLength, struct sockaddr_in addrinfo)
{
  //printf("decode data [%s] length %d\n",buf,bufLength);
  
  // do nothing with empty data packets
  if (bufLength < 1)
    return false;
    
  //printf("DEBUG: revc %s: %s length %d\n",inet_ntoa(addrinfo.sin_addr), buf,bufLength);
 if(strncmp(buf,"SPEAK",5)==0)
    {
      char speak_this[500]={0}; //Set all NULL (c++11 feature)
      
      int next_char=0;
      for (int i=6; i<499; i++)
        {
          if (!buf[i]) break; // if NULL (End of string)
          if ( isalnum(buf[i]) || buf[i]=='.' || buf[i]==' ' )
            {
              //printf("Add char:%c ",buf[i]);
              speak_this[next_char]=buf[i];
              next_char++;
              //printf("%s\n",speak_this);
            }
        }
      cm_msg(MTALK,"felabview_manager",speak_this);

      printf("%s\n",speak_this);
      return false;
    }
  if(strncmp(buf,"QUERY",5) ==0) // Looks for a query for data
	
	// buf needs to be of format "QUERY NAME NN"
	{
	  printf("QUERY ");
	  buf += 6; //Skip over "QUERY "
      
	  if(strlen(buf)<4)
        {
          printf("INVALID\n");
          return false;
        }
	  
	  //get the name of the bank
      
      char name[5];
	  strncpy(name, buf, 4);
	  name[4] = '\0';
	  printf("for bank %s ",name);
      
	  char odb_addr[100];
	  strcpy(odb_addr,"/equipment/labview/Variables/");
	  strcat(odb_addr,name);
	  
	  // Now get the index
	  buf+=4; //Skip over the name
	  int ind = atoi(buf); //bank index 
	  printf("[%d] ", ind);
	  
	  //get the address of the remote computer
	  printf("from %s:%d ", inet_ntoa(addrinfo.sin_addr), ntohs(addrinfo.sin_port));
      
	  
 	  HNDLE hKey, hDB;
	  KEY key;
	  double kData;
	  int kSize;
      
	  cm_get_experiment_database(&hDB,NULL); //get a handle to the odb
	  db_find_key(hDB,0,odb_addr,&hKey);
	  
	  if(hKey == 0)
        {
          printf("NOT FOUND\n");
          return false;
        }
	  
      db_get_key(hDB, hKey, &key);
      
      if(ind>=key.num_values)
        {
          printf("INDEX OUT OF RANGE: key has %d values\n",key.num_values);
          return false;
        }
      
      kSize = sizeof(kData);
      db_get_data_index(hDB, hKey, &kData, &kSize, ind, TID_DOUBLE);
      
      printf(": %.9g\n",kData);
      
      sleep(50);
      
      // Send the data to the client
      
      int sendSocket = socket(AF_INET, SOCK_STREAM, 0);
      
      printf("Send Socket: %d\n",sendSocket);
      if(sendSocket<=0)
        {
          printf("COULD NOT OPEN OUTPUT SOCKET\n");
          return false;
        }
	
      struct timeval timeout;
      timeout.tv_usec =100000;
      
      setsockopt(sendSocket,SOL_SOCKET,SO_SNDTIMEO,&timeout,sizeof(timeout));
      printf("Send socket is now: %d\n",sendSocket);
      //Reply on the port 59150
      addrinfo.sin_family = AF_INET;
      addrinfo.sin_port = htons(59150);
      
      	  printf("connecting to  %s:%d ", inet_ntoa(addrinfo.sin_addr), ntohs(addrinfo.sin_port));

      int connecterror = connect(sendSocket, (sockaddr *)(&addrinfo), sizeof(addrinfo));
      
      if(connecterror<0)
        {
          printf("COULD NOT CONNECT TO CLIENT - ERROR %d\n",connecterror);
          return false;
        }
      //write(sendSocket,kData,sizeof(kData));
      
      FILE * str_out = fdopen(sendSocket, "w");
      fprintf(str_out,"%lf\n",kData);
      fflush(str_out);
      fclose(str_out);
      
      close(sendSocket);
      
      return false;
      
	}
  if (1)
    {
      char tag[256];
      strncpy(tag, buf, sizeof(tag));
      tag[sizeof(tag)-1] = 0;
      for (unsigned int i=0; i<sizeof(tag); i++)
        if (isspace(tag[i]))
          {
            tag[i] = 0;
            break;
          }
      //printf("data from \'%s\'\n", tag);
    }
  

  // First if here for parsing a logged string
  if ( strncasecmp(buf, "sequencerevent", 14) == 0 )
    {
	  printf("Recevied sequencer event\n");
      char cmd[40960];
      if (strlen(buf) > 30000)
        buf[30000] = 0;
      snprintf(cmd, 40959, "ssh -x alphadaq ~/packages/elog/elog -h localhost -p 8080 -l SequencerEvents -a Run=%d", gRunNumber);
      FILE* fp = popen(cmd, "w");
      if(fp){
        printf("to pipe: %s\n", buf);
        fputs(buf, fp);
        pclose(fp);
      }
      else{
        printf("error opening pipe\n");
      }

      return false;
    }

  else if ( strncasecmp(buf, "arevent", 7) == 0 )
    {
	  printf("Recevied sequencer event\n");
      char cmd[20480];
      if (strlen(buf) > 15000)
        buf[15000] = 0;
      snprintf(cmd, 20479, "ssh -x alphadaq ~/packages/elog/elog -h localhost -p 8080 -l ARLog -a Run=%d", gRunNumber);
      FILE* fp = popen(cmd, "w");
      if(fp){
        printf("to pipe: %s\n", buf);
        fputs(buf, fp);
        pclose(fp);
      }
      else{
        printf("error opening pipe\n");
      }

      return false;
    }

  else if ( strncasecmp(buf, "rwevent", 7) == 0 )
    {
      printf("Recevied rw event\n");
      char cmd[10240];
      if (strlen(buf) > 5120)
        buf[5120] = 0;
      char trap[101];
      if(strstr(buf, "Catch Trap 1")!=0) {
		snprintf(trap, 100, "-a Trap=CatchTrap1");
      }
      else if(strstr(buf, "Catch Trap 2")!=0) {
		snprintf(trap, 100, "-a Trap=CatchTrap2");
      }
      else {
		snprintf(trap, 100, " ");
      }

      snprintf(cmd, 10239, "ssh -x alphadaq ~/packages/elog/elog -h localhost -p 8080 -l RWLog -a Run=%d %s", gRunNumber, trap);
      FILE* fp = popen(cmd, "w");
      if(fp){
        printf("to pipe: %s\n", buf);
        fputs(buf, fp);
        pclose(fp);
      }
      else{
        printf("error opening pipe\n");
      }

      return false;
    }
  else if ( strncasecmp(buf, "magnetevent", 11) == 0 )
    {
      printf("Recevied magnet event\n");
      char cmd[40960];
      if (strlen(buf) > 30000)
        buf[30000] = 0;
      snprintf(cmd, 10239, "ssh -x alphadaq ~/packages/elog/elog -h localhost -p 8080 -l MagnetEvents -a Run=%d", gRunNumber);
      FILE* fp = popen(cmd, "w");
      if(fp){
        printf("to pipe: %s\n", buf);
        fputs(buf, fp);
        pclose(fp);
      }
      else{
        printf("error opening pipe\n");
      }
      return false;
    }
  else if ( strncasecmp(buf, "cryostatevent", 13) == 0 )
    {
      printf("Recevied cryostat event\n");
      char cmd[20480];
      if (strlen(buf) > 15000)
        buf[15000] = 0;
      snprintf(cmd, 20479, "ssh -x  alphadaq ~/packages/elog/elog -h localhost -p 8080 -l CryostatEvents -a Run=%d", gRunNumber);
      FILE* fp = popen(cmd, "w");
      if(fp){
        printf("to pipe: %s\n", buf);
        fputs(buf, fp);
        pclose(fp);
      }
      else{
        printf("error opening pipe\n");
      }
      return false;
    }
  else if ( strncasecmp(buf, "MWAVEEVENT", 10) == 0 )
    {
      printf("Recevied microwave event\n");
      char cmd[40960];
      if (strlen(buf) > 30000)
        buf[30000] = 0;
      snprintf(cmd, 40959, "ssh -x  alphadaq ~/packages/elog/elog -h localhost -p 8080 -l MicrowaveLog -a Run=%d", gRunNumber);
      FILE* fp = popen(cmd, "w");
      if(fp){
        printf("to pipe: %s\n", buf);
        fputs(buf, fp);
        pclose(fp);
      }
      else{
        printf("error opening pipe\n");
      }
      return false;
    }
  else
    {
      // Search through banks configured from ODB /Equipment/labview/LabviewBanks

      int i = 0;
      int nBanks;
      bool found = false;


      nBanks = labview_strings.size();

      while((i < nBanks) && !found)
        {
          int nchar = 0;

          nchar = labview_strings[i].length();
          if(strncmp(buf, labview_strings[i].c_str(), nchar)==0)
            {
              // We have a match
              found = true;
              char* s = buf;

              // printf("Loop handling bank %s\n", labview_strings[i].c_str());

              // skip the record name
              s += nchar;

              // init bank structure
              bk_init32(pevent);

              // creat the databank
              double* pdata;
              bk_create(pevent, labview_banks[i].c_str(), TID_DOUBLE, (void**)&pdata);


              // Will notes that this probably limits the lenght of string to be 100?
              int j=0;
              for (; j<100 && *s ; j++)
                {
                  while (isspace(*s))
                    s++;
                  // convert string to double
                  pdata[j] = strtod(s,&s);
                }
              j = checkBankSize(labview_banks[i].c_str(), j, buf, pdata);
              bk_close(pevent, pdata + j);
              return true; // out of here...
            }
          else
            {
              //no match
              found = false;
            }
          i++;
        }

      // END Search. If we got here, we did not find our string.



      // Did not find the label for the string... ditch this.

      printf("unexpected data: [%s], length %d\n",buf,bufLength);
      
      /* data we do not know what to do with is sent to MIDAS verbatim */
      
      if (false)
        {
          /* init bank structure */
          bk_init32(pevent);
          char* pdata;
          bk_create(pevent, "LABV", TID_CHAR, (void**)&pdata);
          int len = bufLength;
          memcpy(pdata,buf,len);
          pdata += len;
          bk_close(pevent, pdata);
          return true;
        }

      return false;
    }
}

INT read_event(char *pevent, INT off)
{
  //printf("read event\n");

  equipment[0].last_called = 0;

  if (!trySelect(0.1))
    return 0;


  for (int i=0; i<=gNumSockets; i++)
    {
      if (gPtr >= gNumSockets)
        gPtr = 0;

      //printf("try ptr %d\n",gPtr);

      if (gSockets[gPtr] && gSockets[gPtr]->hasNewData)
        {
          // check that the buffered data has the record terminators

          char*end = strstr(gSockets[gPtr]->buf,"\n");
          if (!end)
            end = strstr(gSockets[gPtr]->buf,"\r");

          if (gSockets[gPtr]->isOpen && !end)
            {
              // no record terminators- incomplete
              // data- wait for more data...
              gSockets[gPtr]->hasNewData = false;
              gPtr++;
              continue;
            }

          if (!end)
            end = gSockets[gPtr]->buf + gSockets[gPtr]->bufwptr;

          // zero-terminate the data buffer
          end[0] = 0;

          // kill any preceeding \r and \n
          if (end[-1] == '\n')
            end[-1] = 0;
          else if (end[-1] == '\r')
            end[-1] = 0;

          // kill any following \r and \n
          if (end[1] == '\n')
            end++;
          else if (end[1] == '\r')
            end++;

          int endoffset = end-gSockets[gPtr]->buf;
          int leftover = gSockets[gPtr]->bufwptr - endoffset - 1;

          //printf("socket %d, length %d, end %p, terminated at %d, leftover data %d\n",gPtr,gSockets[gPtr]->bufwptr,end,endoffset,leftover);

          bool doSendEvent = decodeData(pevent,gSockets[gPtr]->buf,endoffset, gSockets[gPtr]->addrinfo);

          if (leftover > 0)
            {
              // move any leftover data to the beginning of
              // the data buffer using memmove() because
              // source and destination do overlap
              memmove(gSockets[gPtr]->buf,end+1,leftover);
              gSockets[gPtr]->buf[leftover] = 0;
              gSockets[gPtr]->bufwptr = leftover;
              gSockets[gPtr]->hasNewData = true;

              printf("leftover bytes %d: [%s]\n",gSockets[gPtr]->bufwptr,gSockets[gPtr]->buf);
            }
          else
            {
              leftover = 0;
              gSockets[gPtr]->buf[leftover] = 0;
              gSockets[gPtr]->bufwptr    = leftover;
              gSockets[gPtr]->hasNewData = false;

              if (!gSockets[gPtr]->isOpen)
                {
                  delete gSockets[gPtr];
                  gSockets[gPtr] = NULL;
                }
            }

          // 
          gPtr++;

          //printf("send event: %d\n",doSendEvent);

          // send the data to MIDAS
          if (doSendEvent)
            return bk_size(pevent);
        }

      gPtr++;
    }
  
  return 0;
}

//end
