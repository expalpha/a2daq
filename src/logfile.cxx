//
// Name: logfile.cxx
// Author: K.Olchanski
// Date: 27 Mar 2006 rewritten as standalone program using parts of clauncher
// Description: Capture program output into a log file
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

const char* ts()
{
  static char buf[1024];
  time_t now = time(NULL);
  strftime(buf,sizeof(buf)-1,"%Y%m%d-%a-%H:%M:%S",localtime(&now));
  return buf;
}

FILE* openLog(const char*logdir,const char*name)
{
  char fname[FILENAME_MAX];
  for (int ilog=0; ; ilog++)
    {
      snprintf(fname,sizeof(fname)-1,"%s/%s.%06d.log.gz",logdir,name,ilog);
      FILE* loggz = fopen(fname,"r");
      if (loggz)
	{
	  fclose(loggz);
	  continue;
	}

      snprintf(fname,sizeof(fname)-1,"%s/%s.%06d.log",logdir,name,ilog);
      FILE* log = fopen(fname,"a");

      if (log == NULL)
	{
	  fprintf(stderr,"%s: logfile: Cannot open log file \"%s\", errno %d (%s)\n",ts(),fname,errno,strerror(errno));
	  exit(1);
	}

      if (ftell(log) == 0)
	{
	  setbuf(log,0);
	  fprintf(log,"%s: logfile: Log file open\n",ts());
	  return log;
	}

      fclose(log);
    }
}

void closeLog(FILE*log)
{
  fprintf(log,"%s: logfile: Log file closed\n",ts());
  fclose(log);
}

int main(int argc,char* argv[])
{
  signal(SIGPIPE,SIG_IGN); // writes to disconnected sockets return EPIPE rather than die.
  setbuf(stdout,0); // no buffering for writes to stdout
  setbuf(stderr,0); // no buffering for writes to stderr

  if (argc <2)
    {
      fprintf(stderr,"Usage: logfile [--once] [--maxsize numbytes] [--logdir dirname] [--name logname] program...\n");
      exit(1);
    }

  bool once = false;
  int minRunTime = 10;
  char* name = NULL;
  char* logdir = "logs/";
  int maxSize = 1*1024*1024;

  char** argptr = argv;
  for (int i=1; i<argc; i++)
    {
      if (strcasecmp(argv[i],"--once")==0)
	{
	  once = true;
	}
      else if (strcasecmp(argv[i],"--maxsize")==0)
	{
	  i++;
	  maxSize = atoi(argv[i]);
	}
      else if (strcasecmp(argv[i],"--logdir")==0)
	{
	  i++;
	  logdir = argv[i];
	}
      else if (strcasecmp(argv[i],"--name")==0)
	{
	  i++;
	  name = argv[i];
	}
      else if (argv[i][0]=='-')
	{
	  // skip unknown command line switches
	}
      else
	{
	  argptr = argv+i;
	  break;
	}
    }

  if (0)
    {
      for (int i=0; argptr[i]; i++)
	printf("arg %d is \"%s\"\n",i,argptr[i]);
    }

  if (name == NULL)
    name = argptr[0];

  int shortRunCounter = 0;

  while (1)
    {
      int byteCount = 0;
      FILE *log = openLog(logdir,name);

      // start the program

      int fd[2];
      
      if (pipe(fd) != 0)
	{
	  fprintf(log,"%s: logfile: pipe() error, errno %d (%s)",ts(),errno,strerror(errno));
	  closeLog(log);
	  exit(1);
	}

      int parentpid = getpid();
      int pid = fork();
      if (pid == 0)
	{
	  // I am the child

	  // make our parent be our parent
	  setpgid(getpid(),parentpid);

	  // I will not use the read-side of my pipe
	  close(fd[0]);

	  // I will direct stdin to /dev/null
	  int devnull = open("/dev/null",O_RDONLY);
	  dup2(devnull,0);

	  // I will not use the original file descriptor
	  close(devnull);

	  // I will direct stdout to the write-side of my pipe
	  dup2(fd[1],1);
	  dup2(fd[1],2);

	  // I will not use the original write-side file descriptor
	  close(fd[1]);

	  int err = execvp(argptr[0],argptr);
	  char buf[1024];
	  if (err != 0)
	    snprintf(buf,sizeof(buf),"logfile: Cannot exec(%s), errno %d (%s)\n",argptr[0],errno,strerror(errno));
	  else
	    snprintf(buf,sizeof(buf),"logfile: exec(%s) returned %d\n",argptr[0],err);
	  write(2,buf,strlen(buf));
	  
	  exit(1);
	}

      // I am the parent

      // I will not use the write-side of my pipe
      close(fd[1]);
      
      time_t startTime = time(NULL);
      int childPid = pid;
      int childFd  = fd[0];
      
      fprintf(log,"%s: logfile: Child started, pid %d\n",ts(),childPid);
      
      while (1)
	{
	  char buf[10240];
	  int rd = read(childFd,buf,sizeof(buf));
	  if (rd < 0)
	    {
	      fprintf(log,"%s: logfile: read() error, errno %d (%s)\n",ts(),errno,strerror(errno));
	      break;
	    }

	  if (rd == 0)
	    {
	      fprintf(log,"%s: logfile: Finished\n",ts());
	      break;
	    }

	  buf[rd] = 0;
	  fprintf(log,"%s: %s",ts(),buf);
	  //fwrite(buf,rd,1,log);

	  byteCount += rd;

	  if (byteCount > maxSize)
	    {
	      fprintf(log,"%s: logfile: Reopened upon reaching %d bytes out of max %d bytes\n",ts(),byteCount,maxSize);
	      closeLog(log);
	      log = openLog(logdir,name);
	      byteCount = 0;
	    }
	}

      time_t endTime = time(NULL);
      int runTime = endTime - startTime;

      if (runTime < minRunTime)
	{
	  shortRunCounter ++;
	  fprintf(log,"%s: logfile: Running time: %d sec (finished faster than %d sec, attempt %d)\n",ts(),runTime,minRunTime,shortRunCounter);
	}
      else
	{
	  shortRunCounter = 0;
	  fprintf(log,"%s: logfile: Running time: %d sec\n",ts(),runTime);
	}

      closeLog(log);
      log = NULL;

      if (once)
	break;

      if (shortRunCounter > 10)
	break;
    }

  return 0;
}

// end file
