// writedisable.cxx
// Utility to write disable bits to be read by the VA1TA config file class
// $Id: $

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <ctype.h>
using namespace std;

#define FRCstart 0
#define FRCend   15
#define numPorts 4
#define numASICs 4
#define numTAs   128
#define low  0
#define high 1

int writeAllZero = 0;
int writeAllOne  = 0;
int disableNsides   = 0;
int disableIndividualStrips = 0 ;

int FRC = -1;

struct DisableStrips
{
  int frc ;
  int port ;
  int asic ;
  int strips[numTAs] ;
  
  DisableStrips()
  {
    frc = 0 ; 
    port = 0 ; 
    asic = 0 ;
    for ( int i = 0 ; i < numTAs ; i++ ) strips[i] = low ;
  } 
  
  void Print()
  {
    cout << " disable frc " << frc
         << " port " << port 
         << " asic " << asic << endl;
    for( int i = 1; i < numTAs+1; i++)
	  {
	    cout << " strip "  << i << " : " << strips[i-1] << endl ;
	  }
    cout << endl;
  }
};

int main(int argc, char*argv[])
{
   char * outfilename = "out";
   char * disableStripsFilename = new char[120] ;
   // command line args
   for (int i=1; i<argc; i++)
    {
        if (strcmp(argv[i],"--out")==0)
          {
            outfilename = argv[i+1];
	    //printf("Output File Name: %s\n",argv[i+1]);
	    i++;
          }
        else if ( strcmp(argv[i],"--disableStrips" ) == 0 )
        {
           disableStripsFilename = argv[i+1] ;
           disableIndividualStrips = 1 ;
           i++ ;  
        }
        else if (strcmp(argv[i],"--writeAllZero")==0)
            writeAllZero = 1;
        else if (strcmp(argv[i],"--writeAllOne")==0)
            writeAllOne  = 1;
        else if (strcmp(argv[i],"--disableNsides")==0)
            disableNsides  = 1;
        else if (strcmp(argv[i],"--frc")==0)
          {
            FRC = strtoul(argv[i+1], NULL, 0);
	    //printf("Output File  Name: %s\n",argv[i+1]);
	    i++;
          }
        else if (strcmp(argv[i],"--help")==0)
          {
            printf("./writedisable.exe [args]\n");
            printf("\nArguments\n");
            printf("--out [filename]    Output filename\n");
            printf("--disableStrips[filename] disable filename\n");
            printf("--writeAllZero      Write all the disable bits to 0\n");
            printf("--writeAllOne       Write all the disable bits to 1\n");
            printf("--disableNsides     Make the N disable bits 1\n");
            printf("--frc [address]     Disable all of the that FRC\n");
            exit(0);
          }
    }
// 
//   
  if( outfilename == NULL )
   {
     printf("Improper filename!\n");
     exit(0);
   }
//   printf("Output File Name: %s\n",outfilename);
// 
//   // open outputfiledisableStripsFilename

   ofstream outfile;
   outfile.open(outfilename);
// 
   if( !outfile.is_open() )
    {
      printf("Cannot open output file!\n");
      exit(0);
    }
// 
//   // write all zeros
  if(writeAllZero)
   {
   for( int ifrc = FRCstart; ifrc <= FRCend; ifrc++ )
    for( int iport = 0; iport < numPorts; iport++)
     for( int iASIC = 1; iASIC <= numASICs; iASIC++)
      {
       outfile << ifrc << "  " << iport << "  " << iASIC << "  ";
       for( int ichan = 0; ichan < numTAs; ichan++)
        {      
	  outfile << low << "  ";
        }
       outfile << endl;
     }
   exit(0);
   }
// 
//   // write all ones
  else if(writeAllOne)
   {
   for( int ifrc = FRCstart; ifrc <= FRCend; ifrc++ )
    for( int iport = 0; iport < numPorts; iport++)
     for( int iASIC = 1; iASIC <= numASICs; iASIC++)
      {
       outfile << ifrc << "  " << iport << "  " << iASIC << "  ";
       for( int ichan = 0; ichan < numTAs; ichan++)
        {      
	  outfile << high << "  ";
        }
       outfile << endl;
     }
    exit(0);
    }
// 
//   // disable all n sides
  else if(disableNsides)
   {
   for( int ifrc = FRCstart; ifrc <= FRCend; ifrc++ )
    for( int iport = 0; iport < numPorts; iport++)
     for( int iASIC = 1; iASIC <= numASICs; iASIC++)
      {
       outfile << ifrc << "  " << iport << "  " << iASIC << "  ";
       if( iASIC == 1 || iASIC == 2)
        for( int ichan = 0; ichan < numTAs; ichan++)
        {      
	  outfile << high << "  ";
        }
       else
        for( int ichan = 0; ichan < numTAs; ichan++)
        {      
	  outfile << low << "  ";
        }
       outfile << endl;
     }
    exit(0);
   }
// 
//   // disable a specific FRC
  else if ( FRC != -1 )
  {
   for( int ifrc = FRCstart; ifrc <= FRCend; ifrc++ )
    for( int iport = 0; iport < numPorts; iport++)
     for( int iASIC = 1; iASIC <= numASICs; iASIC++)
      {
       outfile << ifrc << "  " << iport << "  " << iASIC << "  ";

       if ( ifrc == FRC)
        for( int ichan = 0; ichan < numTAs; ichan++)
         {      
	   outfile << high << "  ";
         }
       else
        for( int ichan = 0; ichan < numTAs; ichan++)
         {      
	   outfile << low << "  ";
         }
       outfile << endl;
     }
   }
// 
// 
//   // disable strips specified in a file
   else if ( disableIndividualStrips )
   {
//   
//  Parse the disable strips file
//  ----------------------------------------------------------
   ifstream stripSelectionFile ;
   stripSelectionFile.open(disableStripsFilename) ;
   string line ;
  
   int NumberLines = 0 ;
   int NumberDisabledStrips = 0 ;
   int counter = 0 ;
     
   int* stripValue  = new int[NumberDisabledStrips] ;
   DisableStrips* StripsToDisable = new DisableStrips[NumberLines] ;
  
   for ( int i = 0 ; i < numTAs ; i++ )
     StripsToDisable[NumberLines].strips[i] = low ;

    while( getline(stripSelectionFile,line) )     
       {
		if(line.size() == 0 ) continue;
     		for ( int i = 0 ; i < line.size() ; i++ )
      		{
			if( counter == 0 && isdigit( line[i]  ) ) 
			{
				StripsToDisable[NumberLines].frc = atoi(&line[i]) ;
                                if( isdigit( line[i+1] ) ) // hack get FRC > 9 to work
                                  i++;
				counter++ ;
			}
			else if ( counter == 1 && isdigit( line[i] ) ) 
			{
				StripsToDisable[NumberLines].port = atoi(&line[i]);
				if( isdigit( line[i+1] ) ) // hack get FRC > 9 to work
                                  i++;
				counter++ ;
			} 
			else if ( counter == 2 && isdigit( line[i] ) )
			{
				StripsToDisable[NumberLines].asic = atoi(&line[i]) ;
				if( isdigit( line[i+1] ) ) // hack get FRC > 9 to work
                                  i++;
				counter++ ;
			}
			else if ( (counter >2) && isdigit( line[i] ) )
			{
			
				stripValue[NumberDisabledStrips] =atoi(&line[i])  ;
				if( isdigit( line[i+1] ) ) // hack to get double digits
                                  i++;
				if( isdigit( line[i+1] ) ) // hack to get triple digits
                                  i++;
				int s = stripValue[NumberDisabledStrips]  ;
				StripsToDisable[NumberLines].strips[s-1] = high ;
				NumberDisabledStrips++ ;
				counter++;
			}

                 }
          
//          
// 	      
         	for ( int i = 1 ; i < numTAs+1 ; i++ )
         	{
         		for ( int j = 0	 ; j < NumberDisabledStrips ; j++ ) 
         		{
         			if ( i == stripValue[j] ) 
         		  		StripsToDisable[NumberLines].strips[i-1] = high ;
         		}	
         	}
// 
     		//StripsToDisable[NumberLines].Print() ;
     		NumberLines++;
     		NumberDisabledStrips = 0 ; 
     		counter = 0 ;   	
	}

// 
//    ----------------------------------------------------------
//    
//    //Create the disable file which is read by load_all

       for( int ifrc = FRCstart; ifrc <= FRCend; ifrc++ )
        for( int iport = 0; iport < numPorts; iport++)
         for( int iASIC = 1; iASIC <= numASICs; iASIC++)
          {
                 outfile << ifrc << "  " << iport << "  " << iASIC << "  ";
  	        for ( int i = 0 ; i<NumberLines ; i++ )
  	        {
  	          if ( ifrc == StripsToDisable[i].frc && iport == StripsToDisable[i].port 
 			     && iASIC == StripsToDisable[i].asic )
  	          {
  	            for( int ichan = 0; ichan < numTAs; ichan++)
  	            {
  		              outfile << StripsToDisable[i].strips[ichan] << " " ;
  	            }
                    outfile << endl <<"#" << endl;
                    goto end;  // found an exception, so continue on
  	          }
                }

                for( int ichan = 0; ichan < numTAs; ichan++)
  	             {      
  		 	       outfile << low << "  ";
  	             }
  	        outfile << endl <<"#"<< endl;
 
                end:
                 continue; // hack to continue out of the loop
 
        } 
  }
 
   else 
   {
   for( int ifrc = FRCstart; ifrc <= FRCend; ifrc++ )
    for( int iport = 0; iport < numPorts; iport++)
     for( int iASIC = 1; iASIC <= numASICs; iASIC++)
      {
       outfile << ifrc << "  " << iport << "  " << iASIC << "  ";

       if ( ifrc == FRC && iport == 2 )
        for( int ichan = 0; ichan < numTAs; ichan++)
         {      
	   outfile << high << "  ";
         }
       else
        for( int ichan = 0; ichan < numTAs; ichan++)
         {      
	   outfile << low << "  ";
         }
       outfile << endl;
     }
   
    exit(0) ;
   }
 outfile.close();
 return 0;
} 
