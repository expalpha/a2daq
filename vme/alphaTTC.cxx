//
// Name: alphaTTC.cxx
// Author: K.Olchanski
// Date: 2 May 2007
// Description: driver for the ALPHA trigger and control TTC board
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#include "alphaTTC.h"
#include "mvmestd.h"

#if 0
static int xsleep(int x)
{
  int y = 1;
  for (int i=0; i<x; i++)
    y *= 3;
  return y;
}
#endif

#if 0
static int xudelay(AlphaTTC*ttc, int usec)
{
  int status = 0;
  for (int i=0; i<usec; i++)
    status |= ttc->read32(0);
  return status;
}
#endif

uint32_t AlphaTTC::read32(int ireg)
{
  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A32_ND);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D32);
  return mvme_read_value((MVME_INTERFACE*)fVme, fVmeBaseAddr + ireg);
}

void AlphaTTC::write32(int ireg, uint32_t data)
{
  //if (ireg == 0xAC)
  //printf("Write reg 0x%08x value 0x%08x\n", ireg, data);

  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A32_ND);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D32);
  mvme_write_value((MVME_INTERFACE*)fVme, fVmeBaseAddr + ireg, data);
}

void AlphaTTC::writeTriggerFPGA(int ifpga, int ireg, int value)
{
  assert(ifpga>=0 && ifpga<=1);
  assert(ireg>=0 && ireg<=255);
  assert(value>=0 && value<=0xFFFF);
  write32(0x88, (ifpga<<24) | (ireg<<16) | value);
}

uint32_t AlphaTTC::readTriggerFPGA(int ifpga, int ireg)
{
  assert(ifpga>=0 && ifpga<=1);
  assert(ireg>=0 && ireg<=255);
  write32(0x88, 0x0200FFFF | (ifpga<<24) | (ireg<<16));
  return read32(0x8C);
}

void AlphaTTC::setBits32(int ireg, uint32_t bits)
{
  write32(ireg, read32(ireg) | bits);
}

void AlphaTTC::clearBits32(int ireg, uint32_t bits)
{
  write32(ireg, read32(ireg) & ~bits);
}

void AlphaTTC::invertBits32(int ireg, uint32_t bits)
{
  write32(ireg, read32(ireg) ^ bits);
}

void AlphaTTC::writeCommand(int command)
{
   write32(0xCC, command);
}

bool AlphaTTC::isPresent(void* vmeInterface, uint32_t vmeA32addr) // check for presence
{
   mvme_set_am((MVME_INTERFACE*)vmeInterface, MVME_AM_A32_ND);
   mvme_set_dmode((MVME_INTERFACE*)vmeInterface, MVME_DMODE_D32);
   uint32_t fwrev = mvme_read_value((MVME_INTERFACE*)vmeInterface, vmeA32addr + 0x48);
   if (fwrev == 0xFFFFFFFF)
      return false;
   if ((fwrev & 0xFF000000) != 0x7F000000)
      return false; // old revision, not compatible
   if (fwrev < 0x7F120600)
      return false; // old revision, not compatible
   return true;
}

AlphaTTC::AlphaTTC(void* vme, uint32_t vmeA32addr)
{
  fVme = vme;
  fVmeBaseAddr = vmeA32addr;
  fDebug = 0;
  fVerifyWrite = 1;

  fTaEnableBitmapIsWritten = false;
  memset(fTaEnableBitmap, 0, sizeof(fTaEnableBitmap));

  uint32_t reg48 = read32(0x48);

  printf("AlphaTTC: VME A32 addr: 0x%08x, reg48 revision: 0x%08x (%d)\n",
         vmeA32addr,
         reg48,
         reg48);

  fRevision = reg48;
};

const int holdDelayScale =  50; // "ns"
const int adcDelayScale  =  50; // "ns"

static int CmpBits(AlphaTTC* ttc, int ireg, uint32_t wvalue, uint32_t expected)
{
   ttc->write32(ireg, wvalue);
   uint32_t rvalue = ttc->read32(ireg);
   //uint32_t rvalue1 = ttc->read32(ireg);
   //uint32_t rvalue2 = ttc->read32(ireg);
   //printf("values 0x%08x 0x%08x 0x%08x\n", rvalue, rvalue1, rvalue2);
   if (rvalue != expected) {
      printf("AlphaTTC::init: bits mismatch: write 0x%08x, read 0x%08x, expected 0x%08x, diff 0x%08x\n", wvalue, rvalue, expected, (rvalue ^ expected));
      return 1;
   }
   return 0;
}

static int TestBadBits(AlphaTTC* ttc, int ireg)
{
   int status = 0;
   uint32_t val = ttc->read32(ireg);
   
   printf("AlphaTTC::init: testing bad bits using register 0x%x, preserved value 0x%08x\n", ireg, val);

   for (int i=0; i<1; i++)
      status |= CmpBits(ttc, ireg, 0, 0);

   for (int i=0; i<1; i++)
      status |= CmpBits(ttc, ireg, 0xffffffff, 0xffffffff);

   for (int i=0; i<32; i++)
      status |= CmpBits(ttc, ireg, 1<<i, 1<<i);

   for (int i=0; i<32; i++)
      status |= CmpBits(ttc, ireg, ~(1<<i), ~(1<<i));

   if (status == 0)
      printf("AlphaTTC::init: testing bad bits using register 0x%x: test passed\n", ireg);
   else
      printf("AlphaTTC::init: testing bad bits using register 0x%x: test FAILED!!!\n", ireg);

   ttc->write32(ireg, val);

   return status;
}

int AlphaTTC::init()
{
  uint32_t stat;
  uint32_t reg48 = read32(0x48);

  printf("AlphaTTC::init: TTC at VME A32 0x%08x firmware revision 0x%08x (%d)\n", fVmeBaseAddr, reg48, reg48);

  if (reg48==0xFFFFFFFF)
    {
      printf("AlphaTTC is not responding.\n");
      exit(1);
      // NOT REACHED
    }

  fRevision = reg48;

  if (fRevision < 0x7f120627) {
     printf("AlphaTTC::init: TTC firmware revision 0x%08x is not usable.\n", reg48);
     exit(1);
  }

  if (TestBadBits(this, 0xb0)) {
     printf("AlphaTTC::init: TTC register read-write test failed.\n");
     if (fVmeBaseAddr != 0x42000000)
        exit(1);
  }

  writeTaEnableBitmap();
  
  writeCommand(TTC_CMD_ClearCounters);
  
  writeTriggerFPGA(0, 0, 0x0);
  writeTriggerFPGA(1, 0, 0x0);
  
  writeTriggerFPGA(0, 1, 0x0);
  writeTriggerFPGA(1, 1, 0x0);
  
  vmeClearTriggerFPGA();
  
  write32(0xAC, 0); // enable mux sequencer
  write32(0xB0, 0); // disable self-triggering (revision 2008050301 or later)
  write32(0xBC, 10); // busy extend
  //write32(0xC0, 0); // general control
  write32(0xD0, 0); // boot control

  // clear 0xC0 general control without disturbing the VF48 clock
  stat = read32(0xC0);
  if (stat & TTC_C0_EXTERNAL_MUX_CLK) {
     write32(0xC0, TTC_C0_EXTERNAL_MUX_CLK); // general control
  } else {
     write32(0xC0, 0); // general control
  }
  
  enableReset(true);
  
  setAdcDelay(0);
  setHoldDelay(0);
  writeDac(0, 0); // DACa
  writeDac(1, 0); // DACb
  writeDac(2, 1); // DAC reference - internal
  
  stat = read32(0xC4);
  bool unlocked = false;
  if (stat & TTC_C4_PLL1_UNLOCKED) {
     unlocked = true;
     printf("PLL1 unlocked status bit is set!\n");
  }
  if (stat & TTC_C4_PLL2_UNLOCKED) {
     unlocked = true;
     printf("PLL2 unlocked status bit is set!\n");
  }
  
  setBits32(0xC0, TTC_C0_CLEAR_PLL_UNLOCKED_LATCH);
  clearBits32(0xC0, TTC_C0_CLEAR_PLL_UNLOCKED_LATCH);
  
  stat = read32(0xC4);
  if (stat & TTC_C4_PLL1_UNLOCKED) {
     printf("PLL1 is not locked!\n");
  } else {
     printf("PLL1 is locked!\n");
  }
  if (stat & TTC_C4_PLL2_UNLOCKED) {
     printf("PLL2 is not locked!\n");
  } else {
     printf("PLL2 is locked!\n");
  }

  //status();

  return 0;
};

int AlphaTTC::setInternalClock()
{
   clearBits32(0xC0, TTC_C0_EXTERNAL_MUX_CLK);
   return 0;
}

int AlphaTTC::setExternalClock()
{
   uint32_t stat = read32(0xC4);

   if (stat & TTC_C4_PLL1_UNLOCKED) {
      printf("PLL1 (external clock) unlocked status bit is set, trying to clear!\n");
      
      setBits32(0xC0, TTC_C0_CLEAR_PLL_UNLOCKED_LATCH);
      clearBits32(0xC0, TTC_C0_CLEAR_PLL_UNLOCKED_LATCH);
     
      stat = read32(0xC4);
      if (stat & TTC_C4_PLL1_UNLOCKED) {
         printf("PLL1 (external clock) is not locked!\n");
         return -1;
      }

      printf("PLL1 (external clock) is locked!\n");
   }

   setBits32(0xC0, TTC_C0_EXTERNAL_MUX_CLK);

   return 0;
}

void AlphaTTC::ledTest()
{
   setBits32(0xC0, TTC_C0_LED_TEST);
   sleep(1);
   clearBits32(0xC0, TTC_C0_LED_TEST);
}

static int addBits(int maxBits, int ibit, char bits[], int value, int valueBits)
{
  bool bigEndian = true;

  if (bigEndian)
    for (int i=valueBits-1; i>=0; i--)
      {
        assert(ibit < maxBits);
        bits[ibit++] = (value & (1<<i))?1:0;
      }
  else
    for (int i=0; i<valueBits; i++)
      {
        assert(ibit < maxBits);
        bits[ibit++] = (value & (1<<i))?1:0;
      }

  return ibit;
}

int AlphaTTC::EncodeBits(int maxBits, char bits[], const Va1taConfig& config)
{
  int ibit = 0;

  ibit = addBits(maxBits, ibit, bits, config.obi, 3);
  ibit = addBits(maxBits, ibit, bits, config.ibuf, 3);
  ibit = addBits(maxBits, ibit, bits, config.pre_bias, 3);
  ibit = addBits(maxBits, ibit, bits, config.sbi, 3);
  ibit = addBits(maxBits, ibit, bits, config.vrc, 3);
  ibit = addBits(maxBits, ibit, bits, config.ifp, 3);
  ibit = addBits(maxBits, ibit, bits, config.ifsf, 3);
  ibit = addBits(maxBits, ibit, bits, config.ifss, 3);
  ibit = addBits(maxBits, ibit, bits, config.sha_bias, 3);
  ibit = addBits(maxBits, ibit, bits, config.vthr, 5);
  ibit = addBits(maxBits, ibit, bits, config.twbi, 3);
  for (int i=0; i<128; i++)
    ibit = addBits(maxBits, ibit, bits, config.trim_dac[i], 4);
  for (int i=0; i<128; i++)
    ibit = addBits(maxBits, ibit, bits, config.disable[i], 1);
  ibit = addBits(maxBits, ibit, bits, config.test_on, 1);
  ibit = addBits(maxBits, ibit, bits, config.r2, 1);
  ibit = addBits(maxBits, ibit, bits, config.r1, 1);
  ibit = addBits(maxBits, ibit, bits, config.nside, 1);
  ibit = addBits(maxBits, ibit, bits, config.tp300, 1);

  return ibit;
}

int AlphaTTC::writeConfigurationBits(int ifrc, int ifrcPort, int numBits, const char* bits[4])
{
  assert(ifrc>=0);
  assert(ifrc<16);

  assert(ifrcPort>=0);
  assert(ifrcPort<4);

  int frcAddr = ifrc | (ifrcPort<<4);

  int debug = fDebug;
  fDebug = 0;

  if (fDebug)
    printf("Write VA1TA configuration using reg 0xD0, %d bits, frc %d, chan %d, frcAddr 0x%02x\n", numBits, ifrc, ifrcPort, frcAddr);

  assert(numBits == 680);

  if (fDebug)
    printf("Writing...\n");

  write32(0xD0, 0);

  setBits32(0xD0, frcAddr<<8);
  setBits32(0xD0, TTC_LOAD);
  setBits32(0xD0, 1); // boot enable
  read32(0xD0);

  for (int i=0; i<4; i++)
    for (int j=0; j<numBits; j++)
      {
        if (bits[i][j])
          setBits32(0xD0, TTC_XDATA);
        else
          clearBits32(0xD0, TTC_XDATA);
        
        //xudelay(this,1);
        setBits32(0xD0, TTC_XCLK);
        //xudelay(this,1);
        clearBits32(0xD0, TTC_XCLK);
      }

  clearBits32(0xD0, TTC_LOAD);
  clearBits32(0xD0, 1); // boot enable
  read32(0xD0);

  write32(0xD0, 0);
  read32(0xD0);

  if (fDebug)
    printf("Write done\n");

  fDebug = debug;

  return 0;
}

int AlphaTTC::readConfigurationBits(int ifrc, int ifrcPort, char bits[4][680])
{
  assert(ifrc>=0);
  assert(ifrc<16);

  assert(ifrcPort>=0);
  assert(ifrcPort<4);

  int frcAddr = ifrc | (ifrcPort<<4);

  int debug = fDebug;
  fDebug = 0;

  write32(0xD0, 0);
  setBits32(0xD0, frcAddr<<8);

  setBits32(0xD0, TTC_READ);
  read32(0xD0);
  read32(0xD0);
  clearBits32(0xD0, TTC_READ);

  setBits32(0xD0, 1); // boot enable
  read32(0xD0);

  for (int i=0; i<4; i++)
    for (int j=0; j<680; j++)
      {
        int rd1 = read32(0xC4);
        setBits32(0xD0, TTC_XCLK);
        //xudelay(this,1);
        clearBits32(0xD0, TTC_XCLK);
        //xudelay(this,1);
        //int rd2 = read8(1);
        //printf("chip %d, bit %d: 0x%x 0x%x\n", i, j, rd1&0x4, rd2&0x4);
        
        bits[i][j] = (rd1&4)?1:0;
      }

  //clearBits32(0xD0, TTC_LOAD);  // jws 24/10/2009
  clearBits32(0xD0, 1); // boot enable
  read32(0xD0);

  write32(0xD0, 0);
  read32(0xD0);

  fDebug = debug;
      
  return 0;
}

int AlphaTTC::writeHybridConfig(int ifrc, int ifrcPort, const HybridConfig* cfg)
{
  const int kNumBits = 1000;
  char bits1[kNumBits];
  char bits2[kNumBits];
  char bits3[kNumBits];
  char bits4[kNumBits];
  char rbits[4][680];

  int numBits;
  numBits = EncodeBits(kNumBits, bits1, cfg->cfg[0]);
  numBits = EncodeBits(kNumBits, bits2, cfg->cfg[1]);
  numBits = EncodeBits(kNumBits, bits3, cfg->cfg[2]);
  numBits = EncodeBits(kNumBits, bits4, cfg->cfg[3]);
  
  const char* bitsPtr[4] = { bits4, bits3, bits2, bits1 };

  writeConfigurationBits(ifrc, ifrcPort, numBits, bitsPtr);

  bool noHybrid = true;
  bool noPower  = true;

  if (true)
    {
      // to read back the configuration bits, we have to run the shift register twice:
      // first time to shift out the data we just shifted in,
      // second time to shift out the data latched in the VA1TA configuration bits

      readConfigurationBits(ifrc, ifrcPort, rbits);
      readConfigurationBits(ifrc, ifrcPort, rbits);

      bool stuck0[4];
      bool stuck1[4];
      int  cmp[4];

      bool allSuccess = true;

      for (int c=0; c<4; c++)
        {
          stuck0[c] = true;
          stuck1[c] = true;
          cmp[c] = 0;
	  
          int rc = c;
	  
          for (int i=0; i<680; i++)
            {
              stuck0[c] &= (rbits[rc][i] == 0);
              stuck1[c] &= (rbits[rc][i] != 0);
	      
              if (rbits[rc][i] != bitsPtr[c][i])
                {
                  //printf("chip %d, bit %d, data mismatch: %d should be %d\n", c, i, rbits[rc][i], bitsPtr[c][i]);
                  cmp[c]++;
                }
            }

          if (stuck0[c])
            {
              //printf("BOOT ERROR: FRC %d, port %d, chip %d: data is stuck at zero\n", ifrc, ifrcPort, c+1);
              allSuccess = false;
            }
          else if (stuck1[c])
            {
              //printf("BOOT ERROR: FRC %d, port %d, chip %d: data is stuck at one\n", ifrc, ifrcPort, c+1);
              allSuccess = false;
            }
          else if (cmp[c])
            {
              printf("BOOT ERROR: FRC %d, port %d, chip %d: %d bit errors:\n", ifrc, ifrcPort, c+1, cmp[c]);
              int count = 0;
              for (int i=0; i<680; i++)
                 if (rbits[rc][i] != bitsPtr[c][i]) {
                    count++;
                    if (count < 20)
                       printf("bit %d, data mismatch: %d should be %d\n", i, rbits[rc][i], bitsPtr[c][i]);
                 }
              allSuccess = false;
            }
          else
            {
              //printf("BOOT SUCCESS: FRC %d, port %d, chip %d.\n", ifrc, ifrcPort, c+1);
            }

          noHybrid &= stuck1[c];
          noPower  &= stuck0[c];
        }

      if (noHybrid)
        {
          printf("BOOT ERROR:   FRC %d, port %d, all data stuck at 1, no hybrid connected to this port?\n", ifrc, ifrcPort);
          return -1;
        }

      if (noPower)
        {
          printf("BOOT ERROR:   FRC %d, port %d, all data stuck at 0, no power?\n", ifrc, ifrcPort);
          return -1;
        }

      if (allSuccess)
        {
          printf("BOOT SUCCESS: FRC %d, port %d.\n", ifrc, ifrcPort);
	  return 0;
        }
      else
        {
          for (int c=0; c<4; c++)
            {
              if (stuck0[c])
                printf("BOOT ERROR:   FRC %d, port %d, chip %d: data is stuck at zero\n", ifrc, ifrcPort, c+1);
              else if (stuck1[c])
                printf("BOOT ERROR:   FRC %d, port %d, chip %d: data is stuck at one\n", ifrc, ifrcPort, c+1);
              else if (cmp[c])
                printf("BOOT ERROR:   FRC %d, port %d, chip %d: %d bit errors (see above)\n", ifrc, ifrcPort, c+1, cmp[c]);
              else
                printf("BOOT SUCCESS: FRC %d, port %d, chip %d.\n", ifrc, ifrcPort, c+1);
            }
        }
    }
      
  return -1;
}

void AlphaTTC::vmeTrigger()
{
   writeCommand(TTC_CMD_Trigger);
}

void AlphaTTC::calPulse()
{
   writeCommand(TTC_CMD_CalPulse);
}

void AlphaTTC::vmeLatch(int xfpga)
{
   writeCommand(TTC_CMD_LatchTrigger);
}

void AlphaTTC::vmeClearTriggerFPGA(int xfpga)
{
  for (int ifpga=0; ifpga<2; ifpga++)
    if (xfpga<0 || ifpga==xfpga)
      {
	uint32_t reg1 = 0xFFFF & readTriggerFPGA(ifpga, 1);
	writeTriggerFPGA(ifpga, 1, reg1 | (1<<0));
	writeTriggerFPGA(ifpga, 1, reg1 & ~(1<<0));
      }
}

int AlphaTTC::setHoldDelay(int delay_ns)
{
  int value = delay_ns/holdDelayScale;

  if (value == 0)
     setBits32(0xC0, TTC_C0_ENABLE_FAST_HOLD);
  else
     clearBits32(0xC0, TTC_C0_ENABLE_FAST_HOLD);

  if (value < 1)
     value = 1;
  if (value > 0xFF)
     value = 0xFF;

  clearBits32(0xC0, 0xFF << 16);
  setBits32(0xC0, value << 16);

  printf("Set HOLD delay: requested %d ns, got %d ns\n", delay_ns, value*holdDelayScale);
  return value;
}

int AlphaTTC::setAdcDelay(int delay_ns)
{
  int value = delay_ns/adcDelayScale;

  if (value < 1)
     value = 1;
  if (value > 0xFF)
     value = 0xFF;

  clearBits32(0xC0, 0xFF << 24);
  setBits32(0xC0, value << 24);

  printf("Set ADC delay: requested %d ns, got %d ns\n", delay_ns, value*adcDelayScale);
  return value;
}

int AlphaTTC::setBusyDelay(int delay_ns)
{
  int value = delay_ns/adcDelayScale;

  if (value < 1)
     value = 1;
  if (value > 0xFFFF)
     value = 0xFFFF;

  write32(0xBC, value); // busy extend

  printf("Set BUSY delay: requested %d ns, got %d ns\n", delay_ns, value*adcDelayScale);
  return value;
}

int AlphaTTC::enableReset(bool enable)
{
   if (enable)
      setBits32(0xAC, TTC_AC_RESET_ENA);
   else
      clearBits32(0xAC, TTC_AC_RESET_ENA);
   return 0;
}

int AlphaTTC::enableHold(bool enable)
{
   if (enable)
      setBits32(0xAC, TTC_AC_HOLD_ENA);
   else
      clearBits32(0xAC, TTC_AC_HOLD_ENA);
   return 0;
}

int AlphaTTC::setMuxTrigSource(int source)
{
   clearBits32(0xAC, 3<<10);
   setBits32(0xAC, (source&3)<<10);
   return 0;
}

int AlphaTTC::setTestMode(int testChan, bool extEna, bool trigEna, bool seqEna)
{
   printf("Setting test mode for channel %d using register 0xAC !!!\n", testChan);
   
   //sleep(10);
   
   bool enaTrigOut = disableTrigOut(); // disable trigger outputs
   bool enaTrigIn = disableTrigIn(); // disable trigger inputs EXT_IN and VETO_IN
   
   //write32(0xAC, TTC_AC_SEQ_DIS|TTC_AC_HOLD_POL|TTC_AC_RESET_POL|TTC_AC_SHIN_POL);
   //setBits32(0xAC, TTC_AC_HOLD_POL|TTC_AC_RESET_POL|TTC_AC_SHIN_POL);
   setBits32(0xAC, TTC_AC_SEQ_DIS);

   if (!seqEna) {
      clearBits32(0xAC, 3<<8);  setBits32(0xAC, 2<<8);  // VME command cal_pulse fires the ADC trigger
   } else {
      clearBits32(0xAC, 3<<8);  setBits32(0xAC, 0<<8);  // HOLD delay fires the ADC trigger
   }

   clearBits32(0xAC, 3<<10); setBits32(0xAC, 1<<10); // self trigger fires the MUX
   clearBits32(0xAC, 3<<12); setBits32(0xAC, 3<<12); // VME command cal_pulse fires the calibration pulser

   if (extEna) {
      clearBits32(0xAC, 3<<10); setBits32(0xAC, 0<<10); // external trigger fires the MUX
   } else if (trigEna) {
      clearBits32(0xAC, 3<<10); setBits32(0xAC, 3<<10); // VME command cal_pulse fires the MUX
   }
   
   //fDebug = false;
   
   read32(0x48);
   read32(0x48);
   invertBits32(0xAC, TTC_AC_RESET_POL);
   read32(0x48);
   read32(0x48);
   invertBits32(0xAC, TTC_AC_RESET_POL);
   
   read32(0x48);
   read32(0x48);
   invertBits32(0xAC, TTC_AC_CKB_POL);
   //read32(0x48);
   //read32(0x48);
   invertBits32(0xAC, TTC_AC_SHIN_POL);
   //read32(0x48);
   //read32(0x48);
   invertBits32(0xAC, TTC_AC_SHIN_POL);
   //read32(0x48);
   //read32(0x48);
   invertBits32(0xAC, TTC_AC_CKB_POL);
   read32(0x48);
   read32(0x48);
   
   for (int ichan = 0; ichan < testChan; ichan++) {
      invertBits32(0xAC, TTC_AC_CKB_POL);
      read32(0x48);
      read32(0x48);
      invertBits32(0xAC, TTC_AC_CKB_POL);
      read32(0x48);
      read32(0x48);
   }
   
   if (seqEna)
      clearBits32(0xAC, TTC_AC_SEQ_DIS);
   
   printf("Register AC: 0x%08x\n", read32(0xAC));
   
   enableTrigOut(enaTrigOut);
   enableTrigIn(enaTrigIn);
   read32(0x48);

   return 0;
}

int AlphaTTC::writeDac(int idac, int value)
{
  //
  // NOTE: firmware starting from 2008062601 has these signals
  //   TTC_AC_DAC_CSN -> inverted DAC_CSn ---> 0 (CSn=1) ---> idle, 1 (CSn=0) ---> DAC selected
  //   TTC_AC_DAC_DIN ->  DAC_DIN ---> DIN
  //   TTC_AC_DAC_SCLK -> DAC_SCLK ---> SCLK
  //

  static int bits[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  if (value<0)
     value = 0;
  if (value>255)
     value = 255;

  for (int b=0; b<16; b++)
     bits[b] = 0;

  int r1 = 0;
  int r2 = 0;

  if (idac==0)
    r1 = 1; // addr sel
  else if (idac==1)
    r1 = 0;
  else if (idac==2)
     {
        r1 = 1;
        r2 = 1;
     }

  bits[0] = r1;
  bits[1] = 1; // fast mode

  if (value==0)
     bits[2] = 1; // power off

  bits[3] = r2;

  if (idac != 2)
     {
        for (int b=0; b<8; b++)
           bits[4+8-b-1] = (value & (1<<b))>0;
     }

  bits[12] = 0;
  bits[13] = 0;
  bits[14] = 0;
  bits[15] = 0;

  if (idac==2)
     {
        bits[14] = (value>>1)&1;
        bits[15] = (value>>0)&1;
     }

  printf("Writing into TTC DAC: ");
  for (int b=0; b<16; b++)
    printf(" %d", bits[b]);
  printf("\n");

  // SCLK->1, DIN->0

  setBits32(0xAC, TTC_AC_DAC_SCLK);
  clearBits32(0xAC, TTC_AC_DAC_DIN);

  // select the DAC - set CSn to 0, CS to 1

  setBits32(0xAC, TTC_AC_DAC_CSN);

  for (int b=0; b<16; b++)
    {
      //printf("bit %d -> %d\n", b, bits[b]);
      if (bits[b])
	setBits32(0xAC, TTC_AC_DAC_DIN);
      else
      	clearBits32(0xAC, TTC_AC_DAC_DIN);

      invertBits32(0xAC, TTC_AC_DAC_SCLK);
      invertBits32(0xAC, TTC_AC_DAC_SCLK);
    }

  // deselect the DAC, set CS to 0

  clearBits32(0xAC, TTC_AC_DAC_CSN);

  // clear DIN and SCLK bits
  
  clearBits32(0xAC, TTC_AC_DAC_SCLK);
  clearBits32(0xAC, TTC_AC_DAC_DIN);

  uint32_t ac = read32(0xAC);
  printf("Wrote dac %d, value %d, 0xAC is 0x%08x\n", idac, value, ac);

  return 0;
}
  
int AlphaTTC::setDacLevel(bool level)
{
  if (level)
    setBits32(0xAC, TTC_AC_DAC_LEVEL);
  else
    clearBits32(0xAC, TTC_AC_DAC_LEVEL);
  return 0;
}
  
int AlphaTTC::setMuxMode()
{
   writeCommand(TTC_CMD_Reset);
   clearBits32(0xAC, TTC_AC_SEQ_DIS);
   return 0;
}

void AlphaTTC::enableTrigOut(bool enable)
{
   if (enable)
      setBits32(0xC0, TTC_C0_ENA_TRIGOUT);
   else
      clearBits32(0xC0, TTC_C0_ENA_TRIGOUT);
}

bool AlphaTTC::disableTrigOut()
{
   int v = read32(0xC0) & TTC_C0_ENA_TRIGOUT;
   clearBits32(0xC0, TTC_C0_ENA_TRIGOUT);
   return v!=0;
}

void AlphaTTC::enableTrigIn(bool enable)
{
   if (enable)
      setBits32(0xC0, TTC_C0_ENA_TRIGIN);
   else
      clearBits32(0xC0, TTC_C0_ENA_TRIGIN);
}

bool AlphaTTC::disableTrigIn()
{
   int v = read32(0xC0) & TTC_C0_ENA_TRIGIN;
   clearBits32(0xC0, TTC_C0_ENA_TRIGIN);
   return v!=0;
}

int AlphaTTC::readLatchFifo(int ifpga, char* buf, int bufsize)
{
  uint16_t *buf16 = (uint16_t*)buf;
  int       bufsize16 = bufsize/2;

  int available16 = 0xFFF & readTriggerFPGA(ifpga, 32);
  int count16 = 0;

  for (count16=0; count16<available16; count16++)
    {
      uint32_t data = 0;

      if (false && fRevision >= 2008062101)
	{
	  if (count16 == 0)
	    {
	      write32(0x88, 0x0000FFFF | (1<<26) | (1<<25) | (ifpga<<24) | (33<<16));
	      //printf("Here!\n");
	      //sleep(5);
	    }
	  data = read32(0x8C);
	}
      else
	{
	  data = readTriggerFPGA(ifpga, 33);
	}

      //printf("available %3d, count %3d, bufsize16: %3d, data 0x%08x\n", available16, count16, bufsize16, data);
      //sleep(1);

      if (buf16)
	{
	  if (bufsize16 < 1)
	    break;

	  *buf16++ = (data & 0xFFFF);
	  bufsize16--;
	}
    }

  readTriggerFPGA(ifpga, 32);

  return count16*2;
}

int AlphaTTC::readCounters7(uint32_t data[7])
{
   writeCommand(TTC_CMD_LatchCounters);
   data[0] = read32(0x90);
   data[1] = read32(0x94);
   data[2] = read32(0x98);
   data[3] = read32(0x9C);
   data[4] = read32(0xA0);
   data[5] = read32(0xA4);
   data[6] = read32(0xA8);
   return 7;
}

void AlphaTTC::enableTA()
{
   for (int ifpga=0; ifpga<2; ifpga++)
      for (int i=0; i<8; i++)
         setTaEnableBitmap(ifpga, i, 0xFFFF);
}

void AlphaTTC::disableTA()
{
   for (int ifpga=0; ifpga<2; ifpga++)
      for (int i=0; i<8; i++)
         setTaEnableBitmap(ifpga, i, 0x0000);
}

void AlphaTTC::enableTA(int ifpga, int ibank, int ta)
{
   assert(ifpga>=0 && ifpga<2);
   assert(ibank>=0 && ibank<8);
   assert(ta>=0 && ta<16);

   fTaEnableBitmap[ifpga][ibank] |= (1<<ta);
   fTaEnableBitmapIsWritten = false;
}

void AlphaTTC::disableTA(int ifpga, int ibank, int ta)
{
   assert(ifpga>=0 && ifpga<2);
   assert(ibank>=0 && ibank<8);
   assert(ta>=0 && ta<16);

   fTaEnableBitmap[ifpga][ibank] &= ~(1<<ta);
   fTaEnableBitmapIsWritten = false;
}

void AlphaTTC::setTaEnableBitmap(int ifpga, int i, uint16_t v)
{
   fTaEnableBitmap[ifpga][i] = v;
   fTaEnableBitmapIsWritten = false;
}

uint16_t AlphaTTC::getTaEnableBitmap(int ifpga, int i)
{
   return fTaEnableBitmap[ifpga][i];
}

void AlphaTTC::writeTaEnableBitmap()
{
   for (int ifpga=0; ifpga<2; ifpga++)
      for (int ireg=0; ireg<8; ireg++)
         {
            //printf("TA mask %d %d 0x%04x\n", ifpga, ireg, fTaEnableBitmap[ifpga*8+ireg]);
            writeTriggerFPGA(ifpga, 8+ireg, fTaEnableBitmap[ifpga][ireg]);
         }
  fTaEnableBitmapIsWritten = true;
}

void AlphaTTC::setTaCountersBank(int ifpga, int ibank)
{
   assert(ibank>=0);
   assert(ibank<8);

   int reg1 = 0xFFFF & readTriggerFPGA(ifpga, 1);
   reg1 &= ~(7<<4);  // clear bank selection bits
   reg1 |= ibank<<4; // set new bank selection bits
   writeTriggerFPGA(ifpga, 1, reg1);
}

int AlphaTTC::getTaCountersBank(int ifpga)
{
   uint32_t reg1 = readTriggerFPGA(ifpga, 1);
   return (reg1>>4)&0x7;
}

void AlphaTTC::readTaCounters16(int ifpga, uint16_t count[16])
{
   for (int i=0; i<16; i++) {
      int32_t v = readTriggerFPGA(ifpga, 16+i);
      if ((v & 0xFFFF) == 0xFFFF) // some kind of firmware fault, reread the counter!
         v = readTriggerFPGA(ifpga, 16+i);
      count[i] = 0xFFFF & v;
      //if (count[i]==0xFFFF)
      //printf("ifpga %d, counter %d, read 0x%08x\n", ifpga, i, v);
   }
}

void AlphaTTC::clearTaMap()
{
   for (int ifpga=0; ifpga<=1; ifpga++)
      {
         writeTriggerFPGA(ifpga, 4, 0);
         writeTriggerFPGA(ifpga, 5, 0);
         writeTriggerFPGA(ifpga, 6, 0);
         writeTriggerFPGA(ifpga, 7, 0);
      }
}

void AlphaTTC::setTaMap(int ifpga, int ihybrid, int ilayer)
{
  assert(ifpga>=0 && ifpga<=1);
  assert(ihybrid>=0 && ihybrid<32);
  assert(ilayer>=0 && ilayer<=3);

  int ibit = ihybrid*2;
  int ioff = ibit%16;
  int ireg = ibit/16;

  uint32_t reg = readTriggerFPGA(ifpga, 4+ireg) & 0xFFFF;

  reg &= ~(3<<ioff);
  reg |= ilayer<<ioff;

  writeTriggerFPGA(ifpga, 4+ireg, reg);
}

void AlphaTTC::setMult(int ifpga, int ilayer, int imin, int imax)
{
  assert(ifpga>=0 && ifpga<=1);
  assert(ilayer>=0 && ilayer<=5);
  assert(imin>=0 && imin<=255);
  assert(imax>=0 && imax<=255);

  uint16_t v = (imax<<8) | imin;

  switch (ilayer)
    {
    case 0: writeTriggerFPGA(ifpga, 34, v); break;
    case 1: writeTriggerFPGA(ifpga, 35, v); break;
    case 2: writeTriggerFPGA(ifpga, 36, v); break;
    case 3: writeTriggerFPGA(ifpga, 43, v); break;
    case 4: writeTriggerFPGA(ifpga, 44, v); break;
    case 5: writeTriggerFPGA(ifpga, 45, v); break;
    }
}

void AlphaTTC::setTrig(int ifpga, int itrig, int isource)
{
  int v;

  assert(ifpga>=0 && ifpga<2);
  assert(itrig>=0 && itrig<3);
  assert(isource>=0 && isource<=255);

  switch (itrig)
    {
    case 0:
      v = readTriggerFPGA(ifpga, 0) & 0xFF00;
      writeTriggerFPGA(ifpga, 0, v | isource);
      break;
    case 1:
      v = readTriggerFPGA(ifpga, 0) & 0x00FF;
      writeTriggerFPGA(ifpga, 0, v | (isource<<8));
      break;
    case 2:
      v = readTriggerFPGA(ifpga, 1) & 0x00FF;
      writeTriggerFPGA(ifpga, 1, v | (isource<<8));
      break;
    }
}

int AlphaTTC::getTrig(int ifpga, int itrig)
{
  assert(ifpga>=0 && ifpga<2);
  assert(itrig>=0 && itrig<3);

  switch (itrig)
    {
    case 0:
      return (readTriggerFPGA(ifpga, 0) & 0x00FF) >> 0;
    case 1:
      return (readTriggerFPGA(ifpga, 0) & 0xFF00) >> 8;
    case 2:
      return (readTriggerFPGA(ifpga, 1) & 0xFF00) >> 8;
    }

  assert(!"NOT REACHED");
}

const char* AlphaTTC::trigSourceName(int isource)
{
   // see alphaTTC.txt FPGA register 0

   if (isource>=0 && isource<=127)
      return "raw TA";

   switch (isource)
      {
      case 128: return "0";
      case 129: return "1";
      case 130: return "TA grand OR";
      case 131: return "TA layer A grand OR";
      case 132: return "TA layer B grand OR";
      case 133: return "TA layer C grand OR";
      case 134: return "multa";
      case 135: return "multb";
      case 136: return "multc";
      case 137: return "mult_and: multa*multb*multc";
      case 138: return "multaa";
      case 139: return "multbb";
      case 140: return "multcc";
      case 141: return "mult2_and: multaa*multbb*multcc";
      }

   return "unknown, see alphaTTC.txt trigger FPGA reg0";
}

int AlphaTTC::status()
{
  printf("AlphaTTC at A32 0x%08x status:\n", fVmeBaseAddr);

  int reg;

  reg = read32(0x48);
  printf("reg 0x48: 0x%08x (%d), revision code\n", reg, reg);

  if (fRevision >= 2008021701)
    {
      if (0)
	{
          reg = read32(0x88);
          printf("reg 0x88: 0x%08x, trigger FPGA write\n", reg);
          reg = read32(0x8C);
          printf("reg 0x8C: 0x%08x, trigger FPGA read\n", reg);
	}

      for (int i=0; i<6; i++)
	{
	  int addr = 0x90 + i*4;
	  reg = read32(addr);
	  printf("reg 0x%02x: 0x%08x, counter for NIM output TRIG%d\n", addr, reg, i+1);
	}

      reg = read32(0xA8);
      printf("reg 0xA8: 0x%08x, counter for NIM output timestamp\n", reg);

      reg = read32(0xAC);
      printf("reg 0xAC: 0x%08x, VA1TA mux control:", reg);
      if (reg&TTC_AC_SEQ_DIS)
	printf(" SEQ_DIS");
      if (reg&TTC_AC_CKB_POL)
	printf(" CKB_POL");
      if (reg&TTC_AC_SHIN_POL)
	printf(" SHIN_POL");
      if (reg&TTC_AC_HOLD_ENA)
	printf(" HOLD_ENA");
      if (reg&TTC_AC_HOLD_POL)
	printf(" HOLD_POL");
      if (reg&TTC_AC_RESET_ENA)
	printf(" RESET_ENA");
      if (reg&TTC_AC_RESET_POL)
	printf(" RESET_POL");
      printf(" ADC_trig_src=%d", (reg>>8)&3);
      printf(" MUX_trig_src=%d", (reg>>10)&3);
      printf(" CalPulse_trig_src=%d", (reg>>12)&3);
      if (reg&TTC_AC_DAC_CSN)
	printf(" DAC_CSN");
      if (reg&TTC_AC_DAC_DIN)
	printf(" DAC_DIN");
      if (reg&TTC_AC_DAC_SCLK)
	printf(" DAC_SCLK");
      if (reg&TTC_AC_DAC_LEVEL)
	printf(" DAC_LEVEL");
      printf("\n");

      reg = read32(0xB0);
      printf("reg 0xB0: 0x%08x, self trigger control\n", reg);
    }

  if (fRevision >= 0x7F120208)
    {
      reg = read32(0xBC);
      printf("reg 0xBC: 0x%08x, busy extend:", reg);
      printf("\n");

      reg = read32(0xC0);
      printf("reg 0xC0: 0x%08x, general control:", reg);

      if (reg&TTC_C0_ENA_TRIGOUT)
	printf(" ENA_TRIGOUT");
      if (reg&TTC_C0_ENA_TRIGIN)
	printf(" ENA_TRIGIN");
      if (reg&TTC_C0_DIS_VETO)
	printf(" DIS_VETO");
      if (reg&TTC_C0_LED_TEST)
        printf(" LED_TEST");
      if (reg&TTC_C0_CLEAR_PLL_UNLOCKED_LATCH)
	printf(" CLEAR_PLL_UNLOCKED_LATCH");
      if (reg&TTC_C0_EXTERNAL_MUX_CLK)
	printf(" EXTERNAL_MUX_CLK");
      if (reg&TTC_C0_ENABLE_FAST_HOLD)
	printf(" ENABLE_FAST_HOLD");
      printf(" ADC delay %d,", (reg&0xFF000000)>>24);
      printf(" HOLD delay %d,", (reg&0x00FF0000)>>16);
      printf("\n");

      reg = read32(0xC4);
      printf("reg 0xC4: 0x%08x, general status:", reg);

      if (reg&TTC_C4_BUSY)
	printf(" BUSY");
      if (reg&TTC_C4_FEC_BOOT_REGOUT)
	printf(" FEC_BOOT_REGOUT");
      if (reg&TTC_C4_FP_VETO_IN)
	printf(" FP_VETO_IN");
      if (reg&TTC_C4_FP_EXT_IN)
	printf(" FP_EXT_IN");
      if (reg&TTC_C4_PLL1_UNLOCKED)
	printf(" PLL1_UNLOCKED");
      if (reg&TTC_C4_PLL2_UNLOCKED)
	printf(" PLL2_UNLOCKED");

      printf("\n");

      reg = read32(0xC8);
      printf("reg 0xC8: 0x%08x, firmware revision:", reg);
      printf("\n");

      reg = read32(0xCC);
      printf("reg 0xCC: 0x%08x, VME command:", reg);
      printf("\n");

      reg = read32(0xD0);
      printf("reg 0xD0: 0x%08x, VA1TA BOOT control:", reg);
      if (reg&1)
	printf(" boot_enable");
      if (reg&TTC_XCLK)
	printf(" CLKIN");
      if (reg&TTC_XDATA)
	printf(" REGIN");
      if (reg&TTC_READ)
	printf(" READ");
      if (reg&TTC_LOAD)
	printf(" LOAD");
      printf(" ADDR(0x%02x)", (reg>>8)&0x3F);
      printf("\n");
    }

  for (int ifpga=0; ifpga<2; ifpga++)
    {
      printf("\n");
      printf("Trigger FPGA %d\n", ifpga);

      printf("  reg 0x00: 0x%08x (routing for M1/M4 and M2/M5)\n", readTriggerFPGA(ifpga, 0));
      printf("  reg 0x01: 0x%08x (routing for M3/M6 and misc control)\n", readTriggerFPGA(ifpga, 1));
      printf("  reg 0x02: 0x%08x (multiplicity latch)\n", readTriggerFPGA(ifpga, 2));
      printf("  reg 0x03: 0x%08x (latch counter)\n", readTriggerFPGA(ifpga, 3));

      printf("  reg 0x04: 0x%08x (TA layer map)\n", readTriggerFPGA(ifpga, 4));
      printf("  reg 0x05: 0x%08x (TA layer map)\n", readTriggerFPGA(ifpga, 5));
      printf("  reg 0x06: 0x%08x (TA layer map)\n", readTriggerFPGA(ifpga, 6));
      printf("  reg 0x07: 0x%08x (TA layer map)\n", readTriggerFPGA(ifpga, 7));

#if 1
      printf("  reg 0x%02x+8: ", 8);
      for (int i=0; i<8; i++)
	printf(" 0x%04x", 0xFFFF & readTriggerFPGA(ifpga, 8+i));
      printf(" (TA latch)\n");

      printf("  reg 0x%02x+16: ", 16);
      for (int i=0; i<16; i++)
	printf(" 0x%04x", 0xFFFF & readTriggerFPGA(ifpga, 16+i));
      printf(" (TA counters)\n");
#endif

      printf("  reg 32: 0x%08x (fifo status)\n", readTriggerFPGA(ifpga, 32));
      printf("  reg 33: xxxxxxxxxx (fifo data)\n");

      printf("  reg 34: 0x%08x (layer A/1 min/max multiplicity)\n", readTriggerFPGA(ifpga, 34));
      printf("  reg 35: 0x%08x (layer B/2 min/max multiplicity)\n", readTriggerFPGA(ifpga, 35));
      printf("  reg 36: 0x%08x (layer C/3 min/max multiplicity)\n", readTriggerFPGA(ifpga, 36));

      printf("  reg 37: 0x%08x (time stamp latch, low bits)\n", readTriggerFPGA(ifpga, 37));
      printf("  reg 38: 0x%08x (time stamp latch, high bits)\n", readTriggerFPGA(ifpga, 38));

      printf("  reg 39: 0x%08x (multa counter)\n", readTriggerFPGA(ifpga, 39));
      printf("  reg 40: 0x%08x (multb counter)\n", readTriggerFPGA(ifpga, 40));
      printf("  reg 41: 0x%08x (multc counter)\n", readTriggerFPGA(ifpga, 41));
      printf("  reg 42: 0x%08x (mult_and counter)\n", readTriggerFPGA(ifpga, 42));

      printf("  reg 43: 0x%08x (layer A/1 min/max multiplicity)\n", readTriggerFPGA(ifpga, 43));
      printf("  reg 44: 0x%08x (layer B/2 min/max multiplicity)\n", readTriggerFPGA(ifpga, 44));
      printf("  reg 45: 0x%08x (layer C/3 min/max multiplicity)\n", readTriggerFPGA(ifpga, 45));
      printf("  reg 46: 0x%08x (multiplicity latch)\n", readTriggerFPGA(ifpga, 46));
    }

  printf("\n");
  printf("Trigger configuration:\n");
  printf("\n");

  uint32_t regB0 = read32(0xB0);
  printf("Self trigger regB0: 0x%08x: ", regB0);
  if (regB0 & 0x10000)
     printf(" (TRIG123 or TRIG456)");
  if (regB0 & 0x20000)
     printf(" (TRIG123 and TRIG456)");
  printf("\n");

  printf("  TRIG123: ");
  if (regB0 & 0x1)
     printf(" TRIG1");
  if (regB0 & 0x2)
     printf(" TRIG2");
  if (regB0 & 0x4)
     printf(" TRIG3");
  if (regB0 & 0x10)
     printf(" TRIG1*TRIG2");
  if (regB0 & 0x20)
     printf(" TRIG1*TRIG3");
  if (regB0 & 0x40)
     printf(" TRIG2*TRIG3");
  if (regB0 & 0x80)
     printf(" TRIG1*TRIG2*TRIG3");
  printf("\n");

  printf("  TRIG456: ");
  if (regB0 & 0x100)
     printf(" TRIG4");
  if (regB0 & 0x200)
     printf(" TRIG5");
  if (regB0 & 0x400)
     printf(" TRIG6");
  if (regB0 & 0x1000)
     printf(" TRIG4*TRIG5");
  if (regB0 & 0x2000)
     printf(" TRIG4*TRIG6");
  if (regB0 & 0x4000)
     printf(" TRIG5*TRIG6");
  if (regB0 & 0x8000)
     printf(" TRIG4*TRIG5*TRIG6");
  printf("\n");

  printf("TRIG1..6 source:\n");

  for (int ifpga=0; ifpga<2; ifpga++)
     for (int itrig=0; itrig<3; itrig++)
        {
           int isource = getTrig(ifpga, itrig);
           printf("  TRIG%d is %d (%s)\n", (ifpga*3+itrig)+1, isource, trigSourceName(isource));
        }

  for (int ifpga=0; ifpga<2; ifpga++)
     {
        printf("FPGA%d trigger configuration:\n", ifpga);

#if 0
        printf("  Layer  map: 0x%04x %04x %04x %04x\n",
               0xFFFF & readTriggerFPGA(ifpga, 7),
               0xFFFF & readTriggerFPGA(ifpga, 6),
               0xFFFF & readTriggerFPGA(ifpga, 5),
               0xFFFF & readTriggerFPGA(ifpga, 4));
#endif

        printf("  Enable map: 0x");
        for (int i=0; i<8; i++)
           {
              uint16_t v = getTaEnableBitmap(ifpga, i);
              printf(" %01x %01x %01x %01x ",
                     (v>>0)&0xF,
                     (v>>4)&0xF,
                     (v>>8)&0xF,
                     (v>>12)&0xF
                     );
           }

        if (fTaEnableBitmapIsWritten)
           printf("\n");
        else
           printf(" --- NOT WRITTEN TO HARDWARE!\n");

        printf("  Layer  map:   ");
        for (int i=4; i<8; i++)
           {
              uint32_t v = 0xFFFF & readTriggerFPGA(ifpga, i);
              for (int j=0 ;j<8; j++)
                 {
                    int ll = (v>>(j*2)) & 3;
                    printf(" %d", ll);

                    if (j%4 == 3)
                       printf(" ");
                 }
           }
        printf("\n");

        uint32_t multa = readTriggerFPGA(ifpga, 34);
        uint32_t multb = readTriggerFPGA(ifpga, 35);
        uint32_t multc = readTriggerFPGA(ifpga, 36);

        uint32_t multaa = readTriggerFPGA(ifpga, 43);
        uint32_t multbb = readTriggerFPGA(ifpga, 44);
        uint32_t multcc = readTriggerFPGA(ifpga, 45);

        printf("  Layer A: %d<=multa<=%d, %d<=multaa<=%d\n", multa&0xFF, (multa>>8)&0xFF, multaa&0xFF, (multaa>>8)&0xFF);
        printf("  Layer B: %d<=multb<=%d, %d<=multbb<=%d\n", multb&0xFF, (multb>>8)&0xFF, multbb&0xFF, (multbb>>8)&0xFF);
        printf("  Layer C: %d<=multc<=%d, %d<=multcc<=%d\n", multc&0xFF, (multc>>8)&0xFF, multcc&0xFF, (multcc>>8)&0xFF);
     }

  return 0;
}

/* emacs
 * Local Variables:
 * mode:c++
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

// end
