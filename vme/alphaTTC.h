//
// Name: alphaTTC.h
// Date:  2 May 2007
// Driver for the alpha TTC trigger and control module
//
#ifndef AlphaTTCH
#define AlphaTTCH

#include <stdio.h>
#include <stdint.h>

//
// VA1TA configuration registers
//

struct Va1taConfig
{
  int obi; // 3 bits
  int ibuf; // 3 bits
  int pre_bias; // 3 bits
  int sbi; // 3 bits
  int vrc; // 3 bits
  int ifp; // 3 bits
  int ifsf; // 3 bits
  int ifss; // 3 bits
  int sha_bias; // 3 bits
  int vthr; // 5 bits
  int twbi; // 3 bits
  int trim_dac[128]; // 4 bits each
  int disable[128];  // 1 bits each
  int test_on; // 1 bit
  int r2;
  int r1;
  int nside;
  int tp300;
};

struct HybridConfig
{
  Va1taConfig cfg[4];
};

// TTC register 0 bits

#define TTC_LOAD      (1<<7)
#define TTC_READ      (1<<6)
#define TTC_STEPMODE  (1<<5)
#define TTC_RESET_POL (1<<4)
#define TTC_RESET_ENA (1<<3)
#define TTC_HOLD_POL  (1<<2)
#define TTC_HOLD_ENA  (1<<1)

#define TTC_00_ENA_TRIGOUT (1<<0)
#define TTC_00_ENA_TRIGIN  (1<<1)
#define TTC_00_DIS_VETO    (1<<2) // disable VETO input, firmware 2008050901

// these bits change meaning if TTC_BOOT_ENA is set in register 2
#define TTC_XDATA (1<<5)
#define TTC_XCLK  (1<<4)

// TTC register 2 bits

#define TTC_LED_TEST  (1<<7)
#define TTC_BOOT_ENA  (1<<6)

// TTC register 3 commands

#define TTC_CMD_Trigger         5
#define TTC_CMD_Reset           8
#define TTC_CMD_Step            9 // until 2008mmddnn
#define TTC_CMD_CalPulse       10

#define TTC_CMD_ClearCounters  11 // firmware 2008013001
#define TTC_CMD_LatchCounters  12 // firmware 2008013001

#define TTC_CMD_LatchTrigger   13 // firmware 2008070301

// TTC register 0xAC bits (firmware 2008022701)

#define TTC_AC_SEQ_DIS   (1<<0)  // disable VA1TA mux sequencer
#define TTC_AC_CKB_POL   (1<<1)  // VA1TA mux clock
#define TTC_AC_SHIN_POL  (1<<2)  // VA1TA mux shift-in

#define TTC_AC_HOLD_ENA  (1<<4)
#define TTC_AC_HOLD_POL  (1<<5)
#define TTC_AC_RESET_ENA (1<<6)
#define TTC_AC_RESET_POL (1<<7)

// TTC register 0xAC bits (firmware 2008050901)

#define TTC_AC_DAC_CSN   (1<<28)
#define TTC_AC_DAC_DIN   (1<<29)
#define TTC_AC_DAC_SCLK  (1<<30)
#define TTC_AC_DAC_LEVEL (1<<31)

// TTC register 0xC0 bits (firmware 0x7F120208 - general control)

#define TTC_C0_ENA_TRIGOUT (1<<0)
#define TTC_C0_ENA_TRIGIN  (1<<1)
#define TTC_C0_DIS_VETO    (1<<2) // disable VETO input
#define TTC_C0_LED_TEST    (1<<3)
#define TTC_C0_CLEAR_PLL_UNLOCKED_LATCH (1<<4)
#define TTC_C0_EXTERNAL_MUX_CLK         (1<<5)
#define TTC_C0_ENABLE_FAST_HOLD         (1<<6)

// TTC register 0xC4 bits (firmware 0x7F120208 - general status)

#define TTC_C4_BUSY            (1<<0)
#define TTC_C4_unused1         (1<<1)
#define TTC_C4_FEC_BOOT_REGOUT (1<<2)
#define TTC_C4_FP_VETO_IN      (1<<3)
#define TTC_C4_FP_EXT_IN       (1<<4)
#define TTC_C4_PLL1_UNLOCKED   (1<<5)
#define TTC_C4_PLL2_UNLOCKED   (1<<6)

//
// Interface for the VME ALPHA TTC board
//

class AlphaTTC
{
public: // public methods
  static bool isPresent(void* vmeInterface, uint32_t vmeA32addr); // check for presence

  AlphaTTC(void* vmeInterface, uint32_t vmeA32addr); // ctor

  int init(); ///< initialize the TTC board

  /// functions to read and write TTC registers
  
  uint32_t read32(int addr); ///< read 32-bit register
  void write32(int addr, uint32_t data); ///< write 32-bit register

  uint32_t readTriggerFPGA(int ifpga, int ireg); ///< read from 16-bit registers on the trigger FPGA
  void writeTriggerFPGA(int ifpga, int ireg, int value); ///< write into the 16-bit registers on the trigger FPGA

  /// high level register access

  void setBits32(int ireg, uint32_t bits);    ///< set given bits in given register
  void clearBits32(int ireg, uint32_t bits);  ///< clear given bits in given register
  void invertBits32(int ireg, uint32_t bits); ///< invert given bits in given register

  void writeCommand(int command); ///< write TTC command (register 3)

  /// high level functions

  void ledTest(); ///< activate the TTC LED test function

  void calPulse(); ///< issue a calibration pulse
  void vmeTrigger(); ///< issue a VME trigger

  void vmeClearTriggerFPGA(int ifpga = -1); ///< issue a VME clear of the trigger FPGA
  void vmeLatch(int ifpga = -1); ///< issue a VME trigger latch command

  //int startReadout(bool verbose = false); ///< issue a software trigger and start the readout sequence

  static int EncodeBits(int maxBits, char bits[], const Va1taConfig& config); ///< encode the VA1TA configuration into a bit pattern

  int writeConfigurationBits(int ifrc, int ifrcPort, int numBits, const char* bits[4]);
  int readConfigurationBits(int ifrc, int ifrcPort, char bits[4][680]);

  int writeHybridConfig(int ifrc, int ifrcPort, const HybridConfig* cfg);

  int writeDac(int idac, int value); ///< write into the calibration pulse DAC
  int setDacLevel(bool level); ///< set calibration pulse output into level mode or pulse mode (default)

  int setHoldDelay(int delay_ns);
  int setAdcDelay(int delay_ns);
  int setBusyDelay(int delay_ns);

  int enableHold(bool enable);
  int enableReset(bool enable);
  
  int setTestMode(int testChan, bool extEna, bool trigEna, bool seqEna);
  int setMuxMode();

  int setMuxTrigSource(int source);

  void enableTrigOut(bool enable);
  bool disableTrigOut();

  void enableTrigIn(bool enable);
  bool disableTrigIn();

  /// trigger fpga TA counters

  void setTaCountersBank(int ifpga, int ibank);
  int  getTaCountersBank(int ifpga);
  void readTaCounters16(int ifpga, uint16_t count[16]);

  /// TA-enable bitmap functions

  void enableTA();
  void disableTA();

  void enableTA(int ifpga, int ibank, int ita);
  void disableTA(int ifpga, int ibank, int ita);

  void setTaEnableBitmap(int ifpga, int ibank, uint16_t v);
  uint16_t getTaEnableBitmap(int ifpga, int ibank);

  void writeTaEnableBitmap();

  bool getTaEnableBitmapIsWritten() { return fTaEnableBitmapIsWritten; };

  /// trigger functions

  void clearTaMap();
  void setTaMap(int ifpga, int ihybrid, int ilayer);
  void setMult(int ifpga, int ilayer, int imin, int imax);

  void setTrig(int ifpga, int itrig, int isource);
  int  getTrig(int ifpga, int itrig);

  static const char* trigSourceName(int isource);

  int readLatchFifo(int ifpga, char* buf, int bufsize);

  int readCounters7(uint32_t data[7]);

  /// misc functions

  int setInternalClock();
  int setExternalClock();

  int status();

public: // private data
  void* fVme;
  uint32_t fVmeBaseAddr;
  int fDebug;
  int fVerifyWrite;

  uint32_t fRevision;
   
  bool     fTaEnableBitmapIsWritten;
  uint16_t fTaEnableBitmap[2][8]; ///< keep a copy of TA enable bit map because it is "write-only"
};

#endif

/* emacs
 * Local Variables:
 * mode:c++
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

// end file
