//
// Name: alphaTTCX.h
// Date:  21 Aug 2012
// Driver for multiple alpha TTC boards
//
// $Id: alphaTTC.h 1775 2012-06-29 21:33:24Z olchansk $
//
#ifndef AlphaTTCXH
#define AlphaTTCXH

#include <stdio.h>
#include <stdint.h>
#include <alphaTTC.h>
#include <vector>

#define FOR_EACH_TTC for (unsigned int i=0; i<fTTC.size(); i++)

class AlphaTTCX
{
public: // public methods
   AlphaTTCX() // ctor
   {
      // do nothing
   }

   void AddTTC(AlphaTTC* ttc) ///< add a TTC driver
   {
      fTTC.push_back(ttc);
   }

   void ClearTTC() ///< remove all TTC drivers
   {
      fTTC.clear();
   }

   int GetNumTTC() ///< get number of TTCs
   {
      return fTTC.size();
   }

   void init() ///< initialize the TTC board
   {
      FOR_EACH_TTC {
         fTTC[i]->init();
      }
   }

   void status() ///< initialize the TTC board
   {
      FOR_EACH_TTC {
         fTTC[i]->status();
      }
   }

  uint32_t readTriggerFPGA(int ifpga, int ireg) ///< read from 16-bit registers on the trigger FPGA
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;
      return fTTC[i]->readTriggerFPGA(ifpga, ireg);
   }

  void writeTriggerFPGA(int ifpga, int ireg, int value) ///< write into the 16-bit registers on the trigger FPGA
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;
      fTTC[i]->writeTriggerFPGA(ifpga, ireg, value);
   }

  /// trigger fpga TA counters

  void setTaCountersBank(int ifpga, int ibank)
   {
      if (ifpga >= 0) {
         unsigned int i = ifpga/2;
         assert(i>=0 && i<fTTC.size());
         ifpga %= 2;
         fTTC[i]->setTaCountersBank(ifpga, ibank);
      } else
         FOR_EACH_TTC {
            fTTC[i]->setTaCountersBank(0, ibank);
            fTTC[i]->setTaCountersBank(1, ibank);
         }
   }

  int  getTaCountersBank(int ifpga)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;
      return fTTC[i]->getTaCountersBank(ifpga);
   }

  void readTaCounters16(int ifpga, uint16_t count[16])
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;
      return fTTC[i]->readTaCounters16(ifpga, count);
   }

  int readLatchFifo(int ifpga, char* buf, int bufsize)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      return fTTC[i]->readLatchFifo(ifpga, buf, bufsize);
   }

  void clearTaMap()
   {
      FOR_EACH_TTC {
         fTTC[i]->clearTaMap();
      }
   }

  void setTaMap(int ifpga, int ihybrid, int ilayer)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      return fTTC[i]->setTaMap(ifpga, ihybrid, ilayer);
   }

  void setMult(int ifpga, int ilayer, int imin, int imax)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      return fTTC[i]->setMult(ifpga, ilayer, imin, imax);
   }

  void enableTA(int ifpga, int ibank, int ta)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      fTTC[i]->enableTA(ifpga, ibank, ta);
   }

  void disableTA(int ifpga, int ibank, int ta)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      fTTC[i]->disableTA(ifpga, ibank, ta);
   }

  void setTaEnableBitmap(int ifpga, int ibank, uint16_t v)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      fTTC[i]->setTaEnableBitmap(ifpga, ibank, v);
   }

  uint16_t getTaEnableBitmap(int ifpga, int ibank)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      return fTTC[i]->getTaEnableBitmap(ifpga, ibank);
   }

  void setTrig(int ifpga, int itrig, int isource)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      fTTC[i]->setTrig(ifpga, itrig, isource);
   }

  int  getTrig(int ifpga, int itrig)
   {
      unsigned int i = ifpga/2;
      assert(i>=0 && i<fTTC.size());
      ifpga %= 2;

      return fTTC[i]->getTrig(ifpga, itrig);
   }

  void vmeClearTriggerFPGA(int ifpga = -1)
   {
      if (ifpga >= 0) {
         unsigned int i = ifpga/2;
         assert(i>=0 && i<fTTC.size());
         ifpga %= 2;
         fTTC[i]->vmeClearTriggerFPGA(ifpga);
      } else {
         FOR_EACH_TTC {
            fTTC[i]->vmeClearTriggerFPGA(ifpga);
         }
      }
   }

  void vmeLatch(int ifpga = -1)
   {
      if (ifpga >= 0) {
         unsigned int i = ifpga/2;
         assert(i>=0 && i<fTTC.size());
         ifpga %= 2;
         fTTC[i]->vmeLatch(ifpga);
      } else {
         FOR_EACH_TTC {
            fTTC[i]->vmeLatch(ifpga);
         }
      }
   }

  void enableHold(bool enable)
   {
      FOR_EACH_TTC {
         fTTC[i]->enableHold(enable);
      }
   }

  void enableReset(bool enable)
   {
      FOR_EACH_TTC {
         fTTC[i]->enableReset(enable);
      }
   }

  void enableTrigOut(bool enable)
   {
      FOR_EACH_TTC {
         fTTC[i]->enableTrigOut(enable);
      }
   }

  void enableTrigIn(bool enable)
   {
      FOR_EACH_TTC {
         fTTC[i]->enableTrigIn(enable);
      }
   }
  
  int writeHybridConfig(int ifrc, int ifrcPort, const HybridConfig* cfg)
   {
      unsigned int i = ifrc/16;
      assert(i>=0 && i<fTTC.size());
      ifrc %= 16;
      return fTTC[i]->writeHybridConfig(ifrc, ifrcPort, cfg);
   }

  int readConfigurationBits(int ifrc, int ifrcPort, char rbits[4][680])
   {
      unsigned int i = ifrc/16;
      assert(i>=0 && i<fTTC.size());
      ifrc %= 16;
      return fTTC[i]->readConfigurationBits(ifrc, ifrcPort, rbits);
   }

  void writeCommand(int command)
   {
      FOR_EACH_TTC {
         fTTC[i]->writeCommand(command);
      }
   }

  void calPulse()
   {
      FOR_EACH_TTC {
         fTTC[i]->calPulse();
      }
   }

  void vmeTrigger()
   {
      FOR_EACH_TTC {
         fTTC[i]->vmeTrigger();
      }
   }

   void setTestMode(int testChan, bool extEna, bool trigEna, bool seqEna)
   {
      FOR_EACH_TTC {
         fTTC[i]->setTestMode(testChan, extEna, trigEna, seqEna);
      }
   }

  void setMuxMode()
   {
      FOR_EACH_TTC {
         fTTC[i]->setMuxMode();
      }
   }

  void setMuxTrigSource(int source)
   {
      FOR_EACH_TTC {
         fTTC[i]->setMuxTrigSource(source);
      }
   }

  void writeDac(int idac, int value)
   {
      FOR_EACH_TTC {
         fTTC[i]->writeDac(idac, value);
      }
   }

  void setDacLevel(bool level)
   {
      FOR_EACH_TTC {
         fTTC[i]->setDacLevel(level);
      }
   }

  void enableTA()
   {
      FOR_EACH_TTC {
         fTTC[i]->enableTA();
      }
   }

  void disableTA()
   {
      FOR_EACH_TTC {
         fTTC[i]->disableTA();
      }
   }

  void writeTaEnableBitmap()
   {
      FOR_EACH_TTC {
         fTTC[i]->writeTaEnableBitmap();
      }
   }

  bool getTaEnableBitmapIsWritten()
   {
      bool iswritten = true;
      FOR_EACH_TTC {
         iswritten &= fTTC[i]->getTaEnableBitmapIsWritten();
      }
      return iswritten;
   }

  void resetPol()
   {
      FOR_EACH_TTC {
         fTTC[i]->invertBits32(0xAC, TTC_AC_RESET_POL);
      }
   }

  void setHoldDelay(int delay_ns)
   {
      FOR_EACH_TTC {
         fTTC[i]->setHoldDelay(delay_ns);
      }
   }

  void setAdcDelay(int delay_ns)
   {
      FOR_EACH_TTC {
         fTTC[i]->setAdcDelay(delay_ns);
      }
   }

  void setBusyDelay(int delay_ns)
   {
      FOR_EACH_TTC {
         fTTC[i]->setBusyDelay(delay_ns);
      }
   }

  void ledTest()
   {
      FOR_EACH_TTC {
         fTTC[i]->ledTest();
      }
   }

   void setInternalClock()
   {
      FOR_EACH_TTC {
         fTTC[i]->setInternalClock();
      }
   }

  int setExternalClock()
   {
      int status = 0;
      FOR_EACH_TTC {
         status |= fTTC[i]->setExternalClock();
      }
      return status;
   }

  static const char* trigSourceName(int isource)
   {
      return AlphaTTC::trigSourceName(isource);
   }

public: // private data
   std::vector<AlphaTTC*> fTTC;
};

#endif

/* emacs
 * Local Variables:
 * mode:c++
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

// end file
