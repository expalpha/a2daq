/********************************************************************\

  Name:         fesy127.cxx
  Created by:   K.Olchanski

  Contents:     Frontend for the CAEN SY127 High Voltage mainframe

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include <math.h>

#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "mvmestd.h"
#include "../src/evid.h"

#define HAVE_V288

extern "C" {
#include "v288.h"
#include "sy127.h"
}

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   char *frontend_name = "fesy127";
/* The frontend file name, don't change it */
   char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 20*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 20*1024;

/* buffer size to hold events */
   INT event_buffer_size = 1024*1024;

  extern INT run_state;
  extern HNDLE hDB;

/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_sy127_event(char *pevent, INT off);
/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {

    {"SY127",                   /* equipment name */
     {EVID_SY127, (1<<EVID_SY127),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS|RO_ODB,       /* read only when running */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      60,                     /* whether to log history */
      "", "", "",}
     ,
     read_sy127_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {""}
  };
  
#ifdef __cplusplus
}
#endif
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

static MVME_INTERFACE *gVme = 0;

static int gV288Base   = 0xF00000; // VME A24 address of the V288
static int gSy127addr  = 0;        // SY127 CAENET hardware address

static v288 *gV288 = NULL;
static sy127 *gSy127 = NULL;

#include "../src/utils.cxx"

/*-- VME setup     -------------------------------------------------*/

INT init_vme_modules()
{
#if 0
  for (int mf=0; mf<1; mf++)
    {
      for (int c = 0; c<10; c++)
        {
          uint16_t buf[100];

          int nread = 0;
          //int status = gV288->sendCommand0(0, 0x00, 100, buf, nread);
          int status = gV288->sendCommand1(mf, 0xFF0F, c, 100, buf, nread);
          printf("mainframe %d, status %d, received %d\n", mf, status, nread);
          if (nread > 0)
            {
              for (int i=0; i<nread; i++)
                printf("%d: 0x%x %d \'%c\'\n", i, buf[i], buf[i], buf[i]);
            }
          else if (status || (status==0 && nread==0))
            gV288->reset();
        }
    }
#endif

  return SUCCESS;
}

/*-- Global variables ----------------------------------------------*/

static const int kMaxChan = 40;
static double gVcalibration[kMaxChan];
static double gIcalibration[kMaxChan];

static sy127channel ch[kMaxChan];

static bool gReady = false;

static int   enac_buf[kMaxChan];
static int   ison_buf[kMaxChan];
static float demv_buf[kMaxChan];
static float maxv_buf[kMaxChan];

static bool  gForceSend = false;

void demv_callback(int x, int y, void*z)
{
  printf("demv callback!\n");

  for (int i=0; i<kMaxChan; i++)
    if (enac_buf[i])
      {
        float shvvd = ch[i].v0;
        
        float demv = demv_buf[i];
        if (!ison_buf[i])
          demv = 0;
        if (demv > maxv_buf[i])
          demv = maxv_buf[i];
        float sdemv = demv/gVcalibration[i];
        
        //printf("chan %d, maxv %f, demv %f scaled %f, hvvd scaled %f, calibration %f, ready %d\n", i, maxv_buf[i], demv_buf[i], sdemv, shvvd, gVcalibration[i], gReady);
            
        if (gReady && gSy127)
          {
            if (fabs(sdemv-shvvd) > 0.01)
              {
                gSy127->writeParameter(i, SY127_PAR_V0, (int)sdemv);
                ss_sleep(100);
                gSy127->readChannel(i, ch+i);
                ss_sleep(100);
                gForceSend = true;
              }

#if 1
            if ((ison_buf[i]==0) && ((ch[i].status&SY127_STATUS_ON)!=0))
              {
                // turn off

                gSy127->readChannel(i, ch+i);
                ss_sleep(100);
                gSy127->writeParameter(i, SY127_PAR_ONOFF, 0);
                ss_sleep(500);
                gSy127->readChannel(i, ch+i);
                ss_sleep(100);
                gForceSend = true;
              }

            if ((ison_buf[i]!=0) && ((ch[i].status&SY127_STATUS_ON)==0))
              {
                // turn on

                gSy127->readChannel(i, ch+i);
                ss_sleep(100);
                gSy127->writeParameter(i, SY127_PAR_ONOFF, 1);
                ss_sleep(500);
                gSy127->readChannel(i, ch+i);
                ss_sleep(100);
                gForceSend = true;
              }
#endif
          }
      }
}

void maxv_callback(int x, int y, void*z)
{
  printf("maxv callback!\n");
  demv_callback(0,0,NULL);
}

void enac_callback(int x, int y, void*z)
{
  printf("enac callback!\n");
  demv_callback(0,0,NULL);
}

void ison_callback(int x, int y, void*z)
{
  printf("ison callback!\n");
  demv_callback(0,0,NULL);
}

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int status;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  cm_deregister_transition(TR_START);
  cm_deregister_transition(TR_STOP);
  cm_deregister_transition(TR_PAUSE);
  cm_deregister_transition(TR_RESUME);

  //cm_enable_watchdog(0);

  status = mvme_open(&gVme,0);

  if (!gV288)
    gV288 = new v288(gVme, gV288Base);

  gV288->fDebug = odbReadInt("/Equipment/SY127/Settings/V288_Debug");
  gV288->reset();

  status = init_vme_modules();
  if (status != SUCCESS)
    return status;

  HNDLE hKey;

  status = db_find_key(hDB, 0, "/Equipment/sy127/Settings/EnableControl", &hKey);
  assert(status==DB_SUCCESS);
  status = db_open_record(hDB, hKey, enac_buf, sizeof(enac_buf), MODE_READ, enac_callback, NULL);
  assert(status==DB_SUCCESS);

  status = db_find_key(hDB, 0, "/Equipment/sy127/Settings/IsOn", &hKey);
  assert(status==DB_SUCCESS);
  status = db_open_record(hDB, hKey, ison_buf, sizeof(ison_buf), MODE_READ, ison_callback, NULL);
  assert(status==DB_SUCCESS);

  status = db_find_key(hDB, 0, "/Equipment/sy127/Settings/MaxV", &hKey);
  assert(status==DB_SUCCESS);
  status = db_open_record(hDB, hKey, maxv_buf, sizeof(maxv_buf), MODE_READ, maxv_callback, NULL);
  assert(status==DB_SUCCESS);

  status = db_find_key(hDB, 0, "/Equipment/sy127/Settings/DemandV", &hKey);
  assert(status==DB_SUCCESS);
  status = db_open_record(hDB, hKey, demv_buf, sizeof(demv_buf), MODE_READ, demv_callback, NULL);
  assert(status==DB_SUCCESS);

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  mvme_close(gVme);

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  printf("begin run: %d\n",run_number);

  int status = init_vme_modules();
  if (status != SUCCESS)
    return status;

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  printf("end run %d\n",run_number);

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  assert(!"Not implemented");
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
   assert(!"Not implemented");
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

int read_sy127_event(char *pevent, INT off)
{
  if (!gV288)
    return 0;

  if (!gSy127)
    {
      gSy127 = new sy127(gV288, gSy127addr);
      
      gSy127->fDebug = odbReadInt("/Equipment/SY127/Settings/SY127_Debug");

      if (1)
        {
          char id[256];
          int status = gSy127->readId(id, sizeof(id));
          cm_msg(MINFO, frontend_name, "SY127 status %d, id \'%s\'", status, id);
          
          if (status!=0 || strstr(id, "SY127")==NULL)
            {
              cm_msg(MERROR, frontend_name, "Cannot read identification of the SY127 CAEN HV mainframe...");
              al_trigger_alarm("SY127", "SY127 communication error, see messages", "Warning", "", AT_INTERNAL);
              exit(1);
            }
        }

      al_reset_alarm("SY127");
          
      for (int ichan=0; ichan<40; ichan++)
        {
          int status = gSy127->readChannel(ichan, ch+ichan);

          printf("chan %d, status %d, v0 %f, i0 %f, status %d, type %d, vread %f, iread %f, name \'%s\'\n", ichan, status, ch[ichan].v0, ch[ichan].i0, ch[ichan].status, ch[ichan].mod_type, ch[ichan].vread, ch[ichan].iread, ch[ichan].chname);

          switch (ch[ichan].mod_type)
            {
            case 1:
            case 10:
              gVcalibration[ichan] = 0.5;
              gIcalibration[ichan] = 1.0;
              break;
            case 6: // 800V supply
              gVcalibration[ichan] = 0.1;
              gIcalibration[ichan] = 0.1; // to be checked
              break;
            case 9: // 200V supply
              gVcalibration[ichan] = 0.1;
              gIcalibration[ichan] = 0.1;
              break;
            default:
              gVcalibration[ichan] = 1.0;
              gIcalibration[ichan] = 1.0;
            }
        }

      gReady = true;
      demv_callback(0,0,NULL);

      gForceSend = true;
    }

  if (!gSy127)
    return 0;

  //printf("read_sy127!\n");

  static int nextChan = 0;
  static int nextBgChan = 0;

  if (nextChan < 0)
    nextChan = 0;

  if (nextChan >= kMaxChan)
    nextChan = 0;

  int readChan = -1;

  bool doSend = false;

  while (nextChan < kMaxChan)
    {
      //printf("chan %d status %d\n", nextChan, ch[nextChan].status);
      
      if (ch[nextChan].status != 0 // channel absent
          && ch[nextChan].status != 1 // turned off
          && ch[nextChan].status != 33 // interlock
          )
        {
          readChan = nextChan;
          doSend = true;
          nextChan ++;
          if (nextChan >= kMaxChan)
            nextChan = 0;
          break;
        }

      nextChan++;
    }

  if (readChan < 0)
    {
      readChan = nextBgChan;
      nextBgChan++;
      if (nextBgChan >= kMaxChan)
        {
          nextBgChan = 0;
          doSend = true;
        }
    }

  int status = gSy127->readChannel(readChan, ch+readChan);

  if (status != 0)
    {
      cm_msg(MERROR, frontend_name, "Cannot read data from the SY127 CAEN HV mainframe, readChannel() status %d", status);
      al_trigger_alarm("SY127", "SY127 communication error, see messages", "Warning", "", AT_INTERNAL);
      exit(1);
    }

  if (!gForceSend && !doSend)
    return 0;

  gForceSend = false;
  
  //printf("Send event!\n");

  /* init bank structure */
  bk_init32(pevent);

  float *pdata;

  bk_create(pevent, "HVVD", TID_FLOAT, (void**)&pdata);
  for (int i=0; i<kMaxChan; i++)
    pdata[i] = ch[i].v0 * gVcalibration[i];
  bk_close(pevent, pdata+kMaxChan);

  bk_create(pevent, "HVVM", TID_FLOAT, (void**)&pdata);
  for (int i=0; i<kMaxChan; i++)
    pdata[i] = ch[i].vread * gVcalibration[i];
  bk_close(pevent, pdata+kMaxChan);

  bk_create(pevent, "HVIM", TID_FLOAT, (void**)&pdata);
  for (int i=0; i<kMaxChan; i++)
    pdata[i] = ch[i].iread * gIcalibration[i];
  bk_close(pevent, pdata+kMaxChan);
  
  int *pidata;

  bk_create(pevent, "HVST", TID_INT, (void**)&pidata);
  for (int i=0; i<kMaxChan; i++)
    pidata[i] = ch[i].status;
  bk_close(pevent, pidata+kMaxChan);

  return bk_size(pevent);
}

//end
