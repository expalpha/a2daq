/********************************************************************\

  Name:         fevme.cxx
  Created by:   K.Olchanski

  Contents:     Frontend for the ALPHA VME DAQ

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "mvmestd.h"
#include "../src/evid.h"

#include "fevme_settings.h"

extern "C" {
#ifdef HAVE_VMEIO
#include "vmeio.h"
#endif
#ifdef HAVE_V792
#include "v792.h"
#endif
#ifdef HAVE_SIS3820
#include "sis3820drv.h"
#include "sis3820.h"
#endif
#ifdef HAVE_V560
#include "v560.h"
#endif
#ifdef HAVE_V513
#include "v513.h"
#endif
#ifdef HAVE_VF48
#include "vf48.h"
#endif
}

#ifdef HAVE_TTC
#include "alphaTTC.h"
#include "alphaTTCX.h"
#endif

#ifdef HAVE_IO32 
#include "VMENIMIO32.h" 
VMENIMIO32* io32 = NULL; 
#endif 

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   const char *frontend_name = "fevme";
/* The frontend file name, don't change it */
   const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 2*1024*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 300*1024*1024;

  extern INT run_state;
  extern HNDLE hDB;

/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_adc_event(char *pevent, INT off);
  INT read_scaler_event(char *pevent, INT off);
  INT read_pulser_event(char *pevent, INT off);
  INT read_latch_event(char *pevent, INT off);
  INT read_sispulser_event(char *pevent, INT off);
  INT read_mcs_event(char *pevent, INT off);
  INT read_vf48_event(char *pevent, INT off);
  INT read_vme_event(char *pevent, INT off);
  INT read_ttc_event(char *pevent, INT off);
  INT read_io32_event(char *pevent, INT off);

/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {

    {"VME",                  /* equipment name */
     {EVID_VME, (1<<EVID_VME), /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_MULTITHREAD,         /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* when to read this event */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* whether to log history */
      "", "", "",}
     ,
     read_vme_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#ifdef HAVE_SIS3820
    {"SisHistory",            /* equipment name */
     {EVID_MCS, (1<<EVID_MCS),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* read only when running */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_mcs_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_VF48
    {"VF48",                  /* equipment name */
     {EVID_VF48, (1<<EVID_VF48), /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* when to read this event */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_vf48_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_V792
    {"ADC",                   /* equipment name */
     {EVID_ADC, (1<<EVID_ADC),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* read only when running */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_adc_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_V560
    {"Scaler",                   /* equipment name */
     {EVID_SCALER, (1<<EVID_SCALER),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* read only when running */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_scaler_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_V513
    {"Pulser",                   /* equipment name */
     {EVID_PULSER, (1<<EVID_PULSER),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      100,                    /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_pulser_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_V513
    {"Latch",                   /* equipment name */
     {EVID_LATCH, (1<<EVID_LATCH),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* when to read */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_latch_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_SIS3820
    {"SisPulser",               /* equipment name */
     {EVID_PULSER, (1<<EVID_PULSER), /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* when to read */
      10000,                  /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_sispulser_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_TTC
    {"TTC",               /* equipment name */
     {EVID_TTC, (1<<EVID_TTC), /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_EOR|RO_ODB, /* when to read */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* history period, ms */
      "", "", "",}
     ,
     read_ttc_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
#ifdef HAVE_IO32
    {"IO32",               /* equipment name */
     {EVID_IO32, (1<<EVID_IO32), /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_EOR|RO_ODB,      /* when to read */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* history period, ms */
      "", "", "",}
     ,
     read_io32_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
#endif
    {""}
  };
  
#ifdef __cplusplus
}
#endif
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

MVME_INTERFACE *gVme = 0;

uint16_t gIoreg = 0;

int gVmeioBase   = 0x780000;
int gIoregBase   = 0x510000;
int gScaler1Base = 0x560000;
int gScaler2Base = 0x570000;
int gAdc1base    = 0x110000;
int gTdcBase     = 0xf10000;
int gMcsBase[] = { 0x38000000, 0x39000000, 0 };
int gTtcBase[] = { 0x41000000, 0x42000000, 0 };

int gVmeio32base  = 0x100000;

#define VF48_NUM 8
static int gVF48base[VF48_NUM] = { 0xa00000, 0xad0000, 0xa20000, 0xa40000, 0xa80000, 0xaf0000, 0xaa0000, 0xab0000 };
static const char* gVF48name[VF48_NUM] = { "VFA0", "VFA1", "VFA2", "VFA3", "VFA4", "VFA5", "VFA6", "VFA7" };
static bool gVF48enable[VF48_NUM];

uint64_t gVF48countReads[VF48_NUM];
uint64_t gVF48countBytes[VF48_NUM];

int gVF48pllLockCount[VF48_NUM][6];

#if defined VF48_MULTI 
extern "C" { 
#include "vf48_multi.h" 
} 
struct vf48_multi_buffer vf48_mb; 
#endif 

#ifdef HAVE_TTC
AlphaTTCX *gTTC = NULL;
static int gCalPulse = 0;
static int gCalPulseOnce = 0;
#endif

int vmeread16(int addr)
{
  mvme_set_dmode(gVme, MVME_DMODE_D16);
  return mvme_read_value(gVme, addr);
}

int vmeread32(int addr)
{
  mvme_set_dmode(gVme, MVME_DMODE_D32);
  return mvme_read_value(gVme, addr);
}

void vmewrite8(int addr, uint8_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D8);
  mvme_write_value(gVme, addr, data);
}

void vmewrite16(int addr, uint16_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D16);
  mvme_write_value(gVme, addr, data);
}

void vmewrite32(int addr, uint32_t data)
{
  mvme_set_dmode(gVme, MVME_DMODE_D32);
  mvme_write_value(gVme, addr, data);
}

void encodeU32(char*pdata,uint32_t value)
{
  pdata[0] = (value&0x000000FF)>>0;
  pdata[1] = (value&0x0000FF00)>>8;
  pdata[2] = (value&0x00FF0000)>>16;
  pdata[3] = (value&0xFF000000)>>24;
}

void xusleep(int usec)
{
#if 0
  struct timespec req;
  req.tv_sec = 0;
  req.tv_nsec = usec*1000;
  nanosleep(&req, NULL);
#endif
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = usec;
  select(0, NULL, NULL, NULL, &tv);
}

extern "C" {
  __inline__ uint32_t rdtscl(void) {
    uint32_t lo, hi;
    __asm__ __volatile__ (      // serialize
    "xorl %%eax,%%eax \n        cpuid"
    ::: "%rax", "%rbx", "%rcx", "%rdx");
    /* We cannot use "=A", since this would use %rax on x86_64 and return only the lower 32bits of the TSC */
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    //return (uint64_t)hi << 32 | lo;
    return lo;
  }
}


#include "../src/utils.cxx"

int enable_trigger()
{
  //printf("enable_trigger!\n");
#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0xFFFF);
#endif
#ifdef HAVE_V513
  gIoreg |= 1;
  v513_Write(gVme,gIoregBase,gIoreg);
#endif
#ifdef HAVE_SIS3820
  for (int i=0; gMcsBase[i]!=0; i++)
    sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_ENABLE,0);
#endif
#ifdef HAVE_VF48
  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      vf48_AcqStart(gVme, gVF48base[i]);
#endif
#ifdef HAVE_TTC
  gTTC->writeCommand(TTC_CMD_Reset);
  gTTC->enableTrigIn(true);
  gTTC->enableTrigOut(true);
#endif
  return 0;
}

int disable_trigger()
{
  //printf("disable_trigger!\n");
#ifdef HAVE_TTC
  gTTC->enableTrigOut(false);
  gTTC->enableTrigIn(false);
  gTTC->writeCommand(TTC_CMD_Reset);
#endif
#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0);
#endif
#ifdef HAVE_V513
  gIoreg &= ~1;
  v513_Write(gVme,gIoregBase,gIoreg);
#endif
#ifdef HAVE_SIS3820
  for (int i=0; gMcsBase[i]!=0; i++)
    sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_DISABLE,0);
#endif
#ifdef HAVE_VF48
  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      vf48_AcqStop(gVme, gVF48base[i]);
#endif
  return 0;
}

/*-- VME setup     -------------------------------------------------*/

INT init_vme_modules()
{
  int status;

  int isCosmic = odbReadBool("/Experiment/Edit on Start/Cosmic_Run", 0); // configure for cosmic run

  if (isCosmic)
     cm_msg(MINFO, frontend_name, "Configuring Si detector trigger for cosmic run");

#ifdef HAVE_IO32 
 
  set_equipment_status("VME", "Initializing VME IO32...", "#00FF00"); 
 
  if (!io32) {
     io32 = new VMENIMIO32(gVme, gVmeio32base); 
     io32->init(); 
     cm_msg(MINFO, frontend_name, "VME-NIMIO32 A24 address 0x%08x, firmware 0x%08x", gVmeio32base, io32->revision());
  }
 
  if (io32->read32(0) == 0xFFFFFFFF) 
    { 
      cm_msg(MERROR, frontend_name, "init_vme_modules: VME-NIMIO32 at VME address A24 0x%06x is missing!", gVmeio32base); 
      return FE_ERR_HW; 
    } 

  //io32->write32(4*3, 0xffff); // clear NIM input latch 
  //io32->write32(4*2, 0x00840000); // set NIM output 1 function 1, output 3 function 2 
  //int delay = (312-120)/10; // 10 ns per bin 
  //int width = 500/10; // 10 ns per bin 
  //io32->write32(4*48, (width<<16)|delay); // set L222 delay and width 
 
  //io32->NimOutput(0x0002, 0xFFFF); // clear NIM output 
  //io32->NimOutput(0x0002, 0x0000); // set NIM output 1 (busy) 
  //io32->write32(IO32_NIMIN,  0xFFFF); // clear NIM input latch 
 
  //io32->SetNimOutputFunction(0, 0); // set NIM output 0 function 0 
  //io32->SetNimOutputFunction(0, 1); // set NIM output 0 as 20 MHz clock 
  //io32->SetNimOutputFunction(1, 1); // set NIM output 1 as DAQ busy register 
  //io32->SetNimOutputFunction(2, 0); // set NIM output 2 function 0 
  //io32->SetNimOutputFunction(3, 1); // set NIM output 3 as 40 MHz clock 

  //io32->write32(4*51, 0x00040001); // TDC stop delay and width, 25 ns per count 
  //io32->write32(4*52, 0x001C0001); // FastClear delay and width, 25 ns per count 
  //io32->write32(4*56, 0x80000001); // S1249 trigger control register 
 
  //sleep(1); // sleep 1 sec for the V1190 TDC to see the clock 

  io32->write32(IO32_NIMIN,  0xFFFF); // clear NIM input latch 

  io32->SetNimOutputFunction(0, odbReadUint32("/Equipment/IO32/Settings/Output0_Func", 0)); // set NIM output 0 function 0 
  io32->SetNimOutputFunction(1, odbReadUint32("/Equipment/IO32/Settings/Output1_Func", 0)); // set NIM output 1 function 0 
  io32->SetNimOutputFunction(2, odbReadUint32("/Equipment/IO32/Settings/Output2_Func", 0)); // set NIM output 2 function 0 
  io32->SetNimOutputFunction(3, odbReadUint32("/Equipment/IO32/Settings/Output3_Func", 0)); // set NIM output 3 function 0

  io32->write32(4*17, 0x0000FA10);

  //io32->write32((4*49), odbReadUint32("/Equipment/IO32/Variables/Pulser", 0)); // set pulser period in 10 ns increments

  if (odbReadBool("/Equipment/IO32/Settings/ALPHA_enable", 0, true)) {
    io32->write32(4*57,   0x80000000); // enable ALPHA functions (20 MHz clock & etc)

    if (odbReadBool("/Equipment/IO32/Settings/Pulser_enable", 0, false)) {
      io32->flip32(4*57, 0x20000000, 0, 0); // enable pulser
      cm_msg(MINFO, frontend_name, "IO32 pulser enabled"); 
    } else {
      cm_msg(MINFO, frontend_name, "IO32 pulser disabled"); 
    }

    if (1) {
      double freq = odbReadDouble("/Equipment/IO32/Settings/Pulser_frequency", 0, 10.0); // pulser frequency
      uint32_t period = 0;
      if (freq > 0)
	period = 100e6/freq;
      io32->write32((4*49), period); // set pulser period in 10 ns increments
      printf("Frequency %f, period %d\n", freq, period);

      cm_msg(MINFO, frontend_name, "IO32 pulser period %d, frequency %.1f Hz", period, freq); 
    }

    if (1) {
      int busy_gate_width = odbReadInt("/Equipment/IO32/Settings/Busy_gate_width", 0, 132);
      if (busy_gate_width < 2)
	busy_gate_width = 2;
      if (busy_gate_width > 255)
	busy_gate_width = 255;
      io32->flip32(4*57, busy_gate_width<<8, 0, 0); // width of the busy signal
    }

    if (1) {
      int vf48_busy_enable = odbReadInt("/Equipment/IO32/Settings/VF48_busy_enable", 0, 0xFF);
      io32->flip32(4*57, 0x000000FF & vf48_busy_enable, 0, 0); // enable VF48 busy
    }

    if (odbReadBool("/Equipment/IO32/Settings/Ext_trig_enable"))
       io32->flip32(4*57, 1<<24, 0, 0);

    int veto1_enable = odbReadBool("/Equipment/IO32/Settings/Veto1_enable");
    int veto2_enable = odbReadBool("/Equipment/IO32/Settings/Veto2_enable");

    int veto1_invert = odbReadBool("/Equipment/IO32/Settings/Veto1_invert");
    int veto2_invert = odbReadBool("/Equipment/IO32/Settings/Veto2_invert");

    int trig1_enable = odbReadBool("/Equipment/IO32/Settings/Trig1_enable");
    int trig2_enable = odbReadBool("/Equipment/IO32/Settings/Trig2_enable");

    cm_msg(MINFO, frontend_name, "IO32 trig1 enable: %d, trig2 enable: %d", trig1_enable, trig2_enable);

    if (!trig1_enable) {
       veto1_enable = 0;
       veto1_invert = 0;
    } else if (!veto1_enable) {
       veto1_enable = 0;
       veto1_invert = 1;
    }

    if (!trig2_enable) {
       veto2_enable = 0;
       veto2_invert = 0;
    } else if (!veto2_enable) {
       veto2_enable = 0;
       veto2_invert = 1;
    }

    if (veto1_enable)
       io32->flip32(4*57, 1<<25, 0, 0);
    
    if (veto1_invert)
       io32->flip32(4*57, 1<<27, 0, 0);

    if (veto2_enable)
       io32->flip32(4*57, 1<<26, 0, 0);
    
    if (veto2_invert)
       io32->flip32(4*57, 1<<28, 0, 0);

    cm_msg(MINFO, frontend_name, "IO32 ALPHA trigger control register 57: 0x%08x", io32->read32(4*57)); 

    // setup the scalers

    odbReadString("/Equipment/IO32/Settings/Names", 0, "", 32);
    odbResizeArray("/Equipment/IO32/Settings/Names", TID_STRING, 32);

    io32->status(); 

    set_equipment_status("IO32", "IO32 init done", "#00FF00"); 
  }

#endif 


#ifdef HAVE_TTC

  int enabled = odbReadBool("/Equipment/TTC/Common/Enabled");

  if (enabled)
     {
        set_equipment_status("VME", "Initializing TTC...", "#00FF00"); 

        if (!gTTC) {
           gTTC = new AlphaTTCX();
           
           int numTtc = odbReadArraySize("/Equipment/TTC/Settings/TTC_Address");
           
           if (numTtc == 0)
              odbResizeArray("/Equipment/TTC/Settings/TTC_Address", TID_DWORD, 2);

           odbResizeArray("/Equipment/TTC/Settings/TTC_Enable", TID_BOOL, numTtc);
           
           for (int i=0; i<numTtc; i++) {
              uint32_t base = odbReadUint32("/Equipment/TTC/Settings/TTC_Address", i);
              bool ena = odbReadBool("/Equipment/TTC/Settings/TTC_Enable", i);

              if (base == 0 || !ena)
                 continue;
              
              if (AlphaTTC::isPresent(gVme, base)) {
                 AlphaTTC* ttc = new AlphaTTC(gVme, base);
                 cm_msg(MINFO, frontend_name, "TTC[%d] A32 address 0x%08x, firmware 0x%08x (%d)", i, base, ttc->fRevision, ttc->fRevision);
                 gTTC->AddTTC(ttc);
              } else {
                 cm_msg(MERROR, frontend_name, "TTC[%d] A32 address 0x%08x, is missing", i, base);
                 return FE_ERR_HW; 
              }
           }
        }

      int oldam = 0;
      mvme_get_am(gVme, &oldam);

      gTTC->init();

      gTTC->ledTest();

      int extClock = odbReadBool("/Equipment/TTC/Settings/ExternalClock", 0, 0);

      if (extClock) {
        status = gTTC->setExternalClock();
        if (status != 0) {
          cm_msg(MERROR, frontend_name, "init_vme_modules: TTC refuses to switch to external clock"); 
          return FE_ERR_HW; 
        }

        cm_msg(MINFO, frontend_name, "init_vme_modules: TTC switched to external clock"); 
      }

      int holdDelay = odbReadInt("/Equipment/TTC/Settings/HoldDelay");

      gTTC->setHoldDelay(holdDelay);

      //if (holdDelay == 0)
      //gTTC->setBits32(0xC0, TTC_C0_ENABLE_FAST_HOLD);
      
      int adcDelay = odbReadInt("/Equipment/TTC/Settings/AdcDelay");

      gTTC->setAdcDelay(adcDelay);

      int busyDelay = odbReadInt("/Equipment/TTC/Settings/BusyDelay");

      gTTC->setBusyDelay(busyDelay);

      cm_msg(MINFO, frontend_name, "TTC delays: HOLD %d, ADC %d, BUSY %d ns", holdDelay, adcDelay, busyDelay); 

      int dacA   = odbReadInt("/Equipment/TTC/Settings/DacA");
      int dacB   = odbReadInt("/Equipment/TTC/Settings/DacB");
      int dacRef = odbReadInt("/Equipment/TTC/Settings/DacRef");

      gTTC->writeDac(0, dacA);
      gTTC->writeDac(1, dacB);
      gTTC->writeDac(2, dacRef);

      gCalPulse = odbReadBool("/Equipment/TTC/Settings/CalPulse");
      gCalPulseOnce = false;

      int testMode = odbReadInt("/Equipment/TTC/Settings/TestMode", 0, 0);
      int testChannel = odbReadInt("/Equipment/TTC/Settings/TestChannel", 0, 20);
      int testHoldDelay = odbReadInt("/Equipment/TTC/Settings/TestHoldDelay", 0, 0);

      if (testMode > 0)
        {
          bool extEna  = false;
          bool trigEna = false;
	  bool seqEna  = false;

	  if (testMode == 1) { // same as ttc_test0a (cal pulse waveform)
	    gTTC->enableHold(false);
	    gTTC->enableReset(false);
            extEna  = false;
            trigEna = false;
	    seqEna  = false;
	  }

	  if (testMode == 2) { // same as ttc_test3a (cal pulse waveform with HOLD)
            gTTC->setHoldDelay(testHoldDelay);
	    gTTC->enableHold(true);
	    gTTC->enableReset(false);
            extEna  = false;
            trigEna = true;
	    seqEna  = false;
	  }
          
	  if (testMode == 3) { // same as ttc_test3a but HOLD is triggered through the TA signals
	    gTTC->enableHold(true);
	    gTTC->enableReset(false);
            extEna  = true;
            trigEna = false;
	    seqEna  = false;
	  }
          
	  if (testMode == 4) { // same as ttc_test4a
            gTTC->setHoldDelay(testHoldDelay);
	    gTTC->enableHold(true);
	    gTTC->enableReset(true);
	    //gTTC->enableReset(false);
            extEna  = false;
            trigEna = true;
	    seqEna  = true;
            gCalPulseOnce = true;
	  }
          
	  if (testMode == 5) { // same as ttc_test4a but triggered through the TA signals
	    gTTC->enableHold(true);
	    gTTC->enableReset(false);
            extEna  = true;
            trigEna = false;
	    seqEna  = true;
	  }
          
	  if (testMode == 40) {
	    gTTC->enableHold(true);
	    gTTC->enableReset(true);
            trigEna = true;
	    seqEna  = true;
	  }
          
          // setup oscilloscope mode
          gTTC->setTestMode(testChannel, extEna, trigEna, seqEna);
        }
      else
        {
          // setup normal readout mode
          
          gTTC->setMuxMode();
          gTTC->enableHold(true);
          gTTC->enableReset(true);
        }

      int numFpga = gTTC->GetNumTTC()*2;
      
      // write trigger FPGA TA enable masks

      if (1)
         {
            gTTC->disableTA();
            
            for (int ifpga=0; ifpga<numFpga; ifpga++)
               {
                  char str[256];
                  sprintf(str, "/Equipment/TTC/Settings/TAdisableMask%d", ifpga);
                  
                  // create the arrays if they do not yet exist
                  odbResizeArray(str, TID_INT, 8);
                  
                  for (int i=0; i<8; i++) {
                     int mask = 0xFFFF & odbReadInt(str, i);
                     gTTC->setTaEnableBitmap(ifpga, i, 0xFFFF & ~mask);
                  }
               }
            
            gTTC->writeTaEnableBitmap();
         }
      
      // write trigger FPGA TA layer mapping table
      
      if (1)
         {
            // create the layer map arrays if they do not yet exist
            
            for (int ifpga=0; ifpga<numFpga; ifpga++)
               {
                  char str[256];
                  sprintf(str, "/Equipment/TTC/Settings/LayerMap%d", ifpga);
                  
                  // create the arrays if they do not yet exist
                  odbResizeArray(str, TID_INT, 32);
                  
                  for (int i=0; i<32; i++) {
                     int layer = 0x3 & odbReadInt(str, i);
                     gTTC->setTaMap(ifpga, i, layer);
                  }
               }
         }

      // write trigger FPGA multiplicity limits
      
      if (isCosmic)
         {
            // disable everything
            for (int ifpga=0; ifpga<numFpga; ifpga++)
               for (int i=0; i<6; i++) {
                  int min = 0;
                  int max = 0;
                  gTTC->setMult(ifpga, i, min, max);
               }

            gTTC->setMult(0, 0, 1, 255);
            gTTC->setMult(1, 1, 1, 255);
            gTTC->setMult(2, 2, 1, 255);

            gTTC->setMult(0, 3, 2, 255);
            gTTC->setMult(1, 4, 2, 255);
            gTTC->setMult(2, 5, 2, 255);

            cm_msg(MINFO, frontend_name, "TTC multiplicity trigger: cosmic trigger: 1-1-1 and 2-2-2");
         }
      else
         {
            int xmult[numFpga][6];

            for (int ifpga=0; ifpga<numFpga; ifpga++)
               {
                  char strmin[256];
                  sprintf(strmin, "/Equipment/TTC/Settings/MultMin%d", ifpga);
                  
                  // create the arrays if they do not yet exist
                  odbResizeArray(strmin, TID_INT, 6);
                  
                  char strmax[256];
                  sprintf(strmax, "/Equipment/TTC/Settings/MultMax%d", ifpga);
                  
                  // create the arrays if they do not yet exist
                  odbResizeArray(strmax, TID_INT, 6);
                  
                  for (int i=0; i<6; i++) {
                     int min = 0xFF & odbReadInt(strmin, i);
                     int max = 0xFF & odbReadInt(strmax, i);
                     
                     gTTC->setMult(ifpga, i, min, max);

                     xmult[ifpga][i] = min;
                  }
               }

            cm_msg(MINFO, frontend_name, "TTC multiplicity trigger: trig1: %d-%d-%d, trig2: %d-%d-%d", xmult[0][0], xmult[1][1], xmult[2][2], xmult[0][3], xmult[1][4], xmult[2][5]);
         }

      // write trigger FPGA output signal routing configuration
      
      if (1)
         {
            for (int ifpga=0; ifpga<numFpga; ifpga++)
               {
                  char str[256];
                  sprintf(str, "/Equipment/TTC/Settings/Trig%d", ifpga);
                  
                  // create the arrays if they do not yet exist
                  odbResizeArray(str, TID_INT, 3);

                  for (int i=0; i<3; i++) {
                     int t = 0xFF & odbReadInt(str, i);
                     gTTC->setTrig(ifpga, i, t);
                  }
               }
         }
      
      // write TTC self-trigger configuration
      
      odbResizeArray("/Equipment/TTC/Settings/regB0", TID_INT, gTTC->GetNumTTC());
      
      for (int ittc=0; ittc<gTTC->GetNumTTC(); ittc++) {
         uint32_t regB0 = odbReadInt("/Equipment/TTC/Settings/regB0", ittc);
         gTTC->fTTC[ittc]->write32(0xB0, regB0);
      }

      // create names for variables

      odbReadString("/Equipment/TTC/Settings/Names", 0, "", NAME_LENGTH);
      odbResizeArray("/Equipment/TTC/Settings/Names", TID_STRING, 7*gTTC->GetNumTTC());
      
      // Done!
      
      gTTC->status();

      mvme_set_am(gVme, oldam);

      if (isCosmic)
         set_equipment_status("TTC", "TTC cosmic trigger", "#00FF00"); 
      else
         set_equipment_status("TTC", "TTC init done", "#00FF00"); 
    }

#endif

#ifdef HAVE_SIS3820

  for (int i=0; gMcsBase[i]!=0; i++)
    {
      sis3820_Reset(gVme,gMcsBase[i]);

      int sismode = odbReadUint32("/Equipment/SisHistory/Settings/SIS_mode", 0, 0);
      if (sismode == 0)
        {
          printf("SIS mode 0: external LNE\n");
          sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_PRESCALE,0);
          sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
                                SIS3820_CLEARING_MODE|
                                SIS3820_MCS_DATA_FORMAT_32BIT|
                                SIS3820_LNE_SOURCE_CONTROL_SIGNAL|
                                SIS3820_FIFO_MODE|
                                SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
                                SIS3820_CONTROL_INPUT_MODE1|
                                SIS3820_CONTROL_OUTPUT_MODE1);
        }
      else if (sismode == 1)
        {
          int prescale = odbReadUint32("/Equipment/SisHistory/Settings/SIS_LNE_prescale", 0, 2000);
          
          printf("SIS mode 1: internal LNE, prescale %d\n", prescale);
          
          // LNE prescale 40 is for max VME rate 30 Mbytes/sec
          sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_PRESCALE,prescale);
          sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
                                SIS3820_CLEARING_MODE|
                                SIS3820_MCS_DATA_FORMAT_32BIT|
                                SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
                                SIS3820_FIFO_MODE|
                                SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
                                SIS3820_CONTROL_INPUT_MODE1|
                                SIS3820_CONTROL_OUTPUT_MODE1);
        }
      sis3820_Status(gVme,gMcsBase[i]);
    }
#endif

#ifdef HAVE_VMEIO
  vmeio_OutputSet(gVme,gVmeioBase,0);
  vmeio_StrobeClear(gVme,gVmeioBase);
  printf("VMEIO at 0x%x CSR is 0x%x\n",gVmeioBase,vmeio_CsrRead(gVme,gVmeioBase));
#endif

#ifdef HAVE_V513
  v513_Reset(gVme,gIoregBase);
  v513_SetChannelMode(gVme,gIoregBase,0,V513_CHANMODE_OUTPUT|V513_CHANMODE_POS|V513_CHANMODE_TRANSP);
  for (int i=1; i<8; i++)
    v513_SetChannelMode(gVme,gIoregBase,i,V513_CHANMODE_OUTPUT|V513_CHANMODE_POS|V513_CHANMODE_TRANSP);
  v513_Status(gVme,gIoregBase);
#endif

#ifdef HAVE_V560
  if (gScaler1Base)
    {
      v560_Reset(gVme,gScaler1Base);
      v560_Status(gVme,gScaler1Base);
    }

  if (gScaler2Base)
    {
      v560_Reset(gVme,gScaler2Base);
      v560_Status(gVme,gScaler2Base);
    }
#endif

#ifdef HAVE_V792
  v792_SingleShotReset(gVme,gAdc1base);
  v792_Status(gVme,gAdc1base);
  status = v792_isPresent(gVme,gAdc1base);
  if (!status) {
    cm_msg(MERROR, frontend_name, "V792 ADC at A24 0x%06x is absent", gAdc1base);
    return FE_ERR_HW;
  }
#endif

#ifdef HAVE_VF48
  set_equipment_status("VME", "Initializing VF48...", "#00FF00"); 

  // create arrays
  odbResizeArray("/equipment/VF48/Settings/VF48_Address",     TID_DWORD, VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/VF48_Enable",      TID_BOOL,  VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/VF48_Divisor",     TID_INT,   VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/VF48_NumSamples",  TID_INT,   VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/VF48_GroupEnable", TID_INT,   VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/VF48_Pretrigger",  TID_INT,   VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/VF48_Latency",     TID_INT,   VF48_NUM);
  //odbResizeArray("/equipment/VF48/Settings/VF48_Threshold",   TID_INT,   VF48_NUM*6);
  //odbResizeArray("/equipment/VF48/Settings/VF48_ChSuppress",  TID_BOOL,  VF48_NUM*6);

  odbResizeArray("/equipment/VF48/Settings/ChSuppThresholds/PerModule", TID_INT, VF48_NUM);
  odbResizeArray("/equipment/VF48/Settings/ChSuppThresholds/PerGroup", TID_INT, VF48_NUM*6);
  odbResizeArray("/equipment/VF48/Settings/ChSuppThresholds/PerChannel", TID_INT, VF48_NUM*6*8);

#define VFST_SIZE 20

  assert(VFST_SIZE >= 2*VF48_NUM);

  odbReadString("/Equipment/VF48/Settings/Names VFST", 0, "", NAME_LENGTH);
  odbResizeArray("/Equipment/VF48/Settings/Names VFST", TID_STRING, VFST_SIZE);
      
  bool haveMissing = false;

  mvme_set_am(gVme, MVME_AM_A24);

  for (int i=0; i<VF48_NUM; i++)
    {
      gVF48enable[i] = odbReadBool("/equipment/VF48/Settings/VF48_enable", i);
      gVF48base[i]   = odbReadInt("/equipment/VF48/Settings/VF48_address", i);

      printf("VF48[%d] at VME A24 0x%x", i, gVF48base[i]);

      if (!gVF48enable[i] || gVF48base[i]==0)
        {
          printf(" - disabled\n");
          continue;
        }

      int base = gVF48base[i];

      if (!vf48_isPresent(gVme, base))
         {
            printf(" - missing\n");
            cm_msg(MERROR, frontend_name, "VF48[%d] A24 address 0x%06x is not responding", i, base);
            haveMissing = true;
            continue;
         }

      printf(" - enabled\n");

      status = vf48_Reset(gVme, base);

      if (status != SUCCESS)
         {
            cm_msg(MERROR, frontend_name, "VF48[%d] A24 address 0x%06x would not reset", i, base);
            haveMissing = true;
            continue;
         }

      uint32_t fwcol  = vf48_RegisterRead(gVme, base, VF48_FIRMWARE_R);
      uint16_t fwfehi = vf48_ParameterRead(gVme, base, 0, VF48_FIRMWARE_ID);
      uint16_t fwfelo = vf48_ParameterRead(gVme, base, 0, VF48_FIRMWARE_ID_LOW);
      uint32_t fwfe   = (fwfehi << 16) | fwfelo;

      cm_msg(MINFO, frontend_name, "VF48[%d] A24 address 0x%06x, firmware: collector 0x%08x, frontend 0x%08x", i, base, fwcol, fwfe);

      //set VF48 trigger threshold high to disable self-trigger circuit
      if (1)
        {
          int trig_thr = 9000;
          //printf("Set TRIG_THRESHOLD to %d\n", trig_thr);
          for (int j=0; j<6; j++)
            vf48_ParameterWrite(gVme, gVF48base[i], j, VF48_TRIG_THRESHOLD, trig_thr);
        }

      int latency = odbReadInt("/equipment/VF48/Settings/VF48_Latency", i, 5);
      for (int j=0; j<6; j++)
        {
          vf48_ParameterWrite(gVme, base, j, VF48_LATENCY, latency);
          //printf("Latency grp %i:%d\n", j, vf48_ParameterRead(gVme, base, j, VF48_LATENCY));
        }

      int common_pretrigger = odbReadInt("/equipment/VF48/Settings/Common_Pretrigger", 0, 0);

      int pretrigger = odbReadInt("/equipment/VF48/Settings/VF48_Pretrigger", i, 0);
      for (int j=0; j<6; j++)
        {
          vf48_ParameterWrite(gVme, base, j, VF48_PRE_TRIGGER, common_pretrigger + pretrigger);
        }

      //set VF48 sub sampling 'divisor' parameter
      int divisor = odbReadInt("/equipment/VF48/Settings/VF48_Divisor", i, 0);
      //printf( "Set divisor to %d\n", divisor);
      if (divisor > 0)
        {
          vf48_DivisorWrite(gVme, gVF48base[i], divisor);
        }

      int perchannel_ena = odbReadBool("/equipment/VF48/Settings/PerChannel_ChSuppress", 0, false);
      int common_ena = odbReadBool("/equipment/VF48/Settings/Common_ChSuppress", 0, false);
      int common_thr = odbReadInt("/equipment/VF48/Settings/Common_Threshold", 0, 0);

      // set channel suppression threshold
      for (int j=0; j<6; j++) {
          if (perchannel_ena) {
             int t[8];
             for (int k=0; k<8; k++)
                t[k] = odbReadInt("/equipment/VF48/Settings/ChSuppThresholds/PerChannel", (i*6 + j)*8 + k, 0);
                
             for (int k=0; k<8; k++)
                if (t[k] == 0)
                   t[k] = common_thr;

             // check range is 8 bits only
             for (int k=0; k<8; k++)
                if (t[k] < 0)
                   t[k] = 0;
                else if (t[k] > 255)
                   t[k] = 255;

             uint16_t T = t[0] | (t[1]<<8);
             uint16_t K = t[2] | (t[3]<<8);
             uint16_t L = t[4] | (t[5]<<8);
             uint16_t M = t[6] | (t[7]<<8);

             vf48_ParameterWrite(gVme, base, j, 2, T);
             vf48_ParameterWrite(gVme, base, j, 6, K);
             vf48_ParameterWrite(gVme, base, j, 7, L);
             vf48_ParameterWrite(gVme, base, j, 8, M);

             uint16_t mbits2 = vf48_ParameterRead(gVme, base, j, VF48_MBIT2);
             mbits2 |= (1<<6);
             vf48_ParameterWrite(gVme, base, j, VF48_MBIT2, mbits2);

          } else {
             int threshold = 0; // odbReadInt("/equipment/VF48/Settings/VF48_Threshold", i*6 + j, 0);
             if (threshold==0)
                threshold = common_thr;
             printf("Set Threshold for group %d to %d\n", j, threshold);
             vf48_ParameterWrite(gVme, base, j, VF48_HIT_THRESHOLD, threshold);
          }
             
          int ena = false; // odbReadBool("/equipment/VF48/Settings/VF48_ChSuppress", i*6 + j, false);
          if (common_ena)
             ena = true;
          if (ena)
             vf48_ChSuppSet(gVme, base, j, 1);
          else
             vf48_ChSuppSet(gVme, base, j, 0);
      }
      
      int segsize = odbReadInt("/equipment/VF48/Settings/VF48_NumSamples", i, 200);
      printf("Set number of time bins to %d\n", segsize);
      for (int j=0; j<6; j++)
        {
          vf48_ParameterWrite(gVme, base, j, VF48_SEGMENT_SIZE, segsize);
          //printf("SegSize grp %i:%d\n", j, vf48_ParameterRead(gVme, base, j, VF48_SEGMENT_SIZE));
        }

      vf48_ExtTrgSet(gVme, base);
      
      vf48_CsrWrite(gVme, base, vf48_CsrRead(gVme, base) | VF48_CSR_BUSY_OUT);
      
      int grpmsk = odbReadInt("/equipment/VF48/Settings/VF48_GroupEnable", i, 0x3F);
      vf48_GrpEnable(gVme, base, grpmsk);
      printf("Group Enable: 0x%x\n", vf48_GrpRead(gVme, base));

#if defined VF48_MULTI 
      vf48_multi_init(gVme, &vf48_mb, i, gVF48base[i], gVF48name[i]); 
#endif 

    } //loop over gVF48bases

  if (haveMissing) {
     cm_msg(MERROR, frontend_name, "Some VF48 modules are missing, see messages");
     return FE_ERR_HW;
  }


  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      vf48_Status(gVme, gVF48base[i]);

#if 0
  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      {
        printf("VF48[%d] at VME A24 0x%06x: Firmware: 0x%x, CSR: 0x%x\n",
               i,
               gVF48base[i],
               vf48_ParameterRead(gVme, gVF48base[i], 0, VF48_FIRMWARE_ID),
               vf48_CsrRead(gVme,gVF48base[i]));
      }
#endif

#ifdef HAVE_VF48
  if (1) {
     for (int i=0; i<VF48_NUM; i++)
        if (gVF48enable[i]) {
           int pll[6];
           for (int j=0; j<6; j++) {
              int par18 = vf48_ParameterRead(gVme, gVF48base[i], j, 18); // PLL lock count
              pll[j] = par18;
              gVF48pllLockCount[i][j] = par18;
           }
           //cm_msg(MINFO, frontend_name, "VF48[%d] at 0x%06x PLL lock counts: %d %d %d %d %d %d (after init)", i, gVF48base[i], pll[0], pll[1], pll[2], pll[3], pll[4], pll[5]);
        }
  }
#endif

  set_equipment_status("VF48", "VF48 init done", "#00FF00"); 

#endif //HAVE_VF48

  set_equipment_status("VME", "VME init done", "#00FF00"); 

  return SUCCESS;
}

EQUIPMENT* findEquipment(const char*name)
{
  for (int i=0; equipment[i].name[0] != 0; i++)
    if (strcmp(equipment[i].name, name) == 0)
      return &equipment[i];
  cm_msg(MINFO, frontend_name, "Cannot find equipment %s", name);
  abort();
}

/*-- Global variables ----------------------------------------------*/

static int gHaveRun        = 0;
static int gRunNumber      = 0;
static int gIsPedestalsRun = 0;
static int gMaxAdcEvents   = 0;
static int gCountEvents    = 0;

#ifdef HAVE_SIS3820

static int    gMcsClockChan = 16;  // SIS channel number where the clock is
static double gMcsClockFreq = 50000000.0; // clock frequency

static int    gMcsAdChan    = -1;  // channel of the AD pulse signal
static char   gMcsAdTalk[256];

static int    gMcsMixChan    = -1;  // channel of the Mixing gate pulse signal
static char   gMcsMixTalk[256];

static int    gMcsPOSChan    = -1;  // channel of the Positron gate pulse signal
static char   gMcsPOSTalk[256];

#endif

#ifdef HAVE_V513
static double gPulserPeriod[8];
#endif

#ifdef HAVE_V792
static EQUIPMENT* gAdcEquipmentPtr = 0;
#endif
#ifdef HAVE_SIS3820
static EQUIPMENT* gSisPulserEquipmentPtr = 0;
static int gSisBufferOverflow = 0;
#endif

static void clear_io32_scaler_sum();
//static void clear_ttc_scaler_sum();

static MUTEX_T *vme_mutex = NULL;
//static char vme_who = 'X';
//static DWORD vme_when_lock = 0;
//static DWORD vme_when_unlock = 0;

#include "msystem.h"

static bool want_vme_lock = false;

void lock_vme()
{
  int timeout = 10000;

  time_t t0 = time(NULL);
  want_vme_lock = true;
  int status = ss_mutex_wait_for(vme_mutex, timeout);
  time_t t1 = time(NULL);
  if (t1-t0 > 2) {
     printf("lock_vme: lock wait %d sec\n", (int)(t1-t0));
  }
  assert(status == SS_SUCCESS);
  want_vme_lock = false;
}

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  printf("frontend_init start!\n");
  int status;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  //cm_enable_watchdog(0);

  /* create a common mutex for the independent lazylogger */
  status = ss_mutex_create(&vme_mutex);

  extern BOOL lockout_readout_thread;
  lockout_readout_thread = 0;

#ifdef HAVE_V792
  gAdcEquipmentPtr       = findEquipment("ADC");
#endif
#ifdef HAVE_SIS3820
  gSisPulserEquipmentPtr = findEquipment("SisPulser");
#endif

  status = mvme_open(&gVme,0);
  status = mvme_set_am(gVme, MVME_AM_A24);

  status = init_vme_modules();
  if (status != SUCCESS)
    {
      printf("status : %d\n",status);
      return status;
    }

  disable_trigger();

  printf("frontend_init done!\n");

  int run_state = odbReadInt("/Runinfo/State");
  int run_number = odbReadInt("/Runinfo/Run number");

  if (run_state == STATE_RUNNING)
    return begin_of_run(run_number, NULL);

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  gHaveRun = 0;
  disable_trigger();

  mvme_close(gVme);

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  gRunNumber = run_number;
  gHaveRun = 1;
  gIsPedestalsRun = odbReadBool("/experiment/edit on start/Pedestals run");
  gCountEvents = 0;

  printf("begin run: %d, is pedestals run: %d\n",run_number,gIsPedestalsRun);

#ifdef HAVE_SIS3820
  al_reset_alarm("sis_overflow");
  gSisBufferOverflow = 0;
#endif

  int status = init_vme_modules();
  if (status != SUCCESS)
    return status;

#ifdef HAVE_IO32
  clear_io32_scaler_sum();
#endif
#ifdef HAVE_TTC
  //clear_ttc_scaler_sum();
#endif

  if (gIsPedestalsRun)
    {
      gMaxAdcEvents = odbReadInt("/experiment/edit on start/Pedestal events");
      cm_msg(MINFO, frontend_name, "Starting ADC pedestals run of %d events", gMaxAdcEvents);

      int period = odbReadInt("/experiment/edit on start/Pedestal period",0,50);

      if (period < 10)
        period = 10;

#ifdef HAVE_SIS3820
      printf("changing ADC pulser period from %d to %d\n",gSisPulserEquipmentPtr->info.period,period);

      gSisPulserEquipmentPtr->info.enabled = 1;
      gSisPulserEquipmentPtr->info.period = period;
#endif
#ifdef HAVE_V792
      gAdcEquipmentPtr->info.period       = period;
#endif
    }
  else
    {
#ifdef HAVE_SIS3820
      bool xxx = odbReadBool("/equipment/SisPulser/common/enabled");
      gSisPulserEquipmentPtr->info.enabled = xxx;
      gSisPulserEquipmentPtr->info.period = odbReadInt("/equipment/SisPulser/common/period");
#endif
#ifdef HAVE_V792
      gAdcEquipmentPtr->info.period = odbReadInt("/equipment/ADC/common/period");
      gMaxAdcEvents = 0;
#endif
    }

#ifdef HAVE_SIS3820
  gMcsClockChan = odbReadInt("/equipment/SisHistory/Settings/ClockChannel",0,16);
  gMcsClockFreq = odbReadDouble("/equipment/SisHistory/Settings/ClockFrequency",0,50000000.0);

  gMcsAdChan = odbReadInt("/equipment/SisHistory/Settings/ADChannel",0,1);
  strlcpy(gMcsAdTalk, odbReadString("/equipment/SisHistory/Settings/ADTalk", 0, "peebars", 200), sizeof(gMcsAdTalk));
  printf("AD pulse is SIS channel %d, say \"%s\"\n",gMcsAdChan,gMcsAdTalk);

  gMcsMixChan = odbReadInt("/equipment/SisHistory/Settings/MixChannel",0,1);
  strlcpy(gMcsMixTalk, odbReadString("/equipment/SisHistory/Settings/MixTalk", 0, "Mix", 200), sizeof(gMcsMixTalk));
  printf("Mixing gate is SIS channel %d, say \"%s\"\n",gMcsMixChan,gMcsMixTalk);
  
  gMcsPOSChan = odbReadInt("/equipment/SisHistory/Settings/POSChannel",0,1);
  strlcpy(gMcsPOSTalk, odbReadString("/equipment/SisHistory/Settings/POSTalk", 0, "Positrons", 200), sizeof(gMcsPOSTalk));
  printf("Positron gate is SIS channel %d, say \"%s\"\n",gMcsPOSChan,gMcsPOSTalk);
#endif

#ifdef HAVE_V513
  // pulser period is in "sec". Value of zero means do not pulse.
  gPulserPeriod[0] = 0; // output bit 0 is the "computer busy"
  for (int i=1; i<8; i++)
    gPulserPeriod[i] = odbReadDouble("/equipment/Pulser/Settings/Period",i,0);
#endif

#ifdef HAVE_V792
  // Reset the Beam Spill number
  
  HNDLE hdir = 0;
  HNDLE hkey;
  
  const char* beamspilldir="/equipment/ADC/Variables/Spill Number";
  int initspill = 0;
  
  status = db_find_key(hDB,hdir, beamspilldir, &hkey);
  if (status != SUCCESS)
    {
      cm_msg(MERROR, frontend_name, "Cannot find \'%s\', db_find_key() status %d", beamspilldir, status);
      return false;
    }
  else
    {
      status = db_set_data_index(hDB, hkey, &initspill, sizeof(initspill), 0, TID_INT);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot write \'%d\' of type %d to odb, db_set_data_index() status %d", initspill, 0, TID_INT, status);
          return false;
        }     
    }
#endif

  enable_trigger();
  
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  gRunNumber = run_number;

  static bool gInsideEndRun = false;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  gHaveRun = 0;
  printf("end run %d\n",run_number);

  lock_vme();

  disable_trigger();

#ifdef HAVE_VF48
  if (1) {
     //for (int i=0; i<VF48_NUM; i++)
     //   if (gVF48enable[i])
     //      vf48_Status(gVme, gVF48base[i]);
     for (int i=0; i<VF48_NUM; i++)
        if (gVF48enable[i]) {
           int pll[6];
           for (int j=0; j<6; j++) {
              pll[j] = vf48_ParameterRead(gVme, gVF48base[i], j, 18); // PLL lock count
              if (pll[j] != gVF48pllLockCount[i][j]) {
                 cm_msg(MERROR, frontend_name, "VF48[%d] at 0x%06x group %d: PLL lock count mismatch: %d at init -> %d at end of run: PLL was reset during run", i, gVF48base[i], j, gVF48pllLockCount[i][j], pll[j]);
              }
           }
           //cm_msg(MINFO, frontend_name, "VF48[%d] at 0x%06x PLL lock counts: %d %d %d %d %d %d (end of run)", i, gVF48base[i], pll[0], pll[1], pll[2], pll[3], pll[4], pll[5]);
        }
  }
#endif

#ifdef HAVE_VF48
  if (1) {
     for (int i=0; i<VF48_NUM; i++)
        if (gVF48enable[i])
           vf48_Status(gVme, gVF48base[i]);
     for (int i=0; i<VF48_NUM; i++)
        if (gVF48enable[i]) {
           for (int j=0; j<6; j++) {
              int nsamples = vf48_ParameterRead(gVme, gVF48base[i], j, 5); // num samples
              int mbits2 = vf48_ParameterRead(gVme, gVF48base[i], j, 11); // mbits2
              int trig_thr = vf48_ParameterRead(gVme, gVF48base[i], j, 15); // trigger threshold
              if (nsamples == 48 || mbits2 == 256 || trig_thr == 10) {
                 cm_msg(MERROR, frontend_name, "VF48[%d] at 0x%06x group %d lost settings during run. nsamples: %d, mbits2 0x%x, trigger threshold %d", i, gVF48base[i], j, nsamples, mbits2, trig_thr);
                 char name[256];
                 char msg[256];
                 sprintf(name, "VF48[%d] group %d fail", i, j);
                 sprintf(msg, "VF48[%d] frontend %d lost it's settings during run", i, j);
                 al_trigger_alarm(name, msg, "Alarm", "", AT_INTERNAL);
              }
           }
        }
  }
#endif

  int vf48ec = -1;

#ifdef HAVE_VF48
  if (1) {
     for (int i=0; i<VF48_NUM; i++)
        if (gVF48enable[i]) {
           int feec[6];
           int fets[6];
           bool febad = false;

           for (int j=0; j<6; j++) {
              feec[j] = vf48_ParameterRead(gVme, gVF48base[i], j, 21); // event counter
              fets[j] = vf48_ParameterRead(gVme, gVF48base[i], j, 22); // event timestamp
           }

           for (int j=0; j<6; j++) {
              if (feec[j] != feec[0])
                 febad = true;
              if (fets[j] != fets[0])
                 febad = true;
           }

           if (febad) {
              cm_msg(MERROR, frontend_name, "VF48[%d] at 0x%06x: event counter or timestamp mismatch", i, gVF48base[i]);
           }

           if (1 || febad) {
              cm_msg(MINFO, frontend_name, "VF48[%d] at 0x%06x: EC: %d %d %d %d %d %d (event counters)", i, gVF48base[i], feec[0], feec[1], feec[2], feec[3], feec[4], feec[5]);
              cm_msg(MINFO, frontend_name, "VF48[%d] at 0x%06x: TS: 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x (timestamps of last event)", i, gVF48base[i], fets[0], fets[1], fets[2], fets[3], fets[4], fets[5]);
           }

           // vf48ec values:
           // >=0 is event counter which is same in all modules
           // -1 not initialized (no modules?)
           // -2 some FE has mismatch
           // -3 some module has mismatch

           if (febad) {
              vf48ec = -2;
           } else {
              if (vf48ec == -1)
                 vf48ec = feec[0];

              if (vf48ec < 0) { // already have a mismatch, skip
              } else {
                 if (feec[0] != vf48ec) {
                    cm_msg(MINFO, frontend_name, "VF48[%d] at 0x%06x: event counter mismatch: expected %d, got %d", i, gVF48base[i], vf48ec, feec[0]);
                    vf48ec = -3;
                 }
              }
           }
        }

     if (vf48ec < 0)
        cm_msg(MERROR, frontend_name, "VF48 event counter mismatch, value %d!", vf48ec);
     else
        cm_msg(MINFO, frontend_name, "VF48 event counter: %d", vf48ec);
  }
#endif

#ifdef HAVE_TTC
  if (1) {
     int num_ttc  = gTTC->GetNumTTC();
     int num_fpga = 2*num_ttc;

     for (int ittc=0; ittc<num_ttc; ittc++) {
        uint32_t regC4 = gTTC->fTTC[ittc]->read32(0xC4); // general status
        int pll1 = regC4 & TTC_C4_PLL1_UNLOCKED;
        int pll2 = regC4 & TTC_C4_PLL2_UNLOCKED;
        printf("TTC %d, regC4: 0x%08x, PLL1 %d, PLL2 %d\n", ittc, regC4, pll1, pll2);
        cm_msg(MINFO, frontend_name, "TTC %d, regC4: 0x%08x, PLL1 %d, PLL2 %d", ittc, regC4, pll1, pll2);
     }
     
     for (int ifpga=0; ifpga<num_fpga; ifpga++) {
        uint32_t reg3 = gTTC->readTriggerFPGA(ifpga, 3);
        int ec16 = reg3 & 0xFFFF;
        
        printf("TTC FPGA %d, reg3: 0x%08x, event counter %d\n", ifpga, reg3, ec16);

        cm_msg(MINFO, frontend_name, "TTC event counter FPGA %d: %d (0x%04x)", ifpga, ec16, ec16);
     } 
  }
#endif

  ss_mutex_release(vme_mutex);

  gInsideEndRun = false;

#ifdef HAVE_V792
  //reset the spill count
  HNDLE hdir = 0;
  HNDLE hkey;
  
  const char* beamspilldir="/equipment/ADC/Variables/Spill Number";
  int initspill = 0;
  
  int status = db_find_key(hDB,hdir, beamspilldir, &hkey);
  if (status != SUCCESS)
    {
      cm_msg(MERROR, frontend_name, "Cannot find \'%s\', db_find_key() status %d", beamspilldir, status);
      return false;
    }
  else
    {
      status = db_set_data_index(hDB, hkey, &initspill, sizeof(initspill), 0, TID_INT);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot write \'%d\' of type %d to odb, db_set_data_index() status %d", initspill, 0, TID_INT, status);
          return false;
        }     
    }
#endif

  cm_msg(MINFO, frontend_name, "Run %d finished, generated %d events of all types", run_number, gCountEvents);

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gHaveRun = 0;
  gRunNumber = run_number;
  disable_trigger();
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gHaveRun = 1;
  gRunNumber = run_number;
  enable_trigger();
  return SUCCESS;
}

static int gVmeIsIdle = 0;

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  if (gVmeIsIdle) {
    cm_yield(97);
  }
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  assert(!"Not implemented");
}

/*-- Event readout -------------------------------------------------*/

#ifdef HAVE_V792
int read_v792(int base,const char*bname,char*pevent,int nchan)
{
  int i;
  int wcount = 0;
  DWORD data[100];
  DWORD counter = 0;
  WORD* pdata;
  
  /* Event counter */
  v792_EvtCntRead(gVme, base, &counter);
  
  /* Read Event */
  v792_EventRead(gVme, base, data, &wcount);
  
  /* create ADCS bank */
  bk_create(pevent, bname, TID_WORD, (void**)&pdata);
  
  for (i=0; i<32; i++)
    pdata[i] = 0;
  
  for (i=0; i<wcount; i++)
    {
      uint32_t w = data[i];
      if (((w>>24)&0x7) != 0) continue;
      int chan = (w>>16)&0x1F;
      int val  = (w&0x1FFF);
      pdata[chan] = val;
    }
  
  //printf("counter %6d, words: %3d, header: 0x%08x, ADC0: 0x%08x, ADC0,1,2: %6d %6d %6d\n",counter,wcount,data[0],pdata[0],pdata[0],pdata[1],pdata[2]);
  
  pdata += nchan;
  bk_close(pevent, pdata);

  return wcount;
}
#endif

#ifdef HAVE_SIS3820
static const int kNumSisModules    =  2;   // number of SIS modules
static const int kSisChanPerModule = 32;   // number of channels in each SIS module
static const int kNumSisChannels = kNumSisModules*kSisChanPerModule; // total number of SIS channels
static uint32_t  gSumMcsEvents[kNumSisModules]; // sum the number of events
static uint32_t  gMaxMcs[kNumSisChannels];  // max value of SIS channels
static uint64_t  gSumMcs[kNumSisChannels];  // sum SIS channels
static uint32_t  gSaveMcs[kNumSisChannels]; // sampled SIS data
#endif

static uint32_t mcs_bc = 0;

static double timeDiff(const struct timeval&t1,const struct timeval&t2)
{
  return (t1.tv_sec - t2.tv_sec) + 0.000001*(t1.tv_usec - t2.tv_usec);
}

#ifdef HAVE_SIS3820
static struct timeval gSisLastRead[kNumSisModules];

int SisDataReady(int i)
{
   if (gMcsBase[i])
      return sis3820_DataReady(gVme,gMcsBase[i]);
   else
      return 0;
}

static int have_SIS_data(int i, bool forceRead)
{
  struct timeval now;

  int haveData = SisDataReady(i);
  if (haveData == (-1))
    haveData = 0;

  int minread  = 1000*32;
  //int minread  = 64*1024;

  gettimeofday(&now,NULL);

  double td = timeDiff(now, gSisLastRead[i]);

  //  printf("sis: %d, td: %f, haveData: %d\n",i,td,haveData);

  if (td < 1.5 && haveData < minread && !forceRead)
    return 0;

  //int toRead = minread;
  //int toRead = 128*1024/4;
  int toRead = max_event_size/4 - 5000;

  if (toRead > haveData)
    toRead = haveData;

  if (0)
    if (toRead & 0x1F)
      printf("odd available data: %d (%d)\n",toRead,toRead&0x1F);

  // make sure we always read in multiples of 32 words
  toRead = toRead & 0xFFFFFFE0;

  if (toRead == 0)
    return 0;

  return toRead;
}

static int read_SIS(char* pevent, int isis, int toRead32)
{
  assert(isis >= 0);
  assert(isis < kNumSisModules);

  //if (toRead32*4 < 20000)
  //  return 0;
  //
  //toRead32 = 32*1;

  int used = bk_size(pevent);
  int free = max_event_size - used;

  if (toRead32*4 > free)
    {
      //printf("read_SIS: buffer used %d, free %d, toread %d\n", used, free, toRead32*4);
      toRead32 = (free-10*1024)/4;
      toRead32 = toRead32 & 0xFFFFFFE0;

      if (toRead32 <= 0)
        return 0;
    }

  int maxDma = 256*1024;
  if (toRead32*4 > maxDma)
    toRead32 = maxDma/4;

  struct timeval now;
  gettimeofday(&now,NULL);

  double td = timeDiff(now, gSisLastRead[isis]);

  gSisLastRead[isis] = now;

  /* create data bank */
  uint32_t *pdata32;

  char bankname[] = "MCS0";
  bankname[3] = '0' + isis; 
  bk_create(pevent, bankname, TID_DWORD, (void**)&pdata32);

  int rd = sis3820_FifoRead(gVme,gMcsBase[isis],pdata32,toRead32);

  if (rd != toRead32) {
     cm_msg(MERROR, frontend_name, "read_SIS: unit %d, fifo read for %d words returned %d", isis, toRead32, rd);
     //printf("SIS %d, have %6d, requested %6d words, read %6d words, dt %f\n", isis, SisDataReady(isis), toRead32, rd, td);
     if (rd < 0)
        toRead32 = 0;
  }

  //printf("SIS %d, have %6d, read %6d words, dt %f\n", isis, sis3820_DataReady(gVme,gMcsBase[isis]), toRead32, td);

  mcs_bc += toRead32*4;

  if (0)
    {
      printf("sis3820 data: 0x%x 0x%x 0x%x 0x%x\n",pdata32[16],pdata32[17],pdata32[18],pdata32[19]);
    }

  if (0)
    {
      printf("sis3820 data: rd: %d,",rd);
      for (int i=0; i<toRead32; i++)
        printf(" 0x%x",pdata32[i]);
      printf("\n");
    }

  bk_close(pevent, pdata32 + toRead32);

  int numEvents = toRead32/kSisChanPerModule;
  uint32_t *mptr = pdata32;
  int offset = isis*kSisChanPerModule;
  for (int ievt=0; ievt<numEvents; ievt++)
    {
      gSumMcsEvents[isis]++;
      for (int i=0; i<kSisChanPerModule; i++)
        {
          uint32_t v = *mptr++;
          gSumMcs[offset+i] += v;
          gSaveMcs[offset+i] = v;
          if (v > gMaxMcs[offset+i])
            gMaxMcs[offset+i] = v;
        }
    }

  return rd*4;
}
#endif

#ifdef HAVE_V560
INT read_scaler_event(char *pevent, INT off)
{
  const  int            kNumScalers = 32;
  static struct timeval gTime;
  static uint32_t       gCounts[kNumScalers];

  //v560_Status(gVme,gScalerBase);

  /* init bank structure */
  bk_init32(pevent);

  uint32_t *pdata32;
  bk_create(pevent, "SCLR", TID_DWORD, &pdata32);

  lock_vme();

  struct timeval t;
  gettimeofday(&t,NULL);

  v560_Read(gVme,gScaler1Base,pdata32);
  v560_Read(gVme,gScaler2Base,pdata32+16);

  ss_mutex_release(vme_mutex);

  //printf("pdata 0x%08x 0x%08x\n", pdata32[0], pdata32[15]);

  bk_close(pevent, pdata32+kNumScalers);

  double t1 = gTime.tv_sec + 0.000001*gTime.tv_usec;
  double t2 = t.tv_sec + 0.000001*t.tv_usec;
  double dt = 1.0/(t2-t1);

  if (t1 == 0)
    dt = 0;

  float *pfloat;
  bk_create(pevent, "SCRT", TID_FLOAT, &pfloat);
  for (int i=0; i<kNumScalers; i++)
    pfloat[i] = (pdata32[i]-gCounts[i])*dt;
  bk_close(pevent, pfloat+kNumScalers);

  gTime = t;
  for (int i=0; i<kNumScalers; i++)
    gCounts[i] = pdata32[i];
  
  return bk_size(pevent);
}
#endif

#ifdef HAVE_V513
INT read_pulser_event(char *pevent, INT off)
{
  static double gNextTime[8];

  struct timeval now;
  gettimeofday(&now,NULL);
  double t = now.tv_sec + 0.000001*now.tv_usec;

  uint32_t mask = 0;
  for (int i=1; i<8; i++)
    if (gPulserPeriod[i] > 0)
      if (t > gNextTime[i])
	{
	  if (gNextTime[i] == 0)
	    gNextTime[i] = t;
	  gNextTime[i] += gPulserPeriod[i];
	  //printf("pulse %d!\n",i);
	  mask |= (1<<i);
	}

  if (mask)
    {
      lock_vme();
      v513_Write(gVme,gIoregBase,gIoreg|mask);
      v513_Write(gVme,gIoregBase,gIoreg);
      ss_mutex_release(vme_mutex);
    }
  return 0;
}
#endif

#ifdef HAVE_V513
INT read_latch_event(char *pevent, INT off)
{
  uint16_t latch = 0;
  lock_vme();
  latch = v513_Read(gVme,gIoregBase);
  //printf("latch 0x%02x\n",latch);
  ss_mutex_release(vme_mutex);

  /* init bank structure */
  bk_init32(pevent);

  int *pdata;
  bk_create(pevent, "LAT0", TID_INT, &pdata);

  for (int i=0; i<16; i++)
    *pdata++ = (latch & (1<<i))?1:0;
  bk_close(pevent, pdata);

  return bk_size(pevent);
}
#endif

INT read_sispulser_event(char *pevent, INT off)
{
  //printf("Pulse SIS3820 NIM output\n");
#ifdef HAVE_SIS3820
  lock_vme(); 
  // issue a PCI read to flush posted PCI writes
  uint32_t d1 = sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS);
  //usleep(1);
  sis3820_RegisterWrite(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS,1<<16);
  // issue a PCI read to flush posted PCI writes
  uint32_t d2 = sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS);
  ss_mutex_release(vme_mutex);
  return 0*(d1+d2);
#else
  return 0;
#endif
}

#ifdef HAVE_V792
INT read_adc_event(char *pevent, INT off)
{
  lock_vme();

  int haveData = v792_DataReady(gVme,gAdc1base);

  //v792_Status(gVme,gAdc1base);
  //printf("read adc event, have data: %d\n", haveData);

  if (!haveData) {
    ss_mutex_release(vme_mutex);
    return 0;
  }

  if (gMaxAdcEvents > 0)
    {
      printf("max events: %d\n",gMaxAdcEvents);
      gMaxAdcEvents--;
      if (gMaxAdcEvents == 0)
	{
	  cm_msg(MINFO, frontend_name, "Enough ADC pedestals events, requesting end of run");
	  cm_transition(TR_STOP,gRunNumber,NULL,0,ASYNC,1);
	}
    }

  /* init bank structure */
  bk_init32(pevent);
  
  read_v792(gAdc1base,"ADC",pevent,32);

  if (v792_DataReady(gVme,gAdc1base))
    {
      if (gIsPedestalsRun)
	{
	  gAdcEquipmentPtr->last_called = 0;
	}
      else
	{
	  printf("read_adc_event: ADC gate is double pulsing!\n");
	  v792_SingleShotReset(gVme,gAdc1base);
	}
    }

  ss_mutex_release(vme_mutex);
   
  // For every ADC gate, increase the spill number by one
  int beamspill;
  HNDLE hdir = 0;
  HNDLE hkey;
  
  const char* beamspilldir="/equipment/ADC/Variables/Spill Number";
  
  if(!gIsPedestalsRun)
  {
    beamspill = odbReadInt(beamspilldir);
    beamspill++;
  
    status = db_find_key(hDB,hdir, beamspilldir, &hkey);
    if (status != SUCCESS)
      {
        cm_msg(MERROR, frontend_name, "Cannot create \'%s\', db_find_key() status %d", beamspilldir, status);
        return false;
      }
    else
      {
        status = db_set_data_index(hDB, hkey, &beamspill, sizeof(beamspill), 0, TID_INT);
        if (status != SUCCESS)
          {
            cm_msg(MERROR, frontend_name, "Cannot write \'%d\' of type %d to odb, db_set_data_index() status %d", beamspill, 0, TID_INT, status);
            return false;
          }  
        //printf("Spill %d\n",beamspill);
      }
  }
  return bk_size(pevent);
}
#endif

#ifdef HAVE_SIS3820
static bool gForceReadMcs = false;

INT read_mcs_event(char *pevent, INT off)
{
  //time_t ttt = time(NULL);
  //DWORD ttt1 = ss_millitime();

  //printf("read_mcs_event: time %d, lock [%c] after %d ms\n", (int)ttt, vme_who, ttt1-vme_when_lock);

  lock_vme();

  //DWORD ttt2 = ss_millitime();

  //printf("read_mcs_event: time %d, wait %d ms, who [%c], lock %d unlock %d, time %d\n", (int)ttt, (int)(ttt2-ttt1), vme_who, vme_when_lock, vme_when_unlock, vme_when_unlock-vme_when_lock);

  int haveData0 = SisDataReady(0);
  int haveData1 = SisDataReady(1);

  gForceReadMcs = true;

  ss_mutex_release(vme_mutex);

  const int kSisBufferSize = 16000000;
  if (haveData0 > kSisBufferSize || haveData1 > kSisBufferSize)
    if (!gSisBufferOverflow)
      {
        gSisBufferOverflow = 1;
        al_trigger_alarm("sis_overflow", "SIS buffer overflow: LNE rate is too high",
                         "Warning", "", AT_INTERNAL);
      }

  //printf("read mcs event, have data: %d\n", haveData);

  /* init bank structure */
  bk_init32(pevent);

  uint32_t *pdata32;
  bk_create(pevent, "MCMX", TID_DWORD, (void**)&pdata32);
  for (int i=0; i<kNumSisChannels; i++) // kNumSisChannels -> static var : max number of SIS channels
    pdata32[i] = gMaxMcs[i];    // Max value of SIS channel
  bk_close(pevent, pdata32+kNumSisChannels);

  bk_create(pevent, "MCSA", TID_DWORD, (void**)&pdata32);
  for (int i=0; i<kNumSisChannels; i++)
    pdata32[i] = gSaveMcs[i];  // Sampled SIS data
  bk_close(pevent, pdata32+kNumSisChannels);

  if (false)
    {
      printf("MCS fifo: %12d, bc: %12d, 0x%08x, %6d MiB, data: %d %d %d %d\n", haveData0, mcs_bc, mcs_bc, mcs_bc/(1024*1024), gSaveMcs[16], gSaveMcs[17], gSaveMcs[18], gSaveMcs[19]);
    }

  uint32_t numClocks = gSumMcs[gMcsClockChan];
  double dt = numClocks/gMcsClockFreq;
  double dt1 = 0;
  if (dt > 0)
    dt1 = 1.0/dt; 

#if 0
  static struct timeval gTime;

  struct timeval t;
  gettimeofday(&t,NULL);

  double t1 = gTime.tv_sec + 0.000001*gTime.tv_usec;
  double t2 = t.tv_sec + 0.000001*t.tv_usec;
  double dt1 = 1.0/(t2 - t1);

  if (t1 == 0)
    dt1 = 0;
#endif

  float *pfloat;
  bk_create(pevent, "MCRT", TID_FLOAT, (void**)&pfloat);
  for (int i=0; i<kNumSisChannels; i++)
    pfloat[i] = gSumMcs[i]*dt1;  // sample RaTe in Hz
  bk_close(pevent, pfloat+kNumSisChannels);

#if 0
  gTime = t;
#endif

  bk_create(pevent, "MCST", TID_FLOAT, (void**)&pfloat);
  for (int i=0; i<10; i++)
    pfloat[i] = 0;
  pfloat[0] = haveData0;         // amount of data still buffered in the SIS
  pfloat[1] = gSumMcsEvents[0];  // number of LNE events from the SIS
  pfloat[2] = gSumMcsEvents[0]*dt1; // LNE frequency
  pfloat[3] = haveData1;         // amount of data still buffered in the SIS
  pfloat[4] = gSumMcsEvents[1];  // number of LNE events from the SIS
  pfloat[5] = gSumMcsEvents[1]*dt1; // LNE frequency
  pfloat[6] = haveData0 - haveData1;
  bk_close(pevent, pfloat+10);
  
  if (gMcsAdChan >= 0 && !gIsPedestalsRun)
    if (gSumMcs[gMcsAdChan] > 0)
      {
        //printf("bingo %d %d\n",gMcsAdChan,(int)gSumMcs[gMcsAdChan]);
        cm_msg(MTALK,frontend_name,gMcsAdTalk);
      }

  if (gMcsMixChan >= 0 && !gIsPedestalsRun)
    if (gSumMcs[gMcsMixChan] > 0)
      {
        //printf("bingo %d %d\n",gMcsMixChan,(int)gSumMcs[gMcsMixChan]);
        cm_msg(MTALK,frontend_name,gMcsMixTalk);
      }
      
  if (gMcsPOSChan >= 0 && !gIsPedestalsRun)
    if (gSumMcs[gMcsPOSChan] > 0)
      {
        //printf("bingo %d %d\n",gMcsMixChan,(int)gSumMcs[gMcsMixChan]);
        cm_msg(MTALK,frontend_name,gMcsPOSTalk);
      }

  for (int i=0; i<kNumSisModules; i++)
    gSumMcsEvents[i] = 0;
  for (int i=0; i<kNumSisChannels; i++)
    gSumMcs[i] = 0;

  return bk_size(pevent);
}
#endif

static int have_VF48_data(int i)
{
#ifdef HAVE_VF48
  int haveData = vf48_NFrameRead(gVme,gVF48base[i]);
  //if (haveData != 0)
  //  printf("vf48 %d: have data %d\n", i, haveData);
  haveData &= ~3;
  if (haveData > 0)
    return haveData;
#endif
  return 0;
}

#ifdef HAVE_VF48
static int read_vf48_module(char* pevent, int i, int toRead)
{
  uint32_t *pdata32;
  char bankname[] = "VFA0";
  bankname[3] = '0' + i;
  bk_create(pevent, bankname, TID_DWORD, (void**)&pdata32);

#if 1
  int count = toRead;
  int xwc = vf48_DataRead(gVme, gVF48base[i], pdata32, &count);
#else
  int xwc = 0;
  while (1)
    {
      int count = have_VF48_data(i);
      //printf("have %d, read %d\n", xwc, count);
      if (count <= 0)
        break;
      int wc = vf48_DataRead(gVme, gVF48base[i], pdata32+xwc, &count);

      if (wc == 0)
	printf("error reading %d words\n", count);

      xwc += wc;

      if (xwc > 40000)
        break;
    }
#endif

  //printf("vf48 unit %d: read %d words\n", i, xwc);
  
  bk_close(pevent, pdata32+xwc);

  gVF48countReads[i] += 1;
  gVF48countBytes[i] += xwc*4;

  return xwc*4;
}
#endif

#ifdef HAVE_VF48
INT read_vf48_event(char *pevent, INT off)
{
  /* init bank structure */
  bk_init32(pevent);

  static struct timeval gTime;
  struct timeval t;
  gettimeofday(&t,NULL);

  double t1 = gTime.tv_sec + 0.000001*gTime.tv_usec;
  double t2 = t.tv_sec + 0.000001*t.tv_usec;
  double dt1 = 1.0/(t2 - t1);

  if (t1 == 0)
    dt1 = 0;

  gTime = t;

  assert(VFST_SIZE >= 2*VF48_NUM);

  float *pfloat;
  bk_create(pevent, "VFST", TID_FLOAT, (void**)&pfloat);

  for (int i=0; i<2*VF48_NUM; i++)
    pfloat[i] = 0;

  static uint64_t gSaveCountReads[VF48_NUM];
  static uint64_t gSaveCountBytes[VF48_NUM];

  for (int i=0; i<VF48_NUM; i++)
    {
      pfloat[i] = dt1*(gVF48countReads[i] - gSaveCountReads[i]);
      pfloat[VF48_NUM+i] = dt1*(gVF48countBytes[i] - gSaveCountBytes[i]);

      gSaveCountReads[i] = gVF48countReads[i];
      gSaveCountBytes[i] = gVF48countBytes[i];
    }

  for (int i=2*VF48_NUM; i<VFST_SIZE; i++)
     pfloat[i] = 0;

  bk_close(pevent, pfloat+VFST_SIZE);
  
  return bk_size(pevent);
}
#endif

static int have_ADC_data()
{
  return 0;
}

extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  //printf("poll_event %d %d %d!\n",source,count,test);

  for (int i = 0; i < count; i++)
    {
      //udelay(1);
      if (test)
        xusleep(1000);
      else
        return TRUE;
    }
  return 0;

#if 0
  for (int i = 0; i < count; i++)
    {
      //udelay(1);
      int lam = 0;
      lam |= have_ADC_data();
#ifdef HAVE_SIS3820
      for (int i=0; gMcsBase[i]!=0; i++)
        lam |= have_SIS_data(i);
#endif
#ifdef HAVE_VF48
      for (int i=0; i<VF48_NUM; i++)
        if (gVF48enable[i])
          lam |= have_VF48_data(i);
#endif
      if (lam)
        if (!test)
          return lam;
    }
  return 0;
#endif
}

INT read_vme_event(char *pevent, INT off)
{
  int status;
  static int gCountEmpty = 0;
  int size = 0;

#if XTIME
  static DWORD gPrevTime = 0;
  DWORD start_time = ss_millitime();
#endif

  uint32_t start_time = 0;
  uint32_t trig_count = SERIAL_NUMBER(pevent);

  uint32_t start_vf48_time = 0;
  uint32_t nbytes_vf48 = 0;
  uint32_t end_vf48_time = 0;

  uint32_t start_ttc_time = 0;
  uint32_t nbytes_ttc = 0;
  uint32_t end_ttc_time = 0;

  uint32_t start_sis3820_time = 0;
  uint32_t nbytes_sis3820 = 0;
  uint32_t end_sis3820_time = 0;

  status = ss_mutex_wait_for(vme_mutex, 10000);
  assert(status == SS_SUCCESS);

  //vme_who = 'V';
  //vme_when_lock = ss_millitime();

#ifdef HAVE_IO32
  start_time = io32->read32(IO32_TS); // read the TSC 
#else
  start_time = rdtscl();
#endif
 
  //printf("read_vme_event!\n");

#ifdef HAVE_VF48

#ifdef HAVE_IO32 
  start_vf48_time = io32->read32(IO32_TS); // read the time stamp
#else
  start_vf48_time = rdtscl();
#endif

  bool xdid_read_vf48 = false;

#ifdef VF48_MULTI
  if (1) 
    { 
      //uint32_t t0 = io32->read32(IO32_TS); 
      //status = vf48_read_event(myvme, &vf48_mb, pevent, max_event_size, SERIAL_NUMBER(pevent)+1); 
      ////printf("vf48_read_event() status %d\n", status); 

      int count = 0;
      for (int loop=0; loop<200; loop++)
        {
          int rd = vf48_multi_read(gVme, &vf48_mb, 0); // nodebug
          if (rd < 0) // error
            break;
          if (rd == 0)
            break;
          if (rd > 0)
            count += rd;
          if (count > 1*1024*1024)
            break;

          if (0 && loop>0)
            printf("VF48 loop %d, read %d, total %d\n", loop, rd, count);
        }

      if (count > 0) {
        if (!size)
          bk_init32(pevent);
        size |= 1;

        nbytes_vf48 += vf48_multi_bank_stream(&vf48_mb, pevent, max_event_size/2);
      }

      if (nbytes_vf48 > 0) {
        xdid_read_vf48 = true;

        for (int i=0; i<VF48_NUM; i++)
           if (gVF48enable[i]) {
              gVF48countReads[i] += vf48_mb.vf48_count_reads[i];
              gVF48countBytes[i] += vf48_mb.vf48_count_bytes[i];
              vf48_mb.vf48_count_reads[i] = 0;
              vf48_mb.vf48_count_bytes[i] = 0;
           }
      }

      if (0 && nbytes_vf48 > 0) {
        printf("VF48 multi bank %d bytes\n", nbytes_vf48);
        printf("max_event_size %d, bk_size %d\n", max_event_size, bk_size(pevent));
      }
    }
#else

#if 0
  for (int iter=0; iter<1000; iter++) {
#endif
    bool did_read_vf48 = false;
    for (int i=0; i<VF48_NUM; i++)
      if (gVF48enable[i])
        {
          int haveData = have_VF48_data(i);
          //printf("read VF48 %d, have data %d\n", i, haveData);
          if (haveData > 200)
            {
	      //printf("read VF48 %d, have data %d\n", i, haveData);

              if (!size)
                bk_init32(pevent);
              
              nbytes_vf48 += read_vf48_module(pevent, i, haveData);
              size |= 1;
              did_read_vf48 = true;
              xdid_read_vf48 = true;
            }
        }

    //if (iter>0)
    //printf("VF48 iter %d, nbytes %d, did_read %d\n", iter, nbytes_vf48, did_read_vf48);

#if 0
    if (nbytes_vf48 > 1*1024*1024)
      break;
    if (!did_read_vf48)
      break;
  }

#endif

#endif

  //printf("did read %d, TTC C4: 0x%08x\n", xdid_read_vf48, gTTC->read32(0xC4));

#if 0
  if (!xdid_read_vf48) {
    uint32_t reg= gTTC->read32(0xC4);
    if (reg & TTC_C4_BUSY) {
      //printf("no VF48 data, but TTC is busy, clear it!\n");
      gTTC->writeCommand(TTC_CMD_Reset);
    }
  }
#endif

#ifdef HAVE_IO32 
  end_vf48_time = io32->read32(IO32_TS); // read the time stamp
#else
  end_vf48_time = rdtscl();
#endif

#endif

#ifdef HAVE_TTC
   if (gTTC)
    {
      if (!size)
        bk_init32(pevent);

#ifdef HAVE_IO32 
      start_ttc_time = io32->read32(IO32_TS); // read the time stamp
#else
      start_ttc_time = rdtscl();
#endif

      // read the TTC event latch fifo

      uint16_t *pdata16;

      int num_fpga = 2*gTTC->GetNumTTC();

      for (int ifpga=0; ifpga<num_fpga; ifpga++)
        {
          uint32_t reg32 = gTTC->readTriggerFPGA(ifpga, 32);

          int len   = reg32 & 0x3FFF;
          int empty = reg32 & 0x4000;
          int full  = reg32 & 0x8000;

          if (0 && !empty)
             printf("fpga %d, reg32 0x%08x, len %d, full %d, empty %d\n", ifpga, reg32, len, full, empty);

          if (empty)
            continue;

          char str[4];
          str[0] = 'T';
          str[1] = 'E';
          str[2] = 'L';
          str[3] = '0' + ifpga;
          bk_create(pevent, str, TID_WORD, (void**)&pdata16);

          // for (int i=0; i<len; i++)
          //   pdata16[i] = 0xFFFF & gTTC->readTriggerFPGA(ifpga, 33);

          int rd = gTTC->readLatchFifo(ifpga, (char*)pdata16, 10000);

          if (0)
            {
              printf("fpga %d, reg32 0x%08x, len %d, full %d, empty %d\n", ifpga, reg32, len, full, empty);
            }

          if (0)
            {
              printf("data: 0x");
              for (int i=0; i<rd/2; i++)
                printf(" %04x", pdata16[i]);
              printf("\n");
            }

          bk_close(pevent, pdata16 + rd/2);

          nbytes_ttc += rd;

          size |= len;
        }

#ifdef HAVE_IO32 
      end_ttc_time = io32->read32(IO32_TS); // read the time stamp
#else
      end_ttc_time = rdtscl();
#endif
    }
#endif

#ifdef HAVE_SIS3820

#ifdef HAVE_IO32 
  start_sis3820_time = io32->read32(IO32_TS); // read the time stamp
#else
  start_sis3820_time = rdtscl();
#endif

  if (1) {

    int count[kNumSisModules];
    
    // each SIS has how much data?
    for (int i=0; gMcsBase[i]; i++)
      count[i] = have_SIS_data(i, gForceReadMcs);
    
    // which SIS has the least data
    int min = count[0];
    for (int i=1; gMcsBase[i]; i++)
      if (count[i] < min)
        min = count[i];

    bool doReadSis = true;

    if (nbytes_vf48 > 0)
      doReadSis = false;

    if (gForceReadMcs)
       doReadSis = true;

    gForceReadMcs = false;

    int haveData0 = SisDataReady(0);
    int haveData1 = SisDataReady(1);

    if (haveData0 > 1000000)
      doReadSis = true;
    
    if (haveData1 > 1000000)
      doReadSis = true;
    
    if (0 && doReadSis)
      printf("SIS counts %d: %d %d, fifo %d %d, VF48 %d\n", min, count[0], count[1], SisDataReady(0), SisDataReady(1), nbytes_vf48);
    
#if 0  
    if (doReadSis && min > 0)
      for (int i=0; gMcsBase[i]; i++)
        {
          if (!size)
            bk_init32(pevent);
          size |= read_SIS(pevent, i, min);
        }
#endif
    
#if 1
    if (doReadSis && count[0] > 0)
      {
        if (!size)
          bk_init32(pevent);
        nbytes_sis3820 += read_SIS(pevent, 0, count[0]);
        if (nbytes_sis3820 > 0)
          size |= 1;
      }
#endif
    
#if 1
    if (doReadSis && count[1] > 0)
      {
        if (!size)
          bk_init32(pevent);
        nbytes_sis3820 += read_SIS(pevent, 1, count[1]);
        if (nbytes_sis3820 > 0)
          size |= 1;
      }
#endif
  }
    
  int haveData0 = SisDataReady(0);
  int haveData1 = SisDataReady(1);

#ifdef HAVE_IO32 
  end_sis3820_time = io32->read32(IO32_TS); // read the time stamp
#else
  end_sis3820_time = rdtscl();
#endif

#endif

#if XTIME
  DWORD end_time = ss_millitime();
  static int gCount = 0;
 
  printf("%d/%d/%d...", gCount++, end_time - start_time, start_time - gPrevTime);
  gPrevTime = end_time;
#endif

  uint32_t end_time = 0; 
#ifdef HAVE_IO32 
  end_time = io32->read32(IO32_TS); // read the time stamp
#else
  end_time = rdtscl();
#endif 

  static uint32_t prev_start_time = 0;

  if (size)
    { 
      uint32_t *pdata32; 
      bk_create(pevent, "VTRI", TID_DWORD, (void**)&pdata32); 
      *pdata32++ = trig_count-1;// 0 - event number, counting from 0 
      *pdata32++ = start_time - prev_start_time;  // 1 - time between events
      //*pdata32++ = start_time;  // 1 - readout start time 
      //*pdata32++ = end_time;    // 2 - readout end time 
      *pdata32++ = end_time-start_time;                  // 3 - readout elapsed time
      *pdata32++ = end_vf48_time-start_vf48_time;        // 4 - VF48 readout elapsed time
      *pdata32++ = nbytes_vf48;                          // 5 - VF48 number of bytes
#ifdef HAVE_VF48
      *pdata32++ = 4*have_VF48_data(0);
      *pdata32++ = 4*have_VF48_data(1);
#endif
      *pdata32++ = end_ttc_time-start_ttc_time;          // 6 - TTC readout elapsed time
      *pdata32++ = nbytes_ttc;                           // 7 - TTC number of bytes
      *pdata32++ = end_sis3820_time-start_sis3820_time;  // 8 - SIS3820 readout elapsed time
      *pdata32++ = nbytes_sis3820;                       // 9 - SIS3820 number of bytes
#ifdef HAVE_SIS3820
      *pdata32++ = 4*haveData0;                            // 10 - SIS3820 FIFO
      *pdata32++ = 4*haveData1;                            // 11 - SIS3820 FIFO
#endif
      bk_close(pevent, pdata32);

      prev_start_time = start_time;
    }

  //vme_who = 'X';
  //vme_when_unlock = ss_millitime();

  ss_mutex_release(vme_mutex);

  pthread_yield();
  sched_yield();

  if (want_vme_lock) {
     //printf("want_vme_lock -> sleep(1)!\n");
     usleep(1);
  }

  if (0) {
     static int max_ltime = 0;
     int ltime = end_time - start_time;
     if (ltime > max_ltime) {
        printf("read_vme_event: lock time %d -> %d\n", max_ltime, ltime);
        if (ltime < 200000)
           max_ltime = ltime;
     }
  }

  if (size)
    {
      gCountEmpty = 0;
      gVmeIsIdle = 0;

      int pevent_size = bk_size(pevent);

      if (pevent_size <= 0 || pevent_size > max_event_size*7/8)
        {
          cm_msg(MERROR, frontend_name, "Possible VME event buffer overflow: event size %d, max_event_size %d", pevent_size, max_event_size);
        }

      //printf("read_vme_event %d bytes\n", pevent_size);

      static int max_size = 0;
      if (pevent_size > max_size) {
        printf("read_vme_event max size %d -> %d bytes\n", max_size, pevent_size);
        max_size = pevent_size;
      }

      gCountEvents ++;

      return pevent_size;
    }
  
  //printf("vf48: no data...\n");

  gCountEmpty++;

  if (gCountEmpty > 2) {
    gVmeIsIdle = 1;
    xusleep(100);
  }

  return 0;
}

INT read_ttc_event(char *pevent, INT off)
{
#ifdef HAVE_TTC
  if (gTTC)
    {
      lock_vme();

      if (gCalPulse)
        {
          //printf("TTC trigger!\n");
          gTTC->writeCommand(TTC_CMD_CalPulse);
          if (gCalPulseOnce)
             gCalPulse = false;
        }

      //printf("read_ttc_event!\n");
      
      int num_ttc = gTTC->GetNumTTC();

      const  int            kMaxTTC = 2;
      const  int            kNumScalers = 7;
      static uint32_t       gClock[kMaxTTC];
      static uint32_t       gCounts[kMaxTTC][kNumScalers];

      assert(num_ttc <= kMaxTTC);
      
      /* init bank structure */
      bk_init32(pevent);

      uint32_t *pdata32;
      bk_create(pevent, "TSCL", TID_DWORD, (void**)&pdata32);

      uint32_t* ptr = pdata32;

      struct timeval t;
      gettimeofday(&t,NULL);

      gTTC->writeCommand(TTC_CMD_LatchCounters);

      for (int ittc=0; ittc<num_ttc; ittc++) {
         gTTC->fTTC[ittc]->readCounters7(pdata32);
         pdata32 += 7;
      }

      bk_close(pevent, pdata32);

      ss_mutex_release(vme_mutex);

      float *pfloat;
      bk_create(pevent, "TSRT", TID_FLOAT, (void**)&pfloat);

      for (int ittc=0; ittc<num_ttc; ittc++) {
         uint32_t clock = ptr[6];

         //printf("pdata %d %d, clock %d\n", pdata32[0], pdata32[1], clock);

         uint32_t dtc = clock - gClock[ittc];
         double   dt =  50e6/dtc; // convert from clocks to seconds

         if (gClock[ittc]==0 || dtc==0 || dtc<100 || dtc>504000180)
            {
               dtc = 0;
               dt = 0;
            }

         //printf("dtc %d, dt %f\n", dtc, dt);
         
         for (int i=0; i<kNumScalers; i++)
            pfloat[i] = (ptr[i]-gCounts[ittc][i])*dt;

         gClock[ittc] = clock;

         for (int i=0; i<kNumScalers; i++)
            gCounts[ittc][i] = ptr[i];

         pfloat += kNumScalers;
         ptr    += kNumScalers;
      }

      bk_close(pevent, pfloat);

      return bk_size(pevent);
    }
#endif
  return 0;
}
 
static const int kMaxIo32scalers = 32; ///< Maximum number of io32 scalers

static uint32_t io32scalerSum[kMaxIo32scalers]; ///< Array of scaler sums

char io32scalerBankName[5] = "VSCX"; ///< IO32 bank name

void clear_io32_scaler_sum()
{
   for (int i=0; i<kMaxIo32scalers; i++)
      io32scalerSum[i] = 0;
}

#ifdef HAVE_IO32
INT read_io32scaler(char *pevent, INT off)
{
  /*!
   * Scalers are read out in three banks:
   * -  Head Bank Name (Tail Bank Name): \e Description
   * -# "SCHD" ("SCTD"): Total number of counts in the current read [TID_WORD].
   * -# "SCHR" ("SCTR"): Calculated scaler rate in the current read [TID_DOUBLE].
   * -# "SCHS" ("SCTS"): Calculated scaler sum in the current run [TID_DWORD].
   */

  //printf("read_io32_event!\n");

  int numsc  = 32;
  int iclock = 31;

  io32->write32(IO32_COMMAND, IO32_CMD_LATCH_SCALERS);

  /* init bank structure */
  bk_init32(pevent);

  uint32_t *psc;

  { // Counter bank
    uint32_t *pdata32;

    io32scalerBankName[3] = 'D';
    bk_create(pevent, io32scalerBankName, TID_DWORD, (void**)&pdata32);

    psc = pdata32;

    int count = io32->ReadScaler(pdata32, numsc);
    assert(count == numsc);

    pdata32 += numsc;
    
    bk_close(pevent, pdata32);
  }

  double dt = 50.0*psc[iclock]/1e9; // 50 ns clock
  double dt1 = 1.0/dt;

  { // Rate bank
    double* pdata;
    io32scalerBankName[3] = 'R';
    bk_create(pevent, io32scalerBankName, TID_DOUBLE, (void**)&pdata);
    for (int i=0; i<numsc; i++) {
      *pdata++ = dt1*psc[i];
    }
    bk_close(pevent, pdata);
  }

  { // Sum bank
    DWORD* pdata;
    io32scalerBankName[3] = 'S';
    bk_create(pevent, io32scalerBankName, TID_DWORD, (void**)&pdata);
    for (int i=0; i<numsc; i++) {
      io32scalerSum[i] += psc[i];
      *pdata++ = io32scalerSum[i];
    }
    bk_close(pevent, pdata);
  }

  return bk_size(pevent);
}

INT read_io32_event(char *pevent, INT off)
{
   lock_vme();

   int status = read_io32scaler(pevent, off);

   ss_mutex_release(vme_mutex);

   return status;
}
#endif // HAVE_IO32

/* emacs
 * Local Variables:
 * mode:c++
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

//end
