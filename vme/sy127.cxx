//
// Name: sy127.cxx
// Author: K.Olchanski
// Date: 26 Apr 2007
// Description: driver for the CAEN SY127 High Voltage mainframe
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "v288.h"
#include "sy127.h"

#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"

// For the record, our sy127 is at caenet address 0 and these are the most useful commands:
// ./v288.exe 0 0 <--- get sy127 "who are you?" data
// ./v288.exe 0 0x101 <--- read data from channel 1
// ./v288.exe 0 0xN01 <--- read data from channel N
// ./v288.exe 0 17 1000 <- set v1 for channel 0 to 1000
// ./v288.exe 0 0xN1P V <--- set parameter P for channel N to value V. Parameters seem to be enumerated in the same order as in the readout.
// ./sy127.exe 0 0x1C18 0 <--- turn channel 28 off
// ./sy127.exe 0 0x1C18 1 <--- turn channel 28 on

sy127::sy127(v288* interface, int address)
{
  fInterface = interface;
  fAddress   = address;
  fDebug     = 0;

  printf("SY127: on v288 at %p, caenet address %d\n",
	 fInterface,
	 fAddress);
};

int sy127::readId(char* ubuf, int ubufsize)
{
  const int bufsiz = 100;
  uint16_t buf[bufsiz];
  int nread = 0;
  int status = 0;

  ubuf[0] = 0;

  status = fInterface->sendCommand0(fAddress, 0, bufsiz, buf, nread);

  if (fDebug)
    printf("sy127::readId: status %d, nread %d\n", status, nread);

  if (nread > 1)
    {
      int i=0;
      for (; i<ubufsize && i<=nread; i++)
        ubuf[i] = buf[i+1]&0xFF;
      ubuf[i] = 0;
      return 0;
    }

  return -1;
}

int sy127::readChannel(int ichan, sy127channel* data)
{
  const int bufsiz = 100;
  uint16_t buf[bufsiz];
  int nread = 0;
  int status = 0;

  status = fInterface->sendCommand0(fAddress, (ichan<<8)|1, bufsiz, buf, nread);

  if (fDebug)
    printf("sy127::readChannel: chan %d, status %d, nread %d\n", ichan, status, nread);

  if (nread == 1)
    {
      memset(data, 0, sizeof(sy127channel));
      return 0;
    }
  else if (nread == 20)
    {
      int i=0;
      i++;
      data->v0 = buf[i++];
      data->i0 = buf[i++];
      data->v1 = buf[i++];
      data->i1 = buf[i++];
      data->rup  = buf[i++];
      data->rdwn = buf[i++];
      data->trip   = buf[i++];
      data->status = buf[i++];
      data->gr_ass = buf[i++];
      data->vread  = buf[i++];
      data->iread  = buf[i++];
      data->st_phase = buf[i++];
      data->st_time  = buf[i++];
      data->mod_type = buf[i++];
      strlcpy(data->chname, (char*)&buf[i++], sizeof(data->chname));
      return 0;
    }
  return -1;
}

int sy127::writeParameter(int ichan, int parameter, int value)
{
  assert(parameter>=0);
  assert(parameter<16);

  const int bufsiz = 100;
  uint16_t buf[bufsiz];
  int nread = 0;
  int status = 0;

  // ./v288.exe 0 0xN1P V <--- set parameter P for channel N to value V

  status = fInterface->sendCommand1(fAddress, (ichan<<8)|0x10|(parameter), value, bufsiz, buf, nread);

  if (fDebug)
    printf("sy127::writeParameter: Channel %d, parameter %d, value %d, status %d, nread %d\n", ichan, parameter, value, status, nread);

  if (status == 0)
    return 0;

  // error

  return -1;
}

void sy127::printChannel(const sy127channel* c)
{
  printf("v0: %f, i0: %f, rup: %f, rdwn: %f, trip: %d, status: 0x%x, gr_ass: 0x%x, vread: %f, iread: %f, mod_type: %d, chname: %s",
         c->v0,
         c->i0,
         c->rup,
         c->rdwn,
         c->trip,
         c->status,
         c->gr_ass,
         c->vread,
         c->iread,
         c->mod_type,
         c->chname);
}

#ifdef SY127_MAIN

#include "mvmestd.h"

int main(int argc,char*argv[])
{
  int status;
  MVME_INTERFACE *vme = 0;

  status = mvme_open(&vme,0);
  status = mvme_set_am(vme, MVME_AM_A24);

  v288* v = new v288(vme, 0x00F000);
  v->reset();

  const int bufsiz = 100;
  uint16_t buf[bufsiz];
  int nread = 0;
  status = 0;

  if (argc == 3)
    {
      int arg1 = strtoul(argv[1],NULL,0);
      int arg2 = strtoul(argv[2],NULL,0);
      printf("Command0: 0x%x, 0x%x\n", arg1, arg2);
      status = v->sendCommand0(arg1, arg2, bufsiz, buf, nread);
    }
  else if (argc == 4)
    {
      int arg1 = strtoul(argv[1],NULL,0);
      int arg2 = strtoul(argv[2],NULL,0);
      int arg3 = strtoul(argv[3],NULL,0);
      printf("Command0: 0x%x, 0x%x, 0x%x\n", arg1, arg2, arg3);
      status = v->sendCommand1(arg1, arg2, arg3, bufsiz, buf, nread);
    }

  printf("status %d, nread %d\n", status, nread);

  if (nread==20)
    {
      int i=0;
      i++;
      printf("v0: %d\n", buf[i++]);
      printf("v1: %d\n", buf[i++]);
      printf("i0: %d\n", buf[i++]);
      printf("i1: %d\n", buf[i++]);
      printf("rup: %d\n", buf[i++]);
      printf("rdwn: %d\n", buf[i++]);
      printf("trip: %d\n", buf[i++]);
      printf("status: %d\n", buf[i++]);
      printf("gr_ass: %d\n", buf[i++]);
      printf("vread: %d\n", buf[i++]);
      printf("iread: %d\n", buf[i++]);
      printf("st_phase: %d\n", buf[i++]);
      printf("st_time: %d\n", buf[i++]);
      printf("mod_type: %d\n", buf[i++]);
      printf("chname: %s\n", (char*)&buf[i++]);
    }

  mvme_close(vme);

  return 0;
}
#endif

// end
