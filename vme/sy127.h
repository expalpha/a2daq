//
// Name: sy127.h
// Date: 26 Apr 2007
//
#ifndef sy127_H
#define sy127_H

class v288;

//
// Interface for the CAEN SY127 High Voltage mainframe
//

#define SY127_PAR_V0     0
#define SY127_PAR_V1     1
#define SY127_PAR_I0     2
#define SY127_PAR_I1     3
#define SY127_PAR_VMAX   4
#define SY127_PAR_RUP    5
#define SY127_PAR_RDWN   6
#define SY127_PAR_TRIP   7
#define SY127_PAR_ONOFF  8
#define SY127_PAR_CHNAME 9

#define SY127_STATUS_OFF       0x01
#define SY127_STATUS_ON        0x04
#define SY127_STATUS_RAMP_UP   0x40
#define SY127_STATUS_RAMP_DOWN 0x80

struct sy127channel
{
  double v0;
  double i0;
  double v1;
  double i1;
  double rup;
  double rdwn;
  int trip;
  int status;
  int gr_ass;
  double vread;
  double iread;
  int st_phase;
  int st_time;
  int mod_type;
  char chname[256];
};

class sy127
{
public: // public methods
  sy127(v288* interface, int address); // ctor

  int readId(char* buf,int bufsize);
  int readChannel(int ichan, sy127channel* data);
  int writeParameter(int ichan, int parameter, int value);

  void printChannel(const sy127channel* c);

public: // private data
  v288* fInterface;
  int   fAddress;
  int   fDebug;
};

#endif
// end file
