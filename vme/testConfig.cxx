
#include <stdio.h>

#include "va1taConfig.h"


//------------------------------------------------
//------------------------------------------------


int main( )
{
  printf("***************************\n"); 	 
  printf("       vaConfig !!!       \n"); 	 
  printf("***************************\n\n");

  VaConfig* test = new VaConfig("va1taConfigFiles/va1taConfig_base.txt", 
                                "va1taConfigFiles/va1taConfig_trimDac.txt",
                                "va1taConfigFiles/va1taConfig_disable.txt" );
  
  
  printf("---------------------------------------\n");
  printf("   GetVA1TA\n");
  printf("---------------------------------------\n");
  
  
  for(int address = 1; address <= 4; address++)
  {
  	for(int port = 0; port < 3; port++)
	{ 
	   for(int va = 1; va < 5; va++)
	   {
		Va1taConfig va1ta;
		if(test->GetVA1TA(address,port,va,va1ta))
		{
			cout << "Address: " << address << " Port: " << port << " VA: " << va << " obi: " <<
			va1ta.obi<< " ibuf: " << va1ta.ibuf << endl;
			cout << "trim_dac: ";
			for(int j =0;j<128;j++)
	  		cout << va1ta.trim_dac[j];
			cout << endl << "disable:  ";
			for(int j =0;j<128;j++)
	  		cout << va1ta.disable[j];
			cout << endl;
		}
	    }
	}
  }
  
  printf("---------------------------------------\n");
  printf("   GetVA1TAarray\n");
  printf("---------------------------------------\n");
  
  
  for(int address = 1;address<=4;address++)
  {
     for(int port = 0;port<=3;port++)
     {
        HybridConfig va1ta;
	if(test->GetHybridConfig(address,port,va1ta))
	{
	  for(int i = 0;i<4;i++)
    	   {
     	     cout << "address: "<<address<<" port: " <<port <<" va: " << i+1 <<" obi: " << va1ta.cfg[i].obi<< " ibuf: " << va1ta.cfg[i].ibuf << endl;
	     cout << "trim_dac: ";
	     for(int j =0;j<128;j++)
	       cout << va1ta.cfg[i].trim_dac[j];
	     cout << endl << "disable:  ";
	     for(int j =0;j<128;j++)
	       cout << va1ta.cfg[i].disable[j];
	     cout << endl;
	   }
	}
     }
  }
  
 
  delete test;
  return 0;
}
