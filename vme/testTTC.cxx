// testTTC.cxx

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <sys/time.h>

#include "mvmestd.h"

#include "alphaTTC.h"
#include "alphaTTCX.h"
#include "va1taConfig.h"

extern "C" {
#include "v513.h"
#include "vf48.h"
}

#include "UnpackVF48.h"

int gIoregBase  = 0x510000;

//int gAlphaTtcBase = 0x11000000;
//int gAlphaTtcBase = 0x8F000000;
int gAlphaTtcBase = 0x41000000;

static char gFname[256];

// define a global variable to hold the value of a TA bank
// if setTriggerBank is used 
// set to 8 by default 
int gBank = 8 ;
int gBankToStartScan = 0 ;
int gBankToStopScan =  8 ;
 
#define VF48_NUM 1

//int gVF48base[VF48_NUM] = { 0xAD0000 };
//int gVF48base[VF48_NUM] = { 0xA00000 };
int gVF48base[VF48_NUM] = { 0xAB0000 };
int gVF48enable[VF48_NUM] = { 1 };
int gVF48samples = 1000;

double gettime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 0.000001*tv.tv_usec;
}

int vf48_init(MVME_INTERFACE* vme, int divisor, int delay)
{
  int grpEnabled = 0x3F;
  int samples = gVF48samples;

  if (delay < 1)
     delay = 1;
  if (delay > 240)
     delay = 240;

  mvme_set_am(vme, MVME_AM_A24);

  for (int i=0; i<VF48_NUM; i++)
    {
      //gVF48enable[i] = odbReadBool("/equipment/VF48history/Settings/VF48_enable", i);
      gVF48enable[i] = true;
      
      printf("VF48[%d] at VME A24 0x%x", i, gVF48base[i]);
      
      if (!gVF48enable[i])
        {
          printf(" - disabled\n");
          continue;
        }

      printf(" - enabled\n");

      int base = gVF48base[i];

      int status = vf48_Reset(vme, base);
      printf("vf48_Reset: status %d\n", status);
      if (status != SUCCESS) {
         printf("Cannot initialize VF48 0x%08x, status %d\n", base, status);
         exit(1);
      }

      //int latency = odbReadInt("/equipment/VF48history/Settings/VF48_Latency", i, 5);
      int latency = delay%120;
      printf("Set Latency to %d\n", latency);
      for (int j=0; j<6; j++)
        {
          vf48_ParameterWrite(vme, base, j, VF48_LATENCY, latency);
          //printf("Latency grp %i:%d\n", j, vf48_ParameterRead(vme, base, j, VF48_LATENCY));
        }

      //int pretrig = odbReadInt("/equipment/VF48history/Settings/VF48_PreTrigger", i, 5);
      int pretrig = delay-latency;
      printf("Set PreTrigger to %d\n", pretrig);
      for (int j=0; j<6; j++)
        {
          vf48_ParameterWrite(vme, base, j, VF48_PRE_TRIGGER, pretrig);
        }

      //set VF48 sub sampling 'divisor' parameter
      //int divisor = odbReadInt("/equipment/VF48history/Settings/VF48_Divisor", i, 0);
      printf( "Set divisor to %d\n", divisor);
      if (divisor >= 0)
        {
          vf48_DivisorWrite(vme, base, divisor);
          for (int j=0; j<6; j++)
            {
	      printf( "Divisor group %d: %d\n", j, vf48_DivisorRead(vme, base, j));
            }
        }
      
      //int segsize = odbReadInt("/equipment/VF48history/Settings/VF48_NumSamples", i, 200);
      int segsize = samples;
      printf("Set number of time bins to %d\n", segsize);
      for (int j=0; j<6; j++)
        {
          vf48_ParameterWrite(vme, base, j, VF48_SEGMENT_SIZE, segsize);
          //printf("SegSize grp %i:%d\n", j, vf48_ParameterRead(vme, base, j, VF48_SEGMENT_SIZE));
        }

      vf48_ExtTrgSet(vme, base);

      vf48_CsrWrite(vme, base, vf48_CsrRead(vme, base) | VF48_CSR_BUSY_OUT);

      //int grpmsk = odbReadInt("/equipment/VF48history/Settings/VF48_GroupEnable", i, 0x3F);
      int grpmsk = grpEnabled;
      vf48_GrpEnable(vme, base, grpmsk);
      printf("Group Enable: 0x%x\n", vf48_GrpRead(vme, base));

      //for (int i=0;i<6;i++)
      //vf48_SegSizeSet(vme, base, i, 16);

      //for (int i=0;i<6;i++)
      //printf("SegSize grp %i:%d\n", i, vf48_SegSizeRead(vme, base, i));

    }//loop over gVF48bases

  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      {
        printf("VF48[%d] at VME A24 0x%06x: Firmware: 0x%x, CSR: 0x%x\n",
               i,
               gVF48base[i],
               vf48_ParameterRead(vme, gVF48base[i], 0, VF48_FIRMWARE_ID),
               vf48_CsrRead(vme,gVF48base[i]));
        vf48_Status(vme,gVF48base[i]);
      }


  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      {
        vf48_AcqStart(vme, gVF48base[i]);
      }

  vf48_Status(vme,gVF48base[0]);

  VF48event::BeginRun(grpEnabled, samples, 0);
  return 0;
}

int firstEvent = 0;

int vf48divisor = 1;
int vf48delay = 255;
int vf48timeout = 2;
int vf48debug = 0;
int vf48dma = 1;

int vf48_read(MVME_INTERFACE*vme)
{
  bool verbose = (vf48debug>0);

  firstEvent = 1;

  for (int i=0; i<VF48_NUM; i++)
    if (gVF48enable[i])
      {
	int xtry = vf48timeout;
        for (; xtry>0; xtry--)
          {
            int haveData = vf48_NFrameRead(vme,gVF48base[i]);
            if (haveData > 0)
              break;
            printf("Waiting for VF48[%d] data... %d\n", i, haveData);
            sleep(1);
          }

	if (!xtry)
	  {
	    printf("Timeout waiting for VF48 data!\n");
	    //vf48_init(vme, vf48divisor);
	    return 0;
	  }

        if (1) {
           DWORD buf[100000];
           int nwords = sizeof(buf)/sizeof(buf[0]);
           int status = vf48_EventReadSync(vme, gVF48base[i], buf, &nwords);
           printf("VF48 read status %d, nwords %d\n", status, nwords);
           if ((status == SUCCESS) && (nwords>0)) {
              UnpackVF48(i, nwords, buf, (vf48debug>0));
           }
           continue;
        }

        int xhaveData = -1;
        while (xhaveData != 0)
          {
            int haveData = vf48_NFrameRead(vme,gVF48base[i]);
            bool isLast = (haveData == xhaveData);
            xhaveData = haveData;

            if (verbose)
              printf("vf48 %d: have data %d -> %d, isLast %d\n", i, xhaveData, haveData, isLast);

            if (!isLast && haveData < 200)
              continue;

            //if (!isLast)
            //  haveData = 4*(haveData/4);
            //else
            //  haveData = 4*((haveData+3)/4);

            if (!isLast)
              if (haveData&1)
                haveData -= 1;

            if (verbose)
              printf("read VF48 %d, have data %d\n", i, haveData);

            if (haveData > 0)
              {
                static uint32_t buf[10000];
                int count = haveData;
                if (count > 10000)
                  count = 10000;

                if (vf48dma)
                  {
                    vf48_DataRead(vme, gVF48base[i], buf, &count);
                  }
                else
                  {
		    mvme_set_dmode(vme, MVME_DMODE_D32);
		    for (int j=0; j<count; j++)
                      {
                        buf[j] = mvme_read_value(vme, gVF48base[i]+VF48_DATA_FIFO_R);
                        //printf("read %d -> 0x%x\n", j, buf[j]);
                      }
                  }
                UnpackVF48(i, count, buf, (vf48debug>0));
              }
          }
      }
  return 0;
}

#include "TApplication.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TGraph.h"

#if 0
#include "VF48Utils.h"
#endif

TApplication *app = NULL;

void makeApp()
{
  if (!app)
    app = new TApplication("testTTC", NULL, NULL);
}


TCanvas*gVF48Window;

TH1D* hhh[48];

int xcount = 0;

int hold = -200;
int holdCount = 0;
int holdInter = 0;

int hmin = 0;
int hmax = 0;

int tmin = 0;
int tmax = 0;

bool doPlotAll = true;
bool doPlot1   = false;
bool doPlot4   = false;
bool doPlot6   = false;
bool doPlot16  = false;
bool doPlotAdc = false;
bool doProfile = false;

bool doHardcopy = true;

int base = 0;

void PlotSample(const VF48event *e, int i, int j)
{
  TH1D* hh = hhh[j];

  int nsamples = e->modules[i]->channels[j].numSamples;
  
  if (nsamples < 1)
    return;
  
  if (!hh)
    {
      char name[256];
      sprintf(name, "adc%d", j);
      if (doProfile)
	{
	  TProfile *hp = new TProfile(name, name, nsamples, 0, nsamples, 0, 1000);
	  hp->BuildOptions(0,1000,"s");
	  hh = hhh[j] = hp;
	}
      else
        hh = hhh[j] = new TH1D(name, name, nsamples, 0, nsamples);
    }
  
  for (int s=0; s<nsamples; s++)
    {
      uint16_t v =  e->modules[i]->channels[j].samples[s];
      if (doProfile)
        hh->Fill((s+1), v);
      else
        hh->SetBinContent((s+1), v);
    }
  
  if (hmin || hmax)
    {
      hh->SetMinimum(hmin);
      hh->SetMaximum(hmax);
    }
  
  if (tmin || tmax)
    {
      hh->GetXaxis()->SetRangeUser(tmin, tmax);
    }
  
  gPad->Clear();
  hh->Draw();
  gPad->Modified();
}

void PlotVF48event(const VF48event*e)
{
  makeApp();

  printf("*********************** plot event!\n");
  //e->PrintSummary();

  //return;

  // reinit the plots

  static int nrows = 0;

  if (!gVF48Window)
    {
       gVF48Window = new TCanvas("testTTC", "testTTC", 10, 10, 1100, 900);
      gVF48Window->Clear();
      if (doPlotAll)
        gVF48Window->Divide(8,nrows=6);
      else if (doPlot1)
        gVF48Window->Divide(1,1);
      else if (doPlot4)
        gVF48Window->Divide(2,nrows=2);
      else if (doPlot6)
        gVF48Window->Divide(4,nrows=6);
      else if (doPlot16)
        gVF48Window->Divide(4,nrows=4);
      else
        gVF48Window->Divide(4,nrows=12);
    }

  //printf("xcount %d, nrows %d\n", xcount, nrows);

  if (xcount < 0)
    xcount = nrows-1;
  
  if (xcount >= nrows)
    {
      xcount = 0;
      //gVF48Window->SaveAs("testTTC.root");
      //gVF48Window->SaveAs("testTTC.ps");
    }

  int i=0;
  int bcount = 12;

  int interesting = 0;

  holdCount++;

  if (!doPlotAll && !doPlot1 && !doPlot4 && !doPlot6 && !doPlot16)
    {
#if 0
      for (int j=base; j<base+bcount; j++)
        {
          interesting += CalculatePulseHeight(e,i,j);
        }
#endif

      printf("hold %d, events %d, interesting %d\n", hold, holdCount, holdInter);
      
      //holdCount++;
      
      printf("Interesting %d\n", interesting);

      if (interesting==0)
        return;

      holdInter++;
    }

  if (doPlotAll)
    {
      for (int j=0; j<48; j++)
        {
#if 0
          if (doPlotAdc)
            interesting += CalculatePulseHeight(e,i,j);
#endif
          gVF48Window->cd(1+j);
          PlotSample(e, i, j);
        }
    }
  else if (doPlot1)
    {
      gVF48Window->cd(1);
      PlotSample(e, i, base);
    }
  else if (doPlot4)
    {
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+j-base);
          PlotSample(e, i, j);
        }
    }
  else if (doPlot6)
    {
      int c = 0;
      xcount = 0;

      base = 0*4+0*16;
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+c);
          PlotSample(e, i, j);
          c++;
        }

      base = 1*4+0*16;
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+c);
          PlotSample(e, i, j);
          c++;
        }

      base = 2*4+0*16;
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+c);
          PlotSample(e, i, j);
          c++;
        }

      base = 1*4+2*16;
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+c);
          PlotSample(e, i, j);
          c++;
        }

      base = 2*4+2*16;
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+c);
          PlotSample(e, i, j);
          c++;
        }

      base = 3*4+2*16;
      for (int j=base; j<base+4; j++)
        {
          gVF48Window->cd(1+xcount*4+c);
          PlotSample(e, i, j);
          c++;
        }
    }
  else if (doPlot16)
    {
      for (int j=base; j<base+16; j++)
        {
          gVF48Window->cd(1+xcount*16+j-base);
          PlotSample(e, i, j);
        }
    }
  else
    {
      for (int j=base; j<base+bcount; j++)
        {
          gVF48Window->cd(1+xcount*bcount+j-base);
          PlotSample(e, i, j);
        }

      xcount++;
      if (xcount >= 4)
        {
          xcount = 0;
          //gVF48Window->SaveAs("testTTC.root");
          //gVF48Window->SaveAs("testTTC.ps");
        }
    }

  gVF48Window->Modified();
  gVF48Window->Update();
  if (doHardcopy && !doProfile)
    {
      char fname[256];
      sprintf(fname, "%s-0x%08X.root", gFname, gVF48base[i]);
      gVF48Window->SaveAs(fname);
      sprintf(fname, "%s-0x%08X.pdf", gFname, gVF48base[i]);
      gVF48Window->SaveAs(fname);
    }
  //gVF48Window->SaveAs("testTTC.ps");
  //sleep(1);
}

const VF48event* eptr = NULL;

void HandleVF48event(const VF48event*e)
{
  //printf("*********************** see event!\n");
  //e->PrintSummary();

  if (firstEvent)
    {
       PlotVF48event(e); //jws was //
       firstEvent = false;
    }

  if (eptr)
    delete eptr;
  eptr = e;
}

void v513_init(MVME_INTERFACE *vme)
{
  v513_Reset(vme, gIoregBase);
  v513_Status(vme, gIoregBase);

  printf("Setting V513 channels to OUTPUT mode\n");

  for (int i=0; i<16; i++)
    {
      v513_SetChannelMode(vme, gIoregBase, i, V513_CHANMODE_OUTPUT|V513_CHANMODE_POS|V513_CHANMODE_TRANSP);
    }
}

void v513_pulse(MVME_INTERFACE *vme)
{
  v513_Write(vme,gIoregBase,0x07);
  v513_Write(vme,gIoregBase,0x00);
}

void v513_vetoEvents(MVME_INTERFACE *vme)
{
  //v513_Write(vme,gIoregBase,0x04);
}

void v513_allowEvents(MVME_INTERFACE *vme)
{
  //v513_Write(vme,gIoregBase,0x00);
}

int xudelay(AlphaTTC*ttc, int usec)
{
  int status = 0;
  for (int i=0; i<usec; i++)
    status |= ttc->read32(0);
  return status;
}

int printHybridConfig(const Va1taConfig* cfg)
{
//   int obi; // 3 bits
//   int ibuf; // 3 bits
//   int pre_bias; // 3 bits
//   int sbi; // 3 bits
//   int vrc; // 3 bits
//   int ifp; // 3 bits
//   int ifsf; // 3 bits
//   int ifss; // 3 bits
//   int sha_bias; // 3 bits
//   int vthr; // 5 bits
//   int twbi; // 3 bits
//   int trim_dac[128]; // 4 bits each
//   int disable[128];  // 1 bits each
//   int test_on; // 1 bit
//   int r2;
//   int r1;
//   int nside;
//   int tp300;

  printf("obi=%d ibuf=%d pre_bias=%d sbi=%d sha_bias=%d vthr=%2d, test_on=%d, disable bits 0 1 2 125 126 127 = %d %d %d %d %d %d",
	 cfg->obi,
	 cfg->ibuf,
	 cfg->pre_bias,
	 cfg->sbi,
	 cfg->sha_bias,
	 cfg->vthr,
	 cfg->test_on,
         cfg->disable[0],
         cfg->disable[1],
         cfg->disable[2],
         cfg->disable[125],
         cfg->disable[126],
         cfg->disable[127]
         );

  return 0;
}

int frcFirst = -1;
int frcLast  = -1;

int load_all(AlphaTTCX* ttcx, int test_ena, int vthrn, int vthrp, int ta_enable = 0, int strip_enable = -2)
{
   int numTTC = ttcx->GetNumTTC();
   
   if (frcFirst < 0)
      frcFirst = 0;
   if (frcLast < 0)
      frcLast = 16*numTTC - 1;
   
   int hadErrors = 0;

   VaConfig* configMap = new VaConfig("../va1taConfigFiles/va1taConfig_base.txt", 
                                      "../va1taConfigFiles/va1taConfig_trimDac.txt",
                                      "../va1taConfigFiles/va1taConfig_strips.txt");
   
   ttcx->enableTrigIn(false);
   ttcx->enableTrigOut(false);

   std::string okstr;
   std::string errstr;

   for (int ifrc=frcFirst; ifrc<=frcLast; ifrc++) {
      bool allok = true;
      bool allbad = true;
      std::string xokstr;
      std::string xerrstr;
      
      for(int ifrcport=0; ifrcport<4; ifrcport++) {
         HybridConfig va1taCfg;
         memset(&va1taCfg, 0, sizeof(va1taCfg));
         
         //printf("frc %d, port %d\n", ifrc, ifrcport);

         int xifrc = ifrc%16;

         configMap->GetHybridConfig(xifrc,ifrcport,va1taCfg);
         
         for (int i=0; i<4; i++)
            {
               if (test_ena)
                  va1taCfg.cfg[i].test_on = 1;
               else
                  va1taCfg.cfg[i].test_on = 0;
            }
               
         if (vthrn >= 0)
            {
               va1taCfg.cfg[0].vthr = vthrn;
               va1taCfg.cfg[1].vthr = vthrn;
               //printf("N-side threshold %d\n", vthrn);
            }
               
         if (vthrp >= 0)
            {
               va1taCfg.cfg[2].vthr = vthrp;
               va1taCfg.cfg[3].vthr = vthrp;
               //printf("P-side threshold %d\n", vthrp);
            }
         
         for (int i=0; i<4; i++)
            {
               va1taCfg.cfg[i].disable[0] = 1;     // disable=1.enabled=0.
               va1taCfg.cfg[i].disable[1] = 1;     // disable=1.enabled=0.
               va1taCfg.cfg[i].disable[126] = 1;   // disable=1.enabled=0.
               va1taCfg.cfg[i].disable[127] = 1;   // disable=1.enabled=0.
            }
               
         if (ta_enable) {
            for (int i=0; i<4; i++)
               {
                  for (int j=0; j<128; j++)
                     va1taCfg.cfg[i].disable[j] = 0;     // disable=1.enabled=0.
               }
         }

         if (strip_enable == -1) { // enable all trips
            for (int i=0; i<4; i++)
               for (int j=0; j<128; j++)
                  va1taCfg.cfg[i].disable[j] = 0;     // disable=1.enabled=0.
         } else if (strip_enable == 128) { // disable all strips
            for (int i=0; i<4; i++)
               for (int j=0; j<128; j++)
                  va1taCfg.cfg[i].disable[j] = 1;     // disable=1.enabled=0.
         } else if (strip_enable >= 0 && strip_enable < 128) { // enable just this one strip
            for (int i=0; i<4; i++) {
               for (int j=0; j<128; j++)
                  va1taCfg.cfg[i].disable[j] = 1;     // disable=1.enabled=0.
               va1taCfg.cfg[i].disable[strip_enable] = 0;     // disable=1.enabled=0.
            }
         }
         
         if (1)
            {
               printf("Loading FRC %d, port %d:\n", ifrc, ifrcport);
               printHybridConfig(&va1taCfg.cfg[0]); printf("\n");
               printHybridConfig(&va1taCfg.cfg[1]); printf("\n");
               printHybridConfig(&va1taCfg.cfg[2]); printf("\n");
               printHybridConfig(&va1taCfg.cfg[3]); printf("\n");
            }
               
         char s[265];
         sprintf(s, "%d-%d ", ifrc, ifrcport);
         
         int status = ttcx->writeHybridConfig(ifrc, ifrcport, &va1taCfg);

         if (status != 0) {
            hadErrors ++;
            xerrstr += s;
            allok = false;
         } else {
            xokstr += s;
            allbad = false;
         }

      } //loop over ifrcports
         
      if (allok) {
         char s[256];
         sprintf(s, "%d ", ifrc);
         okstr += s;
      } else if (allbad) {
         char s[256];
         sprintf(s, "%d ", ifrc);
         errstr += s;
      } else {
         okstr += xokstr;
         errstr += xerrstr;
      }
      
   } // loop over ifrc addresses
      
   delete configMap;

   printf("----------------------------------------\n");
   printf("load_all status:\n");
   printf("ok: %s\n", okstr.c_str());
   printf("errors: %s\n", errstr.c_str());
   printf("----------------------------------------\n");
   
   return hadErrors;
}

void ttc_clearCounters(AlphaTTCX* ttcx)
{
  ttcx->writeCommand(TTC_CMD_ClearCounters);
}

void ttc_printCounters(AlphaTTCX* ttcx)
{
   for (int i=0; i<ttcx->GetNumTTC(); i++) {
      AlphaTTC* ttc = ttcx->fTTC[i];
      
      uint32_t data[7];
      ttc->readCounters7(data);

      printf("TTC counters: M1(%3d), M2(%3d), M3(%3d), M4(%3d), M5(%3d), M6(%3d): %5d %5d %5d %5d %5d %5d, clock %9d\n",
             ttc->getTrig(0, 0),
             ttc->getTrig(0, 1),
             ttc->getTrig(0, 2),
             ttc->getTrig(1, 0),
             ttc->getTrig(1, 1),
             ttc->getTrig(1, 2),
             data[0],
             data[1],
             data[2],
             data[3],
             data[4],
             data[5],
             data[6]);
   }
}

void ttc_printTriggerCounters(AlphaTTCX* ttcx)
{
   int num_fpga = 2*ttcx->GetNumTTC();

   for (int ifpga=0; ifpga<num_fpga; ifpga++) {
      int ibank = ttcx->getTaCountersBank(ifpga);
      printf("Trigger counters: FPGA %d, bank %d: ", ifpga, ibank);
      uint16_t c[16];
      ttcx->readTaCounters16(ifpga, c);
      for (int i=0; i<16; i++) {
         printf(" %6u", c[i]);
         if (i%4 == 3)
            printf(" |");
      }
      printf("\n");
   }
}

void ttc_printTA(AlphaTTCX* ttcx)
{
   int num_ttc = ttcx->GetNumTTC();
   for (int ifpga=0; ifpga<2*num_ttc; ifpga++)
      {
         printf("TA signals FPGA %d: 0x", ifpga);
         for (int i=7; i>=0; i--)
            {
               uint32_t value = ttcx->readTriggerFPGA(ifpga, i+8);
               printf(" %04x", value&0xFFFF);
            }
         
         uint32_t reg2 = ttcx->readTriggerFPGA(ifpga,   2) & 0xFFFF;
         uint32_t reg3 = ttcx->readTriggerFPGA(ifpga,   3) & 0xFFFF;
         uint32_t reg37 = ttcx->readTriggerFPGA(ifpga, 37) & 0xFFFF;
         uint32_t reg38 = ttcx->readTriggerFPGA(ifpga, 38) & 0xFFFF;
         
         printf(" multiplicity: 0x%04x, timestamp 0x%04x%04x, count %d", reg2, reg38, reg37, reg3);
         
         printf("\n");
      }
}

int ttc_printTaMap(AlphaTTCX* ttcx)
{
   int num_ttc = ttcx->GetNumTTC();

  for (int ifpga=0; ifpga<2*num_ttc; ifpga++)
    {
      uint32_t reg4 = ttcx->readTriggerFPGA(ifpga, 4) & 0xFFFF;
      uint32_t reg5 = ttcx->readTriggerFPGA(ifpga, 5) & 0xFFFF;
      uint32_t reg6 = ttcx->readTriggerFPGA(ifpga, 6) & 0xFFFF;
      uint32_t reg7 = ttcx->readTriggerFPGA(ifpga, 7) & 0xFFFF;

      printf("Mapping of TA signals to multiplicity layers, FPGA %d, map 0x%04x 0x%04x 0x%04x 0x%04x\n", ifpga, reg4, reg5, reg6, reg7);

      int hybridLayer[32];
      for (int i=0; i<32; i++)
	hybridLayer[i] = 0;

      for (int i=0; i<32; i++)
	{
	  int ibit = i*2;
	  int ioff = ibit%16;
	  int ireg = ibit/16;
	  int v = 0;
	  switch (ireg)
	    {
	    case 0: v = reg4; break;
	    case 1: v = reg5; break;
	    case 2: v = reg6; break;
	    case 3: v = reg7; break;
	    }
	  hybridLayer[i] = (v>>ioff)&3;
	}

      for (int ll=0; ll<=3; ll++)
	{
	  printf(" Layer %d: ", ll);
	  for (int i=0; i<32; i++)
	    if (hybridLayer[i]==ll)
	      printf(" %d", i);
	  printf("\n");
	}
    }
  return 0;
}

void ttc_printLatchFifo(AlphaTTCX*ttcx)
{
   int num_fpga = 2*ttcx->GetNumTTC();

   for (int ifpga=0; ifpga<num_fpga; ifpga++) {
      uint16_t buf16[256000];
      
      int rd = ttcx->readLatchFifo(ifpga, (char*)buf16, sizeof(buf16));
      printf("fpga %d, fifo read %d: ", ifpga, rd);
      
      int rd16 = rd/2;
      
      for (int i=0; i<rd16; i++) {
         if (i%16 == 0)
            printf("\n");
         
         printf(" %04x", buf16[i]);
      }

      printf("\n");
   }
}

int xsleep(int sec)
{
  while (sec--)
    {
      printf("%d...", sec);
      sleep(1);
    }
  printf("\n");
  return 0;
}

int main(int argc, char*argv[])
{
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
  
  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
 
  MVME_INTERFACE *vme = 0;

  int status = mvme_open(&vme,0);
  assert(status == SUCCESS);

  AlphaTTCX* ttcx = new AlphaTTCX();

  AlphaTTC* ttc = new AlphaTTC(vme, gAlphaTtcBase);
  ttc->fDebug = true;

  ttcx->AddTTC(ttc);

  bool doLoad = true;
  bool doLoadErrors = false;
  bool doLoadAll = false;
  bool doLoadLoop = false;
  bool doRead = false;
  bool doNormal = false;
  bool doTest   = false;
  bool doVF48 = false;
  bool doV513 = false;
  bool doHold = true;
  bool doHoldScan = false;
  bool doTrigger = true;
  bool doExtTrigger = false;
  bool doConfigScan = false;
  bool doLoop    = false;
  int  doCycle = 5;
  bool doTrigIn = false;
  bool doVthrScan = false;
  bool doStripScan = false;
  bool doTaRates = false;
  bool doCalpulse = false;
  bool doMux = false;
  bool doMuxPulse = false;
  bool isTtcxMode = false;
  bool doAppRun = true;

  sprintf(gFname, "%s", "testTTC");
  
  int ifrc = 0;
  int ifrcport = 0;
  int testChan = 10;
  int vthr = 31;
  int vthrp = -1;
  int vthrn = -1;

  if (1)
    {
      for (int i=1; i<argc; i++)
        if (0)
          {
            // nothing
          }
        else if (strcmp(argv[i],"--ttc1")==0)
          {
             ttcx->ClearTTC();
             ttc = new AlphaTTC(vme, 0x41000000);
             ttcx->AddTTC(ttc);
          }
        else if (strcmp(argv[i],"--ttc2")==0)
          {
             ttcx->ClearTTC();
             ttc = new AlphaTTC(vme, 0x42000000);
             ttcx->AddTTC(ttc);
          }
        else if (strcmp(argv[i],"--ttcx")==0)
          {
             isTtcxMode = true;
             ttc = NULL;
             ttcx->ClearTTC();
             if (AlphaTTC::isPresent(vme, 0x41000000))
                ttcx->AddTTC(new AlphaTTC(vme, 0x41000000));
             if (AlphaTTC::isPresent(vme, 0x42000000))
                ttcx->AddTTC(new AlphaTTC(vme, 0x42000000));
          }
        else if (strcmp(argv[i],"--reg88")==0)
          {
            assert(!isTtcxMode);
            uint32_t v = strtoul(argv[i+1], NULL, 0);
	    printf("Write register 0x88 value 0x%08x\n", v);
            ttc->write32(0x88, v);
            i++;
          }
	else if (strcmp(argv[i],"--reg8C")==0)
          {
            assert(!isTtcxMode);
            uint32_t v = ttc->read32(0x8C);
	    printf("Read  register 0x8C value 0x%08x\n", v);
          }
	else if (strcmp(argv[i],"--regB0")==0)
          {
            assert(!isTtcxMode);
            uint32_t v = strtoul(argv[i+1], NULL, 0);
	    printf("Write register 0xB0 value 0x%08x\n", v);
            ttc->write32(0xB0, v);
            i++;
          }
	else if (strcmp(argv[i],"--write32")==0)
          {
            assert(!isTtcxMode);
            int ireg  = strtoul(argv[i+1], NULL, 0);
            int value = strtoul(argv[i+2], NULL, 0);
            ttc->write32(ireg, value);
            printf("register 0x%x wrote: 0x%08x\n", ireg, value);
            i+=2;
          }
	else if (strcmp(argv[i],"--read32")==0)
          {
            assert(!isTtcxMode);
            int ireg  = strtoul(argv[i+1], NULL, 0);
            uint32_t value = ttc->read32(ireg);
            printf("register 0x%x read:  0x%08x\n", ireg, value);
            i+=1;
          }
#if 0
	else if (strcmp(argv[i],"--write8")==0)
          {
            assert(!isTtcxMode);
            int ireg  = strtoul(argv[i+1], NULL, 0);
            int value = strtoul(argv[i+2], NULL, 0);
            ttc->write8(ireg, value);
            printf("register 0x%x wrote: 0x%08x\n", ireg, value);
            i+=2;
          }
	else if (strcmp(argv[i],"--read8")==0)
          {
            assert(!isTtcxMode);
            int ireg  = strtoul(argv[i+1], NULL, 0);
            uint32_t value = ttc->read8(ireg);
            printf("register 0x%x read:  0x%08x\n", ireg, value);
            i+=1;
          }
#endif
	else if (strcmp(argv[i],"--writeTriggerFPGA")==0)
          {
            int ifpga = strtoul(argv[i+1], NULL, 0);
            int ireg  = strtoul(argv[i+2], NULL, 0);
            int value = strtoul(argv[i+3], NULL, 0);
            ttcx->writeTriggerFPGA(ifpga, ireg, value);
            i+=3;
          }
	else if (strcmp(argv[i],"--readTriggerFPGA")==0)
          {
            int ifpga = strtoul(argv[i+1], NULL, 0);
            int ireg  = strtoul(argv[i+2], NULL, 0);
            uint32_t value = ttcx->readTriggerFPGA(ifpga, ireg);
	    printf("Trigger FPGA %d, reg 0x%01x: 0x%08x\n", ifpga, ireg, value);
            i+=2;
          }
	else if (strcmp(argv[i],"--testFifo")==0)
          {
            assert(!isTtcxMode);

	    ttc->vmeClearTriggerFPGA();

	    while (1)
	      {
		uint32_t reg03 = 0xFFFF & ttc->readTriggerFPGA(0,  3);
		uint32_t reg32 = 0xFFFF & ttc->readTriggerFPGA(0, 32);
		uint32_t reg33 = 0xFFFF;
		if (! (reg32&0x4000))
		  reg33 = 0xFFFF & ttc->readTriggerFPGA(0, 33);
		printf("read reg03=latch counter: 0x%04x, reg32=fifo status: 0x%04x, reg33=fifo data: 0x%04x\n", reg03, reg32, reg33);
		xsleep(5);

		if (reg32 & 0x4000)
		  {
		    printf("Latch!\n");
		    ttc->vmeLatch();
		    xsleep(5);
		  }
	      }
          }
	else if (strcmp(argv[i],"--readFifo")==0)
          {
            assert(!isTtcxMode);

	    while (0)
	      {
		ttc->vmeClearTriggerFPGA();

		for (int count=0; count<10; count++)
		  {
		    ttc->vmeLatch();
					    
		    uint32_t reg03 = 0xFFFF & ttc->readTriggerFPGA(0,  3);
		    uint32_t reg32 = 0xFFFF & ttc->readTriggerFPGA(0, 32);
		    printf("event %3d, latch counter: %3d, 0x%04x, fifo status: 0x%04x\n", count, reg03, reg03, reg32);

		    sleep(1);
		  }
	      }

	    while (1)
	      {
		ttc->vmeClearTriggerFPGA();

		for (int event=1; ; event++)
		  {
		    ttc->vmeLatch();

		    //uint32_t reg03 = 0xFFFF & ttc->readTriggerFPGA(0,  3);
		    //uint32_t reg32 = 0xFFFF & ttc->readTriggerFPGA(0, 32);
		    //uint32_t reg33 = 0xFFFF & ttc->readTriggerFPGA(0, 33);
		    //printf("read reg03=latch counter: 0x%04x, reg32=fifo status: 0x%04x\n", reg03, reg32);

		    uint16_t buf[256];
		    int rd = ttc->readLatchFifo(0, (char*)buf, sizeof(buf));

		    bool bad = false;

		    bad |= (buf[0] != 0x5555);
		    bad |= (buf[1] != 0x9999);
		    bad |= (buf[4] != 0xF000);
		    bad |= (buf[13] != (0xFFFF & event));
		    bad |= (buf[14] != 0xAAAA);

		    if (bad || (event%1000 == 0))
		      {
			printf("event 0x%x %d, read %d: 0x", event, event, rd);
			for (int i=0; i<rd/2; i++)
			  printf(" %04x", buf[i]);
			printf("%s\n", bad?" ***BAD***":"");
		      }

		    //sleep(1);
		  }
	      }

	    while (1)
	      {
		ttc->vmeClearTriggerFPGA();

		for (int event=1; ; event++)
		  {
		    for (int i=0; i<400; i++)
		      ttc->vmeLatch();

		    //uint32_t reg03 = 0xFFFF & ttc->readTriggerFPGA(0,  3);
		    //uint32_t reg32 = 0xFFFF & ttc->readTriggerFPGA(0, 32);
		    //uint32_t reg33 = 0xFFFF & ttc->readTriggerFPGA(0, 33);
		    //printf("read reg03=latch counter: 0x%04x, reg32=fifo status: 0x%04x\n", reg03, reg32);

		    double t0 = gettime();
		    
		    uint16_t buf[25600];
		    int rd = ttc->readLatchFifo(0, (char*)buf, sizeof(buf));

		    double t1 = gettime();

		    bool bad = false;

		    bad |= (buf[0] != 0x5555);
		    bad |= (buf[1] != 0x9999);
		    bad |= (buf[4] != 0xF000);
		    bad |= (buf[14] != 0xAAAA);

		    if (bad || 1 || (event%1000 == 0))
		      {
			printf("read %d: 0x", rd);
			for (int i=0; i<16; i++)
			  printf(" %04x", buf[i]);
			printf(", time %f sec, %s\n", t1-t0, bad?" ***BAD***":"");
		      }
		  }
	      }
          }
	else if (strcmp(argv[i],"--disableVeto")==0)
          {
            assert(!isTtcxMode);
	    ttc->setBits32(0xC0, TTC_C0_DIS_VETO);
          }
	else if (strcmp(argv[i],"--enableTrigIn")==0)
          {
	    doTrigIn = true;
          }
	else if (strcmp(argv[i],"--enableTA")==0)
          {
             printf("Enabling all TA signals\n");
             ttcx->enableTA();
             ttcx->writeTaEnableBitmap();
          }
	else if (strcmp(argv[i],"--enableTA3")==0)
          {
            int ifpga   = strtoul(argv[i+1], NULL, 0);
            int ibank   = strtoul(argv[i+2], NULL, 0);
            int ita     = strtoul(argv[i+3], NULL, 0);
            i+=3;
	    ttcx->enableTA(ifpga, ibank, ita);
	    ttcx->writeTaEnableBitmap();
          }
	else if (strcmp(argv[i],"--disableTA")==0)
          {
             printf("Disabling all TA signals\n");
             ttcx->disableTA();
             ttcx->writeTaEnableBitmap();
          }
	else if (strcmp(argv[i],"--readTA")==0)
          {
	    ttc_printTA(ttcx);
          }
	else if (strcmp(argv[i],"--readTaMap")==0)
          {
	    ttc_printTaMap(ttcx);
          }
	else if (strcmp(argv[i],"--clearTaMap")==0)
          {
	    ttcx->clearTaMap();
          }
	else if (strcmp(argv[i],"--setTaMap")==0)
          {
            int ifpga   = strtoul(argv[i+1], NULL, 0);
            int ihybrid = strtoul(argv[i+2], NULL, 0);
            int ilayer  = strtoul(argv[i+3], NULL, 0);
            i+=3;
	    ttcx->setTaMap(ifpga, ihybrid, ilayer);
          }
        else if (strcmp(argv[i],"--dutyTA")==0)
          {
             int num_fpga = 2*ttcx->GetNumTTC();

             int count0[128*num_fpga];
             int count1[128*num_fpga];
            
             memset(count0, 0, sizeof(count0));
             memset(count1, 0, sizeof(count1));

             time_t t0 = time(NULL);
            
             for (int iloop=0; true; iloop++)
                {
                   //ttc->writeTriggerFPGA(0, 1, 0x2);
                   //ttc->writeTriggerFPGA(1, 1, 0x2);
                   
                   //ttc->writeTriggerFPGA(0, 1, 0);
                   //ttc->writeTriggerFPGA(1, 1, 0);
                   
                   ttcx->vmeLatch();
                
                   for (int ifpga=0; ifpga<num_fpga; ifpga++)
                      {
                         for (int ibank=7; ibank>=0; ibank--)
                            {
                               uint32_t value = ttcx->readTriggerFPGA(ifpga, ibank+8);
                               
                               for (int ibit=0; ibit<16; ibit++)
                                  {
                                     int ita = ifpga*128 + ibank*16 + ibit;
                                     int v = value & (1<<ibit);
                                     if (v)
                                        count1[ita]++;
                                     else
                                        count0[ita]++;
                                  }
                            }
                      }

                   time_t t = time(NULL);
                   if (t - t0 > 10)
                      break;
                }
             
             const char* xtype[] = { "N", "P", "P", "N" };
             
             for (int i=0; i<128*num_fpga; i++)
                {
                   double duty = 100.0*count1[i]/(1.0*count0[i]+1.0*count1[i]);
                   if (count0[i] == 0)
                      printf("TA %3d: FPGA %d, %3d, IFRC %d, IMOD %2d, ITA %d, type %s: always 1\n", i, i/128, i%128, (i%128)/16, (i%128)/4, i%4, xtype[i%4]);
                   else if (count1[i] > 0) {
                      if (duty < 0.2)
                         printf("TA %3d: FPGA %d, %3d, IFRC %d, IMOD %2d, ITA %d, type %s: %d counts out of %d reads\n", i, i/128, i%128, (i%128)/16, (i%128)/4, i%4, xtype[i%4], count1[i], count1[i]+count0[i]);
                      else
                         printf("TA %3d: FPGA %d, %3d, IFRC %d, IMOD %2d, ITA %d, type %s: duty cycle: %5.1f%%\n", i, i/128, i%128, (i%128)/16, (i%128)/4, i%4, xtype[i%4], duty);
                   }
                }
          }
	else if (strcmp(argv[i],"--setMult")==0)
          {
            int ifpga   = strtoul(argv[i+1], NULL, 0);
            int ilayer  = strtoul(argv[i+2], NULL, 0);
            int imin    = strtoul(argv[i+3], NULL, 0);
            int imax    = strtoul(argv[i+4], NULL, 0);
            i+=4;

	    assert(ilayer>=0 && ilayer<6);

	    printf("Set multiplicity for FPGA %d, layer %d, min %d, max %d\n", ifpga, ilayer, imin, imax);

	    ttcx->setMult(ifpga, ilayer, imin, imax);
          }
	else if (strcmp(argv[i],"--enableTrigOut")==0)
	  {
	    ttcx->enableTrigOut(true);
	  }
	else if (strcmp(argv[i],"--setTrig3")==0)
          {
            int ifpga   = strtoul(argv[i+1], NULL, 0);
            int itrig   = strtoul(argv[i+2], NULL, 0);
            int isource = strtoul(argv[i+3], NULL, 0);
            i+=3;

	    assert(itrig>=0 && itrig<3);

	    printf("Set FPGA %d, TRIG %d to source %d\n", ifpga, itrig, isource);

	    ttcx->setTrig(ifpga, itrig, isource);
          }
	else if (strcmp(argv[i],"--selectCounters3")==0)
          {
            int ifpga   = strtoul(argv[i+1], NULL, 0);
            int trig0 = strtoul(argv[i+2], NULL, 0);
            int trig1 = strtoul(argv[i+3], NULL, 0);
            int trig2 = strtoul(argv[i+4], NULL, 0);
            i+=4;

	    ttcx->setTrig(ifpga, 0, trig0);
	    ttcx->setTrig(ifpga, 1, trig1);
	    ttcx->setTrig(ifpga, 2, trig2);
          }
	else if (strcmp(argv[i],"--clearCounters")==0)
          {
	    ttc_clearCounters(ttcx);
	  }
	else if (strcmp(argv[i],"--readCounters")==0)
          {
	    while (1)
	      {
                 ttc_printCounters(ttcx);
                 sleep(1);
	      }
          }
	else if (strcmp(argv[i],"--clearTriggerFPGA")==0)
	  {
	    ttcx->vmeClearTriggerFPGA();
	  }
        else if (strcmp(argv[i],"--setTriggerCountersBank")==0)
          {
            int ibank = strtoul(argv[i+1], NULL, 0);
            i++;

            // try and make a flag to show that the trigger
            // bank has been set
            gBank = ibank  ;
            
            ttcx->setTaCountersBank(-1, ibank);
	  }
	else if (strcmp(argv[i],"--readTriggerCounters")==0)
          {
	    while (1)
	      {
		ttc_printTriggerCounters(ttcx);
		sleep(1);
	      }
	  }
	else if (strcmp(argv[i],"--xreadTriggerCounters")==0)
          {
	    for (int i=0; ; i++)
	      {
		ttc_printTriggerCounters(ttcx);
		//usleep(100);
		sleep(1);

		if (i%5==4)
		  {
		    printf("Clear!\n");
		    ttcx->vmeClearTriggerFPGA();
		  }
	      }
	  }
	else if (strcmp(argv[i],"--regout")==0)
          {
            assert(!isTtcxMode);
	    while (1)
	      {
		uint32_t regC4 = ttc->read32(0xC4);
		printf("TTC regout 0x%08x, %d\n", regC4, regC4&0x4);
		sleep(1);
	      }
          }
	else if (strcmp(argv[i],"--rdelay")==0)
          {
            hold = strtoul(argv[i+1], NULL, 0);
            ttcx->setHoldDelay(hold);
            i++;
          }
	else if (strcmp(argv[i],"--holdDelay")==0)
          {
            hold = strtoul(argv[i+1], NULL, 0);
            ttcx->setHoldDelay(hold);
            i++;
          }
        else if (strcmp(argv[i],"--adelay")==0)
          {
            int adcDelay = strtoul(argv[i+1], NULL, 0);
            ttcx->setAdcDelay(adcDelay);
            i++;
          }
        else if (strcmp(argv[i],"--adcDelay")==0)
          {
            int adcDelay = strtoul(argv[i+1], NULL, 0);
            ttcx->setAdcDelay(adcDelay);
            i++;
          }
        else if (strcmp(argv[i],"--vthrScan")==0)
          {
            doVthrScan = true;
          }
        else if (strcmp(argv[i],"--stripScan")==0)
          {
            doStripScan = true;
          }
        else if (strcmp(argv[i],"--rates")==0)
          {
            doTaRates = true;
          }
        else if (strcmp(argv[i],"--vthr")==0)
          {
            vthr = strtoul(argv[i+1], NULL, 0);
            printf("Set TA threshold %d\n", vthr);
            i++;
          }
        else if (strcmp(argv[i],"--vthrp")==0)
          {
            vthrp = strtoul(argv[i+1], NULL, 0);
            printf("Set P-side TA threshold %d\n", vthrp);
            i++;
          }
        else if (strcmp(argv[i],"--vthrn")==0)
          {
            vthrn = strtoul(argv[i+1], NULL, 0);
            printf("Set N-side TA threshold %d\n", vthrn);
            i++;
          }
        else if (strcmp(argv[i],"--frc")==0)
          {
            ifrc = strtoul(argv[i+1], NULL, 0);
            printf("Set FRC address %d\n", ifrc);
            i++;
          }
        else if (strcmp(argv[i],"--frcport")==0)
          {
            ifrcport = strtoul(argv[i+1], NULL, 0);
            printf("Set FRC channel %d\n", ifrcport);
            i++;
          }
        else if (strcmp(argv[i],"--frcRange")==0)
          {
            frcFirst = strtoul(argv[i+1], NULL, 0);
            i++;
            frcLast = strtoul(argv[i+1], NULL, 0);
            i++;
            printf("Set FRC address range from %d to %d\n", frcFirst, frcLast);
          }
        else if (strcmp(argv[i],"--dac")==0)
          {
            int dac = strtoul(argv[i+1], NULL, 0);
            printf("Set DAC to %d\n", dac);

            // set the calibration DACs
	    ttcx->writeDac(0, dac);
	    ttcx->writeDac(1, dac);

            i++;
          }
        else if (strcmp(argv[i],"--dacRef")==0) 
           { 
              int dac = strtoul(argv[i+1], NULL, 0); 
              printf("Set DAC reference to %d\n", dac); 
 
              ttcx->writeDac(2, dac); 
 
              i++; 
           } 
        else if (strcmp(argv[i],"--dacLevel")==0)
          {
	    ttcx->setDacLevel(true);
          }
        else if (strcmp(argv[i],"--dacb")==0)
          {
            int dac = strtoul(argv[i+1], NULL, 0);
            printf("Set DAC B to %d\n", dac);

	    ttcx->writeDac(1, dac);

            i++;
          }
        else if (strcmp(argv[i],"--daca")==0)
          {
            int dac = strtoul(argv[i+1], NULL, 0);
            printf("Set DAC A to %d\n", dac);

	    ttcx->writeDac(0, dac);

            i++;
          }
        else if (strcmp(argv[i],"--testDac")==0)
          {
            assert(!isTtcxMode);
            int dac = strtoul(argv[i+1], NULL, 0);
	    i++;
            printf("Set DAC X to %d\n", dac);

	    while (0)
	      {
		ttc->write32(0xAC, TTC_AC_DAC_LEVEL);
		sleep(1);
	      }

	    while (1)
	      {
		printf("set TTC_AC_DAC_CSN to 0\n");
		ttc->write32(0xAC, 0);
		sleep(2);
		printf("set TTC_AC_DAC_CSN to 1111\n");
		ttc->write32(0xAC, TTC_AC_DAC_CSN);
		sleep(4);
	      }

	    while (0)
	      {
		printf("set TTC_AC_DAC_SCLK to 0\n");
		ttc->write32(0xAC, 0);
		sleep(2);
		printf("set TTC_AC_DAC_SCLK to 1111\n");
		ttc->write32(0xAC, TTC_AC_DAC_SCLK);
		sleep(4);
	      }

	    while (0)
	      {
		printf("set TTC_AC_DAC_DIN to 0\n");
		ttc->write32(0xAC, 0);
		sleep(2);
		printf("set TTC_AC_DAC_DIN to 1111\n");
		ttc->write32(0xAC, TTC_AC_DAC_DIN);
		sleep(4);
	      }

	    exit(1);
          }
        else if (strcmp(argv[i],"--vf48")==0)
          {
            doVF48 = true;
          }
        else if (strcmp(argv[i],"--novf48")==0)
          {
            doVF48 = false;
          }
        else if (strcmp(argv[i],"--vf48debug")==0)
          {
            vf48debug = 1;
          }
        else if (strcmp(argv[i],"--vf48pio")==0)
          {
            vf48dma = 0;
          }
        else if (strcmp(argv[i],"--vf48addr")==0)
          {
            gVF48base[0] = strtoul(argv[i+1], NULL, 0);
            i++;
          }
        else if (strcmp(argv[i],"--vf48divisor")==0)
          {
            vf48divisor = strtoul(argv[i+1], NULL, 0);
            i++;
          }
        else if (strcmp(argv[i],"--vf48delay")==0)
          {
            vf48delay = strtoul(argv[i+1], NULL, 0);
            i++;
          }
        else if (strcmp(argv[i],"--vf48samples")==0)
          {
            gVF48samples = strtoul(argv[i+1], NULL, 0);
            i++;
          }
        else if (strcmp(argv[i],"--vf48timeout")==0)
          {
            vf48timeout = strtoul(argv[i+1], NULL, 0);
            i++;
          }
        else if (strcmp(argv[i],"--vf48range")==0)
          {
            int r = strtoul(argv[i+1], NULL, 0);
            i++;

            hmin = 512-r;
            hmax = 512+r;
          }
        else if (strcmp(argv[i],"--vf48time")==0)
          {
            tmin = strtoul(argv[i+1], NULL, 0);
            i++;

            tmax = strtoul(argv[i+1], NULL, 0);
            i++;
          }
        else if (strcmp(argv[i],"--nopdf")==0)
          {
            doHardcopy = false;
          }
        else if (strcmp(argv[i],"--fname")==0)
          {
            sprintf(gFname, "%s", argv[i+1]);
            i++;
          }
        else if (strcmp(argv[i],"--plot1")==0)
          {
            int r = strtoul(argv[i+1], NULL, 0);
            i++;

            base = r;
            printf("Plot channel %d\n", base);

            doPlotAll = false;
            doPlot1 = true;
          }
        else if (strcmp(argv[i],"--plot4")==0)
          {
            int r = strtoul(argv[i+1], NULL, 0);
            i++;

            base = r;
            printf("Plot 4 channels starting from channel %d\n", base);

            doPlotAll = false;
            doPlot4 = true;
          }
        else if (strcmp(argv[i],"--plot6")==0)
          {
            doPlotAll = false;
            doPlot6 = true;
          }
        else if (strcmp(argv[i],"--plot16")==0)
          {
            int r = strtoul(argv[i+1], NULL, 0);
            i++;

            base = r;
            printf("Plot 16 channels starting from channel %d\n", base);

            doPlotAll = false;
            doPlot16 = true;
          }
        else if (strcmp(argv[i],"--plotAdc")==0)
          {
            printf("Plot ADC pulse height plots\n");
            doPlotAdc = true;
          }
        else if (strcmp(argv[i],"--v513")==0)
          {
	    v513_init(vme);
	    v513_vetoEvents(vme);

            doV513 = true;
          }
        else if (strcmp(argv[i],"--v513test")==0)
	  {
	    printf("Test v513 pulser\n");
	    
	    while (1)
	      {
		v513_pulse(vme);
		sleep(1);
	      }
	  }
        else if (strcmp(argv[i],"--init")==0)
          {
            printf("Initialize the TTC\n");
            ttcx->init();
          }
        else if (strcmp(argv[i],"--extClk")==0)
          {
            printf("Use external mux clock\n");
            status = ttcx->setExternalClock();
            if (status != 0)
               exit(1);
          }
        else if (strcmp(argv[i],"--intClk")==0)
          {
            printf("Use internal mux clock\n");
            ttcx->setInternalClock();
          }
        else if (strcmp(argv[i],"--load")==0)
          {
            doLoad = true;
          }
        else if (strcmp(argv[i],"--loadAll")==0)
          {
            doLoad = false;
            doLoadAll = true;
          }
        else if (strcmp(argv[i],"--loadLoop")==0)
          {
            doLoadLoop = true;
          }
        else if (strcmp(argv[i],"--loop")==0)
          {
            doLoop = true;
          }
        else if (strcmp(argv[i],"--status")==0)
          {
            ttcx->status();
          }
        else if (strcmp(argv[i],"--exit")==0)
          {
            //ttc->status();
            exit(0);
          }
        else if (strcmp(argv[i],"--cycle")==0)
          {
            doCycle = strtoul(argv[i+1], NULL, 0);
            i++;
            printf("Set cycle count to %d\n", doCycle);
          }
        else if (strcmp(argv[i],"--profile")==0)
          {
            doProfile = true;
          }
        else if (strcmp(argv[i],"--noapprun")==0)
          {
            doAppRun = false;
          }
        else if (strcmp(argv[i],"--noload")==0)
          {
            doLoad = false;
            doLoadAll = false;
          }
        else if (strcmp(argv[i],"--noloaderrors")==0)
          {
            doLoadErrors = true;
          }
        else if (strcmp(argv[i],"--read")==0)
          {
            doRead = true;
          }
        else if (strcmp(argv[i],"--normal")==0)
          {
            doNormal = true;
          }
        else if (strcmp(argv[i],"--notrigger")==0)
          {
            doTrigger = false;
          }
        else if (strcmp(argv[i],"--extTrigger")==0)
          {
            doTrigger = false;
            doExtTrigger = true;
          }
        else if (strcmp(argv[i],"--reset")==0)
          {
            ttcx->enableReset(true);
          }
        else if (strcmp(argv[i],"--noreset")==0)
          {
            ttcx->enableReset(false);
          }
        else if (strcmp(argv[i],"--resetPol")==0)
          {
            ttcx->resetPol();
          }
        else if (strcmp(argv[i],"--hold")==0)
          {
	    ttcx->enableHold(true);
          }
        else if (strcmp(argv[i],"--nohold")==0)
          {
	    doHold = false;
	    ttcx->enableHold(false);
          }
        else if (strcmp(argv[i],"--holdPol")==0)
          {
            assert(!isTtcxMode);
            ttc->invertBits32(0xAC, TTC_AC_HOLD_POL);
          }
        else if (strcmp(argv[i],"--shinPol")==0)
          {
            assert(!isTtcxMode);
	    ttc->invertBits32(0xAC, TTC_AC_SHIN_POL);
          }
        else if (strcmp(argv[i],"--ckbPol")==0)
          {
            assert(!isTtcxMode);
	    ttc->invertBits32(0xAC, TTC_AC_CKB_POL);
          }
        else if (strcmp(argv[i],"--holdScan")==0)
          {
            doHoldScan = true;
          }
        else if (strcmp(argv[i],"--configScan")==0)
          {
            doConfigScan = true;
          }
        else if (strcmp(argv[i],"--testChan")==0)
          {
            doTest = true;
            testChan = strtoul(argv[i+1], NULL, 0);
            printf("Test channel %d\n", testChan);
            i++;
          }
        else if (strcmp(argv[i],"--testBootLoad")==0)
          {
            assert(!isTtcxMode);
            int fecAddr = ifrc | (ifrcport<<4);
            printf("Write FEC address 0x%x\n", fecAddr);
            ttc->write32(0xD0, TTC_BOOT_ENA|(fecAddr<<8));

            printf("Slow pulse the FRC LOAD signal\n");

            while (1)
              {
                ttc->setBits32(0xD0, TTC_LOAD);
                sleep(1);
                ttc->clearBits32(0xD0, TTC_LOAD);
                sleep(1);
              }
          }
        else if (strcmp(argv[i],"--testBootRead")==0)
          {
            assert(!isTtcxMode);
            int fecAddr = ifrc | (ifrcport<<4);
            printf("Write FEC address 0x%x\n", fecAddr);
            ttc->write32(0xD0, TTC_BOOT_ENA|(fecAddr<<8));

            printf("Slow pulse the FRC READ signal\n");

            while (1)
              {
                ttc->setBits32(0xD0, TTC_READ);
                sleep(1);
                ttc->clearBits32(0xD0, TTC_READ);
                sleep(1);
              }
          }
        else if (strcmp(argv[i],"--testBootData")==0)
          {
            assert(!isTtcxMode);
            int fecAddr = ifrc | (ifrcport<<4);
            printf("Write FEC address 0x%x\n", fecAddr);
            ttc->write32(0xD0, TTC_BOOT_ENA|TTC_LOAD|(fecAddr<<8));

            printf("Slow pulse the FRC REGIN signal\n");

            while (1)
              {
                ttc->setBits32(0xD0, TTC_XDATA);
                sleep(1);
                ttc->clearBits32(0xD0, TTC_XDATA);
                sleep(1);
              }
          }
        else if (strcmp(argv[i],"--testBootClock")==0)
          {
            assert(!isTtcxMode);
            int fecAddr = ifrc | (ifrcport<<4);
            printf("Write FEC address 0x%x\n", fecAddr);
            ttc->write32(0xD0, TTC_BOOT_ENA|TTC_LOAD|(fecAddr<<8));

            printf("Slow pulse the FRC CLKIN signal\n");

            while (1)
              {
                ttc->setBits32(0xD0, TTC_XCLK);
                sleep(1);
                ttc->clearBits32(0xD0, TTC_XCLK);
                sleep(1);
              }
          }
        else if (strcmp(argv[i],"--testBootAll")==0)
          {
            assert(!isTtcxMode);
            int fecAddr = ifrc | (ifrcport<<4);
            printf("Write FEC address 0x%x\n", fecAddr);
            ttc->write32(0xD0, TTC_BOOT_ENA|fecAddr);

	    while (1)
	      {
		printf("Set to 1 all FRC BOOT signals\n");
		ttc->setBits32(0xD0, TTC_LOAD|TTC_XCLK|TTC_XDATA|TTC_READ);
		sleep(1);
		printf("Set to 0 all FRC BOOT signals\n");
		ttc->clearBits32(0xD0, TTC_LOAD|TTC_XCLK|TTC_XDATA|TTC_READ);
		sleep(1);
	      }
		
            return 0;
          }
        else if (strcmp(argv[i],"--testLED")==0)
          {
            printf("Testing LEDs\n");

            while (1)
              {
                 ttcx->ledTest();
                 sleep(1);
              }
            return 0;
          }
        else if (strcmp(argv[i],"--testMuxClock")==0)
          {
            assert(!isTtcxMode);
	    assert(ttc->fRevision >= 2008030201);

            printf("Slow pulse the FRC CKB signal\n");

            while (1)
              {
		printf("CKB on!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS|TTC_AC_CKB_POL);
                sleep(2);
		printf("CKB off!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS);
                sleep(2);
              }
          }
        else if (strcmp(argv[i],"--testMuxShin")==0)
          {
            assert(!isTtcxMode);
	    assert(ttc->fRevision >= 2008030201);

            printf("Slow pulse the FRC SHIN signal\n");

            while (1)
              {
		printf("SHIN on!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS|TTC_AC_SHIN_POL);
                sleep(2);
		printf("SHIN off!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS);
                sleep(2);
              }
          }
        else if (strcmp(argv[i],"--testMuxHold")==0)
          {
            assert(!isTtcxMode);
	    assert(ttc->fRevision >= 2008030201);

            printf("Slow pulse the FRC HOLD signal\n");

            while (1)
              {
		printf("HOLD on!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS|TTC_AC_HOLD_POL);
                sleep(2);
		printf("HOLD off!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS);
                sleep(2);
              }
          }
        else if (strcmp(argv[i],"--testMuxReset")==0)
          {
            assert(!isTtcxMode);
	    assert(ttc->fRevision >= 2008030201);

            printf("Slow pulse the FRC RESET signal\n");

            while (1)
              {
		printf("RESET on!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS|TTC_AC_RESET_POL);
                sleep(2);
		printf("RESET off!\n");
                ttc->write32(0xAC, TTC_AC_SEQ_DIS);
                sleep(2);
              }
          }
        else if (strcmp(argv[i],"--testPulse")==0)
          {
            assert(!isTtcxMode);

            printf("test the TTC analog output pulsing, calPulse()\n");
            
            while (1)
              {
                ttc->calPulse();
                xudelay(ttc, 100);
              }
          }
        else if (strcmp(argv[i],"--testAddr")==0)
          {
            assert(!isTtcxMode);

            printf("Testing FEC addressing\n");

            while (1)
              {
                for (int ireg=0; ireg<1<<6; ireg++)
                  {
                    ttc->clearBits32(0xD0, 0x3F<<8);
                    ttc->setBits32(0xD0, ireg<<8);
                    int rd = ttc->read32(0xD0);
                    printf("reg2 wrote: %3d 0x%02x, read: %3d 0x%02x, xor: 0x%02x\n", ireg, ireg, rd, rd, rd^ireg);
                    xudelay(ttc, 1000);
                    //sleep(1);
                  }
              }
            return 0;
          }
        else if (strcmp(argv[i],"--calpulse")==0)
          {
             doCalpulse = true;
             doMux = false;
             doMuxPulse = false;
          }
        else if (strcmp(argv[i],"--mux")==0)
          {
             doMux = true;
             doCalpulse = false;
             doMuxPulse = false;
          }
        else if (strcmp(argv[i],"--muxpulse")==0)
          {
             doMuxPulse = true;
             doMux = false;
             doCalpulse = false;
          }
        else
          {
            printf("Unknown switch %s\n", argv[i]);
            exit(1);
          }
    }

  if (doVF48)
    {
       vf48_init(vme, vf48divisor, vf48delay);
    }

  ttcx->enableTrigIn(doTrigIn);
  ttcx->enableTrigOut(true);

  ttcx->status();

#define VTHR_DIV 1
#define MAX_VTHR (32/VTHR_DIV)
#define MAX_FPGA 4
#define TA_PER_FPGA 128
#define MAX_TA  (MAX_FPGA*TA_PER_FPGA)

  // threshold scan
  if (doVthrScan)
    {
      makeApp();

      double freq[MAX_VTHR][MAX_TA];
      
      memset(freq, 0, sizeof(freq));

      int num_fpga = 2*ttcx->GetNumTTC();
      assert(num_fpga > 0);
      assert(num_fpga <= MAX_FPGA);
      
      for (int vthr=MAX_VTHR-1; vthr>=0; vthr-=1)
        {
           int vthrn = VTHR_DIV*vthr;
           int vthrp = VTHR_DIV*vthr;
	  
          printf("-------------> vthr %d %d\n", vthrn, vthrp);
	  
          if (1)
             load_all(ttcx, doTest, vthrn, vthrp, 1);
	  
          // try and set trigger bank to scan
         
          if (gBank != 8)
             {
                gBankToStopScan  = gBank+1 ;
                gBankToStartScan = gBank   ;       
             }

          for (int ibank=gBankToStartScan ; ibank<gBankToStopScan ; ibank++) {
             for (int ifpga=0; ifpga<num_fpga; ifpga++)
                {
                   // select the TA bank, and clear counters
                   ttcx->writeTriggerFPGA(ifpga, 1, ibank<<4);
                   ttcx->vmeClearTriggerFPGA(ifpga);

                   double t0 = gettime();
                   
                   int accumulate = 1000;
                   
                   int iloop = 0;
                   bool enough = false;
                   
                   for (; iloop<accumulate; iloop++)
                      {
                         //ttc_printTA(ttcx);
                         //ttc_printCounters(ttcx);
                         //ttc_printTriggerCounters(ttcx);
                         
                         uint16_t count[MAX_TA];
                         
                         ttcx->readTaCounters16(ifpga, count);
                         
                         if (0) {
                            uint16_t xcount[16];
                            ttcx->readTaCounters16(ifpga, xcount);
                            
                            for (int k=0; k<16; k++) {
                               unsigned int d = xcount[k]-count[k];
                               //printf("%d ", xcount[k]-count[k]);
                               if (d > 100)
                                  printf("counter %d: %d->%d\n", k, count[k],xcount[k]);
                            }
                            //printf("\n");
                         }
                      	      
                         double t = gettime();
	      
                         double dt = t-t0;
                         
                         for (int i=0; i<16; i++)
                            {
                               freq[vthr][ifpga*128+ibank*16+i] = count[i]/dt;
                               if (count[i] > 50000)
                                  enough = true;
                               
                               if (iloop==0 && count[i] > 10000)
                                  enough = true;
                            }
                         
                         if (enough)
                            break;
                      }
                   
                   if (1) {
                      printf("vthr %2d %2d | bank %d, fpga %d | %4d |", vthrn, vthrp, ibank, ifpga, iloop);
                      for (int i=0; i<16; i++)
                         {
                            printf(" %7.0f", freq[vthr][ifpga*128+ibank*16+i]);
                            if (i%4==3)
                               printf(" | ");
                         }
                      printf("\n");
                   }
                }
          }
          
          //exit(123);
          
        } //loop over vthr
      
      printf("Summary:\n");
      for (int ifpga=0; ifpga<num_fpga; ifpga++)
         {
            for ( int ibank = gBankToStartScan ; ibank<gBankToStopScan ; ibank++ )
               //for (int ibank=0; ibank<8; ibank++)
               {
                  printf("-----> FPGA %d, BANK %d, TA %d..%d\n", ifpga, ibank, ifpga*128+ibank*16, ifpga*128+ibank*16+15);
                  for (int vthr=0; vthr<MAX_VTHR; vthr++)
                     {
                        printf("vthr %2d | ", vthr);
                        for (int i=0; i<16; i++)
                           {
                              printf(" %7.0f", freq[vthr][ifpga*128+ibank*16+i]);
                              if (i%4==3)
                                 printf(" | ");
                           }
                        printf("\n");
                     }
               }
         }
      
      if (1)
         {
            for (int ifpga=0; ifpga<num_fpga; ifpga++)
               {
                  for ( int ibank = gBankToStartScan ; ibank < gBankToStopScan ; ibank++ ) 
                     {
                        TCanvas *c = new TCanvas();
                        
                        char name[256];
                        sprintf(name,"FPGA %d, IFRC %d", ifpga, ibank);
                        c->SetTitle(name);
                        
                        c->Divide(4, 4);
                        
                        printf("-----> FPGA %d, BANK %d, TA %d..%d\n", ifpga, ibank, ifpga*128+ibank*16, ifpga*128+ibank*16+15);
                        
                        TGraph* gg[16];
                        
                        double max = 0;
                        
                        for (int i=0; i<16; i++)
                           {
                              c->cd(i+1);
                              
                              TGraph *g = new TGraph();
                              gg[i] = g;
                              char title[256];
                              sprintf(title,"FPGA %d, Bank %d, TA %d, freq %d", ifpga, ibank, i, (int)freq[0][ifpga*128+ibank*16+i]);
                              g->SetTitle(title);
                              //g->Set(MAX_VTHR);
                              for (int vthr=0; vthr<MAX_VTHR; vthr++)
                                 g->SetPoint(vthr, (double)vthr, freq[vthr][ifpga*128+ibank*16+i]);
                              
                              g->GetYaxis()->SetNoExponent(true);
                              g->SetMinimum(0);
                              
                              double lmax = 0;
                              
                              for (int vthr=0; vthr<MAX_VTHR; vthr++)
                                 if (freq[vthr][ifpga*128+ibank*16+i] > lmax)
                                    lmax = freq[vthr][ifpga*128+ibank*16+i];
                              
                              if (lmax > max)
                                 max = lmax;
                              
                              if (lmax < 100)
                                 g->SetMaximum(100);
                              else if (lmax < 1000)
                                 g->SetMaximum(1000);
                           }
                        
                        double ymax = -1;
                        //if (max<100)
                        //  ymax = 100;
                        //else if (max<1000)
                        //  ymax = 1000;
                        
                        for (int i=0; i<16; i++)
                           {
                              c->cd(i+1);
                              
                              TGraph*g = gg[i];
                              
                              switch (i%4)
                                 {
                                 case 0:
                                 case 3:
                                    if (ymax>0)
                                       g->SetMaximum(ymax);
                                    break;
                                 case 1:
                                 case 2:
                                    if (ymax>0)
                                       g->SetMaximum(ymax);
                                    break;
                                 }
                              g->Draw("AL*");
                              //g->Print();
                              gPad->Modified();
                           }
                        
                        c->Modified();
                        c->Update();
                        
                        sprintf(name,"vthr_scan_%d%d.root", ifpga, ibank);
                        c->SaveAs(name);
                        
                        sprintf(name,"vthr_scan_%d%d.pdf", ifpga, ibank);
                        c->SaveAs(name);
                     }
               }
            // app->Run(true);
         }
      exit(0);
    } // end of threshold scan

  // TA strip scan
  if (doStripScan)
    {
      makeApp();

      double sfreq[2+128][MAX_TA];
      
      memset(sfreq, 0, sizeof(sfreq));

      int num_fpga = 2*ttcx->GetNumTTC();
      assert(num_fpga > 0);
      assert(num_fpga <= MAX_FPGA);
      
      for (int istrip=-1; istrip<129; istrip++) {

         //if (istrip != -1 && istrip > 2)
         //continue;

         //if (istrip > 10)
         //   continue;

         if (0) {
            if (istrip == -1) {}
            else if (istrip == 0) {}
            else if (istrip == 127) {}
            else if (istrip == 128) {}
            else
               continue;
         }
            
         int vthrn = 31;
         int vthrp = 4;
	  
         printf("-------------> strip %d\n", istrip);
	  
         if (1)
            load_all(ttcx, doTest, vthrn, vthrp, 1, istrip);
	  
         for (int ibank=0; ibank<8; ibank++) {
            for (int ifpga=0; ifpga<num_fpga; ifpga++) {

               if (ifpga == 0 && ibank == 3) // nothing connected
                  continue;

               if (ifpga == 0 && ibank == 7) // nothing connected
                  continue;

               if (ifpga == 1 && ibank == 3) // nothing connected
                  continue;

               if (ifpga == 1 && ibank == 7) // nothing connected
                  continue;

               if (ifpga == 3) // nothing connected
                  continue;

               // select the TA bank, and clear counters
               ttcx->writeTriggerFPGA(ifpga, 1, ibank<<4);
               ttcx->vmeClearTriggerFPGA(ifpga);
               
               double t0 = gettime();
               
               int accumulate = 1000000;
               
               int iloop = 0;
               bool enough = false;
               
               for (; iloop<accumulate; iloop++) {
                  //ttc_printTA(ttcx);
                  //ttc_printCounters(ttcx);
                  //ttc_printTriggerCounters(ttcx);
                  
                  uint16_t count[MAX_TA];
                  
                  ttcx->readTaCounters16(ifpga, count);
                  
                  if (0) {
                     uint16_t xcount[16];
                     ttcx->readTaCounters16(ifpga, xcount);
                     
                     for (int k=0; k<16; k++) {
                        unsigned int d = xcount[k]-count[k];
                        //printf("%d ", xcount[k]-count[k]);
                        if (d > 100)
                           printf("counter %d: %d->%d\n", k, count[k],xcount[k]);
                     }
                     //printf("\n");
                  }
                  
                  double t = gettime();
                  
                  double dt = t-t0;
                  
                  for (int i=0; i<16; i++) {
                     sfreq[1+istrip][ifpga*128+ibank*16+i] = count[i]/dt;
                     //if (count[i] > 50000)
                     //enough = true;
                     
                     //if (iloop==0 && count[i] > 10000)
                     //   enough = true;

                     //if (count[i] < 2)
                     //enough = false;

                     if (count[i] > 5)
                        enough = true;
                  }

                  if (1) { // make sure there are enough P-side counts
                     if (count[1] < 2)
                        enough = false;
                     if (count[2] < 2)
                        enough = false;

                     if (count[5] < 2)
                        enough = false;
                     if (count[6] < 2)
                        enough = false;

                     if (count[9] < 2)
                        enough = false;
                     if (count[10] < 2)
                        enough = false;

                     if (count[13] < 2)
                        enough = false;
                     if (count[14] < 2)
                        enough = false;
                  }

                  if (dt > 2)
                     enough = true;
                  
                  if (enough) {
                     for (int i=0; i<16; i++) {
                        printf("accumulate %d, i=%d, count=%d\n", accumulate, i, count[i]);
                     }
                  }

                  if (enough)
                     break;
               }
               
               if (1) {
                  printf("strip %3d | bank %d, fpga %d | %4d |", istrip, ibank, ifpga, iloop);
                  for (int i=0; i<16; i++)
                     {
                        printf(" %7.1f", sfreq[1+istrip][ifpga*128+ibank*16+i]);
                        if (i%4==3)
                           printf(" | ");
                     }
                  printf("\n");
               }
               //break; // FIXME
            } // loop over fpga
            //break; // FIXME
         } // loop over banks

         if (1) {
            for (int ifpga=0; ifpga<num_fpga; ifpga++) {
               for (int ibank=0; ibank<8; ibank++) {
                  TCanvas *c = new TCanvas();
                        
                  char name[256];
                  sprintf(name,"FPGA %d, IFRC %d", ifpga, ibank);
                  c->SetTitle(name);
                        
                  c->Divide(4, 4);
                  
                  printf("-----> FPGA %d, BANK %d, TA %d..%d\n", ifpga, ibank, ifpga*128+ibank*16, ifpga*128+ibank*16+15);
                        
                  for (int i=0; i<16; i++) {
                     c->cd(i+1);

                     char title[256];
                     sprintf(title,"FPGA %d, Bank %d, TA %d", ifpga, ibank, i);
                        
                     TH1D *h = new TH1D(title, title, 130, -1, 129);
                     for (int istrip=-1; istrip<129; istrip++)
                        h->SetBinContent(2+istrip, sfreq[1+istrip][ifpga*128+ibank*16+i]);
                     h->Draw();
                     
                     if (0) {
                     TGraph *g = new TGraph();
                     g->SetTitle(title);
                     for (int istrip=-1; istrip<129; istrip++)
                        g->SetPoint(1+istrip, istrip, sfreq[1+istrip][ifpga*128+ibank*16+i]);
                     
                     g->GetYaxis()->SetNoExponent(true);
                     g->SetMinimum(0);

                     g->Draw("AL*");
                     //g->Print();
                     }
                     gPad->Modified();
                  }
                  
                  c->Modified();
                  c->Update();
                  
                  sprintf(name,"strip_scan_%d%d.root", ifpga, ibank);
                  c->SaveAs(name);
                  
                  sprintf(name,"strip_scan_%d%d.pdf", ifpga, ibank);
                  c->SaveAs(name);
               }
            }
            // app->Run(true);
         } // if report
      } //loop over strips
      
      printf("Summary:\n");
      for (int ifpga=0; ifpga<num_fpga; ifpga++) {
         for (int ibank=0; ibank<8; ibank++) {
            printf("-----> FPGA %d, BANK %d, TA %d..%d\n", ifpga, ibank, ifpga*128+ibank*16, ifpga*128+ibank*16+15);
            for (int istrip=-1; istrip<129; istrip++) {
               printf("strip %3d | ", istrip);
               for (int i=0; i<16; i++) {
                  printf(" %7.1f", sfreq[1+istrip][ifpga*128+ibank*16+i]);
                  if (i%4==3)
                     printf(" | ");
               }
               printf("\n");
            }
         }
      }
      
      exit(0);
    } // end of TA strip scan

  // report TA firing rates
  if (doTaRates)
    {
      double freq[MAX_TA];
      
      memset(freq, 0, sizeof(freq));

      int num_fpga = 2*ttcx->GetNumTTC();
      assert(num_fpga > 0);
      assert(num_fpga <= MAX_FPGA);
      
      for (int ibank=0; ibank<8; ibank++) {
         for (int ifpga=0; ifpga<num_fpga; ifpga++)
            {
               // select the TA bank, and clear counters
               ttcx->writeTriggerFPGA(ifpga, 1, ibank<<4);
               ttcx->vmeClearTriggerFPGA(ifpga);

               double t0 = gettime();
                   
               int accumulate = 90000;
               
               int iloop = 0;
               bool enough = false;
               
               for (; iloop<accumulate; iloop++)
                  {
                     //ttc_printTA(ttcx);
                     //ttc_printCounters(ttcx);
                     //ttc_printTriggerCounters(ttcx);
                     
                     uint16_t count[MAX_TA];
                     
                     ttcx->readTaCounters16(ifpga, count);
                     
                     if (0) {
                        uint16_t xcount[16];
                        ttcx->readTaCounters16(ifpga, xcount);
                        
                        for (int k=0; k<16; k++) {
                           unsigned int d = xcount[k]-count[k];
                           //printf("%d ", xcount[k]-count[k]);
                           if (d > 100)
                              printf("counter %d: %d->%d\n", k, count[k],xcount[k]);
                        }
                        //printf("\n");
                     }
                     
                     double t = gettime();
                     
                     double dt = t-t0;
                     
                     for (int i=0; i<16; i++)
                        {
                           freq[ifpga*128+ibank*16+i] = count[i]/dt;
                           if (count[i] == 1)
                              freq[ifpga*128+ibank*16+i] = 1;

                           if (count[i] > 50000)
                              enough = true;
                           
                           if (iloop==0 && count[i] > 10000)
                              enough = true;
                        }
                     
                     if (enough)
                        break;
                  }
               
               if (1) {
                  printf("bank %d, fpga %d | %4d |", ibank, ifpga, iloop);
                  for (int i=0; i<16; i++)
                     {
                        printf(" %7.0f", freq[ifpga*128+ibank*16+i]);
                        if (i%4==3)
                           printf(" | ");
                     }
                  printf("\n");
               }
            }
      }

      printf("Summary:\n");
      for (int ifpga=0; ifpga<num_fpga; ifpga++)
         {
            for (int ibank=0; ibank<8; ibank++)
               {
                  printf("FPGA %d, BANK %d, TA %3d..%3d | ", ifpga, ibank, ifpga*128+ibank*16, ifpga*128+ibank*16+15);

                  for (int i=0; i<16; i++)
                     {
                        printf(" %7.0f", freq[ifpga*128+ibank*16+i]);
                        if (i%4==3)
                           printf(" | ");
                     }
                  printf("\n");
               }
         }

      exit(0);
    } // end of TA rates report

  if (doCalpulse)
     {
        HybridConfig cfg;
        memset(&cfg, 0, sizeof(cfg));

        while (1) {
           
           printf("Single channel calibration pulse mode!\n");
                  
           if (doLoadAll)
              load_all(ttcx, doTest, vthrn, vthrp, 1);
                             
           if (doLoad)
              status = ttcx->writeHybridConfig(ifrc, ifrcport, &cfg);

           ttcx->enableTrigIn(doTrigIn);
           ttcx->enableTrigOut(true);
                             
           ttcx->setTestMode(testChan, false, true, false);
           ttcx->enableHold(doHold);
           ttc_clearCounters(ttcx);
           ttcx->vmeClearTriggerFPGA();

           printf("Running %d cycles of calibration pulses!\n", doCycle);
                              
           for (int xxx=0; xxx<doCycle; xxx++)
              {
                 printf("Calibration pulse %d!\n", xxx);
                 if (doV513)
                    v513_pulse(vme);
                 else
                    ttcx->calPulse();
                 
                 if (doVF48)
                    vf48_read(vme);
                 
                 ttc_printTA(ttcx);
                 ttc_printCounters(ttcx);
                 ttc_printTriggerCounters(ttcx);
                 
                 ttc_printLatchFifo(ttcx);
                 
                 // clear "busy"
                 ttcx->writeCommand(TTC_CMD_Reset);
                 
                 if (!doProfile)
                    sleep(1);
              }
                              
           if (doProfile)
              {
                 if (gVF48Window && doHardcopy) {
                    char fname[256];
                    sprintf(fname, "%s-0x%08X.root", gFname, gVF48base[0]);
                    gVF48Window->SaveAs(fname);
                    sprintf(fname, "%s-0x%08X.pdf", gFname, gVF48base[0]);
                    gVF48Window->SaveAs(fname);
                 }

                 printf("Done!\n");
                 if (doAppRun)
                    app->Run(true);
                 exit(0);
              }
        }
     }

  if (doMux)
     {
        HybridConfig cfg;
        memset(&cfg, 0, sizeof(cfg));

        while (1) {
           printf("Loop VA1 mux readout sequence!\n");

           //ttcx->clearBits32(0xC0, 3<<10);ttcx->setBits32(0xC0, 2<<10);
           
           ttcx->setMuxMode();
           ttcx->setMuxTrigSource(2);
           
           //ttcx->writeCommand(TTC_CMD_Reset);
           
           ttcx->enableHold(doHold);
           
           if (doLoadAll)
              status = load_all(ttcx, 0, vthrn, vthrp, 1);
           
           if (doLoad)
              status = ttcx->writeHybridConfig(ifrc, ifrcport, &cfg);
           
           ttc_clearCounters(ttcx);
           ttcx->vmeClearTriggerFPGA();
           
           if (doExtTrigger) {
              ttcx->setMuxTrigSource(0);
              ttcx->enableTrigIn(true);
           }

           printf("Starting %d mux cycles!\n", doCycle);
           
           for (int xxx=0; xxx<doCycle; xxx++) {
              printf("Read mux cycle %d\n", xxx);
              
              //sleep(10);
              
              if (doTrigger) {
                 if (doV513)
                    v513_pulse(vme);
                 else if (1)
                    ttcx->vmeTrigger();
                 else
                    ttcx->calPulse();
                 //sleep(1);
                 //xudelay(ttc, 50);
              }
              
              if (doVF48)
                 vf48_read(vme);
              
              ttc_printTA(ttcx);
              ttc_printCounters(ttcx);
              ttc_printTriggerCounters(ttcx);
              
              ttc_printLatchFifo(ttcx);
              
              ttc_clearCounters(ttcx);
              ttcx->vmeClearTriggerFPGA();
              
              //   if (doTrigger && !doProfile)
              //     sleep(1);
           }

           if (doProfile)
              {
                 if (gVF48Window && doHardcopy) {
                    char fname[256];
                    sprintf(fname, "%s-0x%08X.root", gFname, gVF48base[0]);
                    gVF48Window->SaveAs(fname);
                    sprintf(fname, "%s-0x%08X.pdf", gFname, gVF48base[0]);
                    gVF48Window->SaveAs(fname);
                 }

                 printf("Done!\n");
                 if (doAppRun)
                    app->Run(true);
                 exit(0);
              }
        }
     }
  
  if (doMuxPulse)
     {
        HybridConfig cfg;
        memset(&cfg, 0, sizeof(cfg));

        while (1) {
           printf("Loop VA1 mux readout sequence!\n");

           if (doLoadAll)
              load_all(ttcx, 1, vthrn, vthrp, 1);
                             
           if (doLoad)
              status = ttcx->writeHybridConfig(ifrc, ifrcport, &cfg);
           
           ttc_clearCounters(ttcx);
           ttcx->vmeClearTriggerFPGA();
           
           printf("Starting %d mux cycles!\n", doCycle);
           
           for (int xxx=0; xxx<doCycle; xxx++)
              {
                 printf("Read mux cycle %d\n", xxx);
                 
                 ttcx->enableHold(true);

                 //ttc->setBits32(0xAC, (1<<8));

                 ttcx->setTestMode(testChan, false, true, true);

                 //ttc->clearBits32(0xAC, TTC_AC_SEQ_DIS);

                 //ttc->setBits32(0xAC, (1<<10));
                 ttcx->calPulse();

                 //for (int i=0; i<20; i++)
                 //   ttc->read32(0x48);

                 //ttc->invertBits32(0xAC, TTC_AC_RESET_POL);
                 //ttc->invertBits32(0xAC, TTC_AC_RESET_POL);

                 //ttc->setMuxMode();
                 
                 //ttc->clearBits32(0xAC, (1<<10));
                 //ttc->vmeTrigger();
                 
                 if (doVF48)
                    vf48_read(vme);
                 
                 //ttc->setBits32(0xAC, (1<<9));
                 //ttc->clearBits32(0xAC, (1<<9));

                 ttc_printTA(ttcx);
                 ttc_printCounters(ttcx);
                 ttc_printTriggerCounters(ttcx);
                 
                 ttc_printLatchFifo(ttcx);
                 
                 ttc_clearCounters(ttcx);
                 ttcx->vmeClearTriggerFPGA();
                 
                 //   if (doTrigger && !doProfile)
                 //     sleep(1);
              }
           
           if (doProfile)
              {
                 if (gVF48Window && doHardcopy) {
                    char fname[256];
                    sprintf(fname, "%s-0x%08X.root", gFname, gVF48base[0]);
                    gVF48Window->SaveAs(fname);
                    sprintf(fname, "%s-0x%08X.pdf", gFname, gVF48base[0]);
                    gVF48Window->SaveAs(fname);
                 }

                 printf("Done!\n");
                 if (doAppRun)
                    app->Run(true);
                 exit(0);
              }
        }
     }

  mvme_close(vme);

  return 0;
}

/* emacs
 * Local Variables:
 * mode:c++
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

// end
