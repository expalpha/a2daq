#!/bin/sh

echo ttc_dutyTA: Check whether TA signals are stuck at logical 1 or 0.

./testTTC.exe --ttcx --dutyTA --exit

#end
