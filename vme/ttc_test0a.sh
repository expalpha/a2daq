#!/bin/sh

echo ttc_test0a: Test VA1TA calibration pulser, all FRCs, all FRC ports

./testTTC.exe --ttcx --init --nohold --loadAll --calpulse --testChan 10 --dacRef 1 --dac 60 --vf48 --vf48range 400 --vf48divisor 3 --enableTA $*

#end
