#!/bin/sh

echo ttc_test0b: Test integrated VA1TA calibration pulser, FRCs, all FRC ports

./testTTC.exe --ttcx --init --nohold --loadAll --calpulse --testChan 10 --dacRef 1 --dac 60 --vf48 --vf48range 600 --vf48divisor 3 --enableTA --cycle 10 --profile $*

#end
