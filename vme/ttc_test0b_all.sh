#!/bin/sh

echo ttc_test0b_all: calibration pulser

./ttc_test0b.sh --fname test0b --noapprun --vf48addr 0xa00000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xab0000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xac0000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xad0000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xae0000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xaf0000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xa10000
./ttc_test0b.sh --noload --fname test0b --noapprun --vf48addr 0xa20000
#end
