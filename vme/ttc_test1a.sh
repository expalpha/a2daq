#!/bin/sh

echo ttc_test1a: Test VA1TA multiplexor without HOLD, load and look at all FRCs

./testTTC.exe --ttcx --init --mux --nohold --loadAll --vf48 --vf48range 600 --vf48divisor 3 $*

#end
