#!/bin/sh

echo ttc_test2a: Test VA1TA multiplexor and reset

./testTTC.exe --ttcx --init --mux --nohold --loadAll --vf48 --vf48range 600 --resetPol --vf48divisor 3 $*

#end
