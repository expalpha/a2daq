#!/bin/sh

echo ttc_test3a: Test VA1TA calibration pulser, with HOLD

./testTTC.exe --ttcx --init --hold --adcDelay 100 --holdDelay 11000 --loadAll --calpulse --testChan 10 --dac 60 --vf48 --vf48range 500 --vf48divisor 3 --enableTA $*

#end
