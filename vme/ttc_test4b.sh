#!/bin/sh

echo ttc_test4a: Test VA1TA multiplexor and HOLD

./testTTC.exe --ttcx --init --loadAll --mux --vf48 --vf48range 600 --hold --vf48divisor 3 --cycle 10 --profile $*

#end
