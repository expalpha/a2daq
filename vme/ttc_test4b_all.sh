#!/bin/sh

echo ttc_test4b_all: VA1TA analog mux output

./ttc_test4b.sh --fname test4b --noapprun --vf48addr 0xa00000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xab0000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xac0000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xad0000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xae0000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xaf0000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xa10000
./ttc_test4b.sh --noload --fname test4b --noapprun --vf48addr 0xa20000
#end
