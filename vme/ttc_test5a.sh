#!/bin/sh

echo ttc_test5a: Run VA1TA calibration pulser through the VA1 mux, all FRCs, all FRC ports

./testTTC.exe --ttcx --init --loadAll --vf48 --vf48range 600 --vf48divisor 3 --muxpulse --testChan 10 --holdDelay 1000 --dacRef 1 --dac 60 $*

#end
