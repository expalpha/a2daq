#!/bin/sh

echo ttc_test5b_all: calibration pulser through VA1TA analog mux

./ttc_test5b.sh --ttc1 --fname test5b --noapprun --vf48addr 0xa00000
./ttc_test5b.sh --ttc2 --fname test5b --noapprun --vf48addr 0xa10000
./ttc_test5b.sh --ttc2 --fname test5b --noapprun --vf48addr 0xa20000
./ttc_test5b.sh --ttc1 --fname test5b --noapprun --vf48addr 0xab0000
./ttc_test5b.sh --ttc1 --fname test5b --noapprun --vf48addr 0xac0000
./ttc_test5b.sh --ttc1 --fname test5b --noapprun --vf48addr 0xad0000
./ttc_test5b.sh --ttc2 --fname test5b --noapprun --vf48addr 0xae0000
./ttc_test5b.sh --ttc2 --fname test5b --noapprun --vf48addr 0xaf0000

#end
