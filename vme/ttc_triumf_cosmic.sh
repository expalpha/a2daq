#!/bin/sh

echo ttc_read: Read events from the Si DAQ

TAALL=" --enableTA"
TAPSIDE=" --disableTA --enableTA3 0 0 5 --enableTA3 0 0 6 --enableTA3 0 0 9 --enableTA3 0 0 10 --enableTA3 0 0 13 --enableTA3 0 0 14"

./testTTC.exe --init --loadAll --vf48 --vf48timeout 6000000 --vf48range 500 --vf48divisor 3 $TAPSIDE --clearTaMap --setTaMap 0 1 1 --setTaMap 0 2 2 --setTaMap 0 3 3 --readTaMap --setMult 0 0 1 255 --setMult 0 1 1 255 --setMult 0 2 1 255 --setTrig3 0 0 134 --setTrig3 0 1 135 --setTrig3 0 2 136 --enableTrigOut --holdDelay 1000 --hold --notrigger --extTrigger --cycle 99999 --disableVeto --enableTrigIn --regB0 0x10080 --mux $*

# --selectCounters 134 135 136 128 128 128 
# OR  of M1+M2: --regB0 0x10003
# AND of M1*M2: --regB0 0x10010
# AND of M1*M2*M3: --regB0 0x10080

#end
