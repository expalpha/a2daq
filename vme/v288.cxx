//
// Name: DaqDriverV288.cpp
// Author: K.Olchanski and others
// Date: 24 Aug 1999
// Description: driver for the CAEN V288 interface
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#include "v288.h"
#include "mvmestd.h"

#if 0
  struct Registers
  {
    volatile uint16 dataReg;     // read/write data
    volatile uint16 statusReg;   // bit 0: 0=valid, 1=error
    volatile uint16 transmitReg; // write op starts transmission
    volatile uint16 resetReg;    // write op resets board
    volatile uint16 intrVecReg;  // set the intr vector, level is set by jumpers
  };
#endif

inline static uint16_t read16(v288* p, int ireg)
{
  return mvme_read_value((MVME_INTERFACE*)p->fVme, p->fVmeBaseAddr + ireg);
}

inline static void write16(v288* p, int ireg, uint16_t data)
{
  mvme_write_value((MVME_INTERFACE*)p->fVme, p->fVmeBaseAddr + ireg, data);
}

#define DATA_REG    0
#define STATUS_REG  2
#define XMIT_REG    4
#define RESET_REG   6
#define INTRVEC_REG 8

v288::v288(void* vme, uint32_t vmeA24addr)
{
  fVme = vme;
  fVmeBaseAddr = vmeA24addr;
  fDebug = 0;

  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A24);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D16);

  printf("V288: VME A24 addr: 0x%06x, status 0x%04x\n",
	 vmeA24addr,
	 read16(this, STATUS_REG));
};

int v288::init()
{
  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A24);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D16);
  printf("V288::init: status: 0x%04x\n", read16(this, STATUS_REG));
  return 0;
};

int v288::reset()
{
  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A24);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D16);
  printf("V288: Reset.\n");
  write16(this, RESET_REG, 1);
  printf("V288: Reset: status: 0x%04x\n", read16(this, STATUS_REG));
  sleep(1);
  printf("V288: Reset: status: 0x%04x\n", read16(this, STATUS_REG));
  return 0;
};

int v288::writeData(int wordCount,const uint16_t data[])
{
  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A24);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D16);

  for (int i=0; i<wordCount; i++)
    write16(this, DATA_REG, data[i]);

  uint16_t status = read16(this, STATUS_REG);

  if ((status&1) != 0)
    {
      printf("V288: Error: Cannot write into the V288 send buffer, status 0x%x.\n", status);
      return -1;
    }

  return 0;
}

int v288::startTransmit()
{
  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A24);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D16);

  write16(this, XMIT_REG, 1);

  uint16_t status = read16(this, STATUS_REG);
  if ((status&1) != 0)
    {
      printf("V288: Error: Cannot start data transmission, status 0x%x.\n", status);
      return -1;
    }

  return 0;
}

int v288::readData(int bufSize, uint16_t buf[], int &wordCount)
{
  mvme_set_am((MVME_INTERFACE*)fVme, MVME_AM_A24);
  mvme_set_dmode((MVME_INTERFACE*)fVme, MVME_DMODE_D16);

  wordCount = 0;

  uint16_t data   = 0;
  uint16_t status = 0;

  //fDebug = true;

  // wait for data
  int iwait;
  for (iwait=0; iwait<1000; iwait++)
    {
      data   = read16(this, DATA_REG);
      status = read16(this, STATUS_REG);

      if (fDebug)
        printf("v288::readData: Wait %d, data 0x%04x, status 0x%04x\n", iwait, data, status);

      //if (data != 0xFFFF)
      //  break;

      if ((status&1) == 0)
        break;

      usleep(100000);
    }

  if ((status&1) != 0)
    {
      printf("V288: Error: Timeout waiting for V288 response, status 0x%x.\n", status);
      return 0;
    }

  time_t timeout = time(NULL) + 5;

  while (1)
    {
      if (fDebug)
        printf("V288::readData: Wait: %d, Data: 0x%04x, status: 0x%x (char [%c])\n",
               iwait,data,status,data&0xFF);

      if (data == 0xffff)
        {
          printf("V288: Notice: Timeout waiting for slave response.\n");
          break;
        }
      else if (data == 0xfffe)
        {
          printf("V288: Error: Controller ID is incorrect.\n");
          break;
        }
      else if (data == 0xfffd)
        {
          printf("V288: Error: Empty transmit buffer.\n");
          break;
        }
      else if (data == 0xff01)
        {
          printf("V288: Warning: Incorrect message.\n");
          break;
        }
#ifdef UNDEF  // these messages are returned to the caller.
      else if (data == 0xff02)
        {
          printf("V288: Warning: Incorrect set value.\n");
          break;
        }
      else if (data == 0xff03)
        {
          printf("V288: Warning: Voltage not supplied.\n");
          break;
        }
#endif
      else
        {
          if (wordCount < bufSize)
            {
              buf[wordCount++] = data;
            }
	  
        }

      data   = read16(this, DATA_REG);
      status = read16(this, STATUS_REG);

      if (((status&1) != 0))
        break;

      int t0 = time(NULL) - timeout;
      //printf("t0 = %d\n", t0);
      if (t0 > 0)
        break;
    }

  return 0;
}

int v288::sendCommand0(int       slaveAddr,
		       uint16_t  command,
		       int       bufSize,
		       uint16_t  buffer[],
		       int      &wordsRead)
{
  int status;
  uint16_t packet[3];
  packet[0] = 1;
  packet[1] = slaveAddr;
  packet[2] = command;
  status = writeData(3,packet);
  if (status!=0)
    return 1;
  status = startTransmit();
  if (status!=0)
    return 1;
  status = readData(bufSize,buffer,wordsRead);
  if (status!=0)
    return 1;
  return 0;
}

int v288::sendCommand1(int       slaveAddr,
		       uint16_t  command,
		       uint16_t  argument,
		       int       bufSize,
		       uint16_t  buffer[],
		       int      &wordsRead)
{
  uint16_t packet[4];
  packet[0] = 1;
  packet[1] = slaveAddr;
  packet[2] = command;
  packet[3] = argument;
  if (writeData(4,packet)) return 1;
  if (startTransmit())     return 1;
  if (readData(bufSize,buffer,wordsRead)) return 1;
  return 0;
}

#if 0
int main(int argc,char*argv[])
{
  int status;
  MVME_INTERFACE *vme = 0;

  status = mvme_open(&vme,0);

  v288* v = new v288(vme, 0x000000);
  v->reset();

  const int bufsiz = 100;
  uint16_t buf[bufsiz];
  int nread = 0;
  status = 0;

  if (argc == 3)
    {
      int arg1 = strtoul(argv[1],NULL,0);
      int arg2 = strtoul(argv[2],NULL,0);
      printf("Command0: 0x%x, 0x%x\n", arg1, arg2);
      status = v->sendCommand0(arg1, arg2, bufsiz, buf, nread);
    }
  else if (argc == 4)
    {
      int arg1 = strtoul(argv[1],NULL,0);
      int arg2 = strtoul(argv[2],NULL,0);
      int arg3 = strtoul(argv[3],NULL,0);
      printf("Command0: 0x%x, 0x%x, 0x%x\n", arg1, arg2, arg3);
      status = v->sendCommand1(arg1, arg2, arg3, bufsiz, buf, nread);
    }

  printf("status %d, nread %d\n", status, nread);

  if (nread==20)
    {
      int i=0;
      i++;
      printf("v0: %d\n", buf[i++]);
      printf("v1: %d\n", buf[i++]);
      printf("i0: %d\n", buf[i++]);
      printf("i1: %d\n", buf[i++]);
      printf("rup: %d\n", buf[i++]);
      printf("rdwn: %d\n", buf[i++]);
      printf("trip: %d\n", buf[i++]);
      printf("status: %d\n", buf[i++]);
      printf("gr_ass: %d\n", buf[i++]);
      printf("vread: %d\n", buf[i++]);
      printf("iread: %d\n", buf[i++]);
      printf("st_phase: %d\n", buf[i++]);
      printf("st_time: %d\n", buf[i++]);
      printf("mod_type: %d\n", buf[i++]);
      printf("chname: %s\n", &buf[i++]);
    }

  mvme_close(vme);

  return 0;
}
#endif

// end
