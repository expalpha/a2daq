//
// Name: v288.h
// Date: 12 Apr 2007
// Adopted from BRAHMS DAQ software
//
// Name: DaqDriverV288.h
// Author: K.Olchanski
// Date: 24 Aug 1999
// Description: driver for the CAEN V288 interface
//
#ifndef DaqDriverV288H
#define DaqDriverV288H

#include <stdio.h>
#include <stdint.h>

//
// Interface for the VME V288 CAENET interface
//

class v288
{
public: // public methods
  v288(void* vmeInterface, uint32_t vmeA24addr); // ctor

  //
  // initialize the V288
  //
  // Returns: 0-success, !0-error
  //
  int init();

  //
  // reset the V288 interface
  //
  int reset();

  //
  // Low level interface controls
  //

  int    writeData(int wordCount,const uint16_t data[]);
  int    startTransmit();
  int    readData(int bufSize,uint16_t buf[],int &wordCount);

  //
  // High level commands
  //

  int    sendCommand0(int      slaveAddr,
		      uint16_t command,
		      int      bufSize,
		      uint16_t buffer[],
		      int   &wordsRead);

  int    sendCommand1(int      slaveAddr,
		      uint16_t command,
		      uint16_t argument,
		      int      bufSize,
		      uint16_t buffer[],
		      int   &wordsRead);

public: // private data
  void* fVme;
  uint32_t fVmeBaseAddr;
  int fDebug;
};

#endif
// end file
