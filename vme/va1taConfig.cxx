#include "va1taConfig.h"

VaConfig::VaConfig( const char* base_fname, const char* trim_dac_fname, const char* disable_fname ) :
  lineNumber(0),
  trim_dac_lineNumber(0),
  disable_lineNumber(0)
{
  ifstream mapfile(base_fname);
  if( !mapfile) {
    cout<<" file "<<base_fname<<" not found\n";
    exit(EXIT_FAILURE);//failure
  }

  for(int i = 0; i < maxNumVA; i++) // Initialize all the parameters
  {
      FRCaddress[i] 	= -1;
      FRCport[i]   	= -1;
      VA[i] 		= -1;
      VA1TA[i].obi 	= -1;
      VA1TA[i].ibuf	= -1;
      VA1TA[i].pre_bias	= -1;
      VA1TA[i].sbi	= -1;
      VA1TA[i].vrc	= -1;
      VA1TA[i].ifp	= -1;
      VA1TA[i].ifsf	= -1;
      VA1TA[i].ifss	= -1;
      VA1TA[i].sha_bias = -1;
      VA1TA[i].vthr	= -1;
      VA1TA[i].twbi	= -1;
      for( int j = 0; j < 128; j++)
      {
        VA1TA[i].trim_dac[j] = 0;
        VA1TA[i].disable[j] = 0;
      }
      VA1TA[i].test_on	= -1;
      VA1TA[i].r2	= -1;
      VA1TA[i].r1	= -1;
      VA1TA[i].nside	= -1;
      VA1TA[i].tp300	= -1;
  }

  string buffer; 
  while( !getline(mapfile,buffer).eof())
  {
     if(buffer[0] == '#')
     {
       //cout << buffer << endl;
     }
     else
     {
      // cout << buffer << endl;
       stringstream ss;
       ss << buffer;
       ss 	 >> FRCaddress[lineNumber] 
                 >> FRCport[lineNumber]
                 >> VA[lineNumber] 
                 >> VA1TA[lineNumber].obi
                 >> VA1TA[lineNumber].ibuf
                 >> VA1TA[lineNumber].pre_bias
                 >> VA1TA[lineNumber].sbi
                 >> VA1TA[lineNumber].vrc
                 >> VA1TA[lineNumber].ifp
                 >> VA1TA[lineNumber].ifsf
                 >> VA1TA[lineNumber].ifss
                 >> VA1TA[lineNumber].sha_bias
                 >> VA1TA[lineNumber].vthr
                 >> VA1TA[lineNumber].twbi
                 >> VA1TA[lineNumber].test_on
                 >> VA1TA[lineNumber].r2
                 >> VA1TA[lineNumber].r1
                 >> VA1TA[lineNumber].nside
                 >> VA1TA[lineNumber].tp300;

       //printf("See FRCaddr %d, port %d, asic %d\n", FRCaddress[lineNumber], FRCport[lineNumber], VA[lineNumber]);

       lineNumber++;
       if( lineNumber > maxNumVA )
       {
          printf("More line entries than max number of VA chips!\n");
	  break;
       }
     }
  }
  mapfile.close();

  GetTrimDac(trim_dac_fname);
  GetDisable(disable_fname);

}

void VaConfig::GetTrimDac(const char* fname)
{
  ifstream mapfile(fname);
  if( !mapfile) {
    cout<<" file "<<fname<<" not found\n";
    exit(EXIT_FAILURE);//failure
  }

  string buffer;
  while( !getline(mapfile,buffer).eof())
  {
     if( buffer[0] == '#' || buffer[0] == '"' )
     {
       //cout << buffer << endl;
     }
     else
     {
       if( trim_dac_lineNumber > lineNumber ) break;
       stringstream ss;
       ss << buffer;
       
       int dummy;
       ss >> dummy >> dummy >> dummy;      
       //ss >> FRCaddress[trim_dac_lineNumber] >> FRCport[trim_dac_lineNumber] >> VA[trim_dac_lineNumber]; 
       
       for(int i = 0; i < 128; i++)
       {
         ss >> VA1TA[trim_dac_lineNumber].trim_dac[i];
	 //cout << VA1TA[trim_dac_lineNumber].trim_dac[i];
       }
       
       //cout << endl <<  FRCaddress[trim_dac_lineNumber] << endl;
       trim_dac_lineNumber++;
     }
  }

  if( trim_dac_lineNumber != lineNumber )
    {
      cout << "Warning: Base and trim_dac configuration files have different number of entries!!! " << endl;
      //  exit(EXIT_FAILURE);//failure
    }
}

void VaConfig::GetDisable(const char* fname)
{
  ifstream mapfile(fname);
  if( !mapfile) {
    cout<<" file "<<fname<<" not found\n";
    exit(EXIT_FAILURE);//failure
  }

  string buffer;
  while( !getline(mapfile,buffer).eof())
  {
     if( buffer[0] == '#' || buffer[0] == '"' )
     {
       //cout << buffer << endl;
     }
     else
     {
       if( disable_lineNumber > lineNumber ) break;
       stringstream ss;
       ss << buffer;
       
       int dummy;
       ss >> dummy >> dummy >> dummy;      
       //ss >> FRCaddress[trim_dac_lineNumber] >> FRCport[trim_dac_lineNumber] >> VA[trim_dac_lineNumber]; 
       
       for(int i = 0; i < 128; i++)
       {
         ss >> VA1TA[disable_lineNumber].disable[i];
	 //cout << VA1TA[disable_lineNumber].disable[i];
       }
       
       //cout << endl <<  FRCaddress[disable_lineNumber] << endl;
       disable_lineNumber++;
     }
  }

  if( disable_lineNumber != lineNumber )
    {
      cout << "Warning: Base and disable configuration files have different number of entries!!! " << endl;
      //  exit(EXIT_FAILURE);//failure
    }
}

int VaConfig::GetVA1TA(int address, int port, int va, Va1taConfig &va1ta)
{
  for(int i=0;i<maxNumVA;i++)
  {
     //printf("i: %d FRCaddress: %d, FRCport: %d, VA: %d, address: %d, port: %d, va: %d\n",i,FRCaddress[i],FRCport[i],VA[i],address,port,va);
     if( (FRCaddress[i] == address) && (FRCport[i] == port) && (VA[i] == va))
     {
       va1ta = VA1TA[i];
       return 1;
     }
  }
  return 0;
}

int VaConfig::GetHybridConfig(int address, int port, HybridConfig &config)
{
  int complete = 0;
  
  for(int i=0;i<maxNumVA;i++)
  {
    if((FRCaddress[i]==address)&&(FRCport[i]==port))
    {
      config.cfg[VA[i]-1] = VA1TA[i];
      complete++;
      if(complete == 4)
     	return 1;
    }
  }
  return 0;
}

