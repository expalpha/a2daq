#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include "alphaTTC.h"

#define maxNumVA 256

using namespace std;

class VaConfig {
 private:
  int FRCaddress[maxNumVA];
  int FRCport[maxNumVA];
  int VA[maxNumVA];

  int lineNumber;
  int trim_dac_lineNumber;
  int disable_lineNumber;

  Va1taConfig VA1TA[maxNumVA];

  void GetTrimDac(const char*);
  void GetDisable(const char*);

 public:
  VaConfig(const char*, const char*, const char*);
  virtual ~VaConfig( ){};
  
  int GetVA1TA(int,int,int,Va1taConfig &);
  int GetHybridConfig(int,int, HybridConfig &);
};
