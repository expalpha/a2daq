// va1ta_load.cxx
// $Id: testTTC.cxx 1822 2012-09-27 19:41:23Z olchansk $

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <sys/time.h>

#include <string>

#undef HAVE_ROOT
#undef USE_ROOT

#include "midas.h"
#include "mvmestd.h"

#include "alphaTTC.h"
#include "alphaTTCX.h"

HNDLE hDB;
const char* frontend_name = "va1ta_load";

#include "../src/utils.cxx"

int getValue(const char* name, int defaultValue)
{
   char str[256];
   strlcpy(str, "/Equipment/TTC/Settings/VA1TA/", sizeof(str));
   strlcat(str, name, sizeof(str));

   int value = defaultValue;
   int size = sizeof(value);
   int status = db_get_value(hDB, NULL, str, &value, &size, TID_INT, TRUE);
   //printf("status %d\n", status);
   assert(status == SUCCESS);
   return value;
}

#define NUM_FRC 32

int getHybridPresentMask(int ifrc)
{
   int status;

   assert(ifrc >= 0);
   assert(ifrc < NUM_FRC);

   static int once = 1;
   if (once) {
      once = 0;
      odbResizeArray("/Equipment/TTC/Settings/VA1TA/HybridPresentMask", TID_DWORD, NUM_FRC);
   }

   char str[256];
   sprintf(str, "/Equipment/TTC/Settings/VA1TA/HybridPresentMask[%d]", ifrc);

   int value = 0;
   int size = sizeof(value);
   status = db_get_value(hDB, NULL, str, &value, &size, TID_DWORD, FALSE);
   //printf("str %s status %d\n", str, status);
   assert(status == SUCCESS);
   return value;
}

double gettime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 0.000001*tv.tv_usec;
}

int printHybridConfig(const Va1taConfig* cfg)
{
//   int obi;  // 0+3 bits
//   int ibuf; // 3+3 bits
//   int pre_bias; // 6+3 bits
//   int sbi; // 9+3 bits
//   int vrc; // 12+3 bits
//   int ifp; // 15+3 bits
//   int ifsf; // 18+3 bits
//   int ifss; // 21+3 bits
//   int sha_bias; // 24+3 bits
//   int vthr; // 27+5 bits
//   int twbi; // 32+3 bits
//   int trim_dac[128]; // 35+128*4 bits each
//   int disable[128];  // 547+128 bits each
//   int test_on; // 675+1 bit
//   int r2; // 676+1
//   int r1; // 677+1
//   int nside; // 678+1
//   int tp300; // 679+1
// 680 grand total

  printf("obi=%d ibuf=%d pre_bias=%d sbi=%d ifss=%d sha_bias=%d vthr=%2d test_on=%d, disable bits 0 1 2 125 126 127 = %d %d %d %d %d %d",
	 cfg->obi,
	 cfg->ibuf,
	 cfg->pre_bias,
	 cfg->sbi,
	 cfg->ifss,
	 cfg->sha_bias,
	 cfg->vthr,
	 cfg->test_on,
         cfg->disable[0],
         cfg->disable[1],
         cfg->disable[2],
         cfg->disable[125],
         cfg->disable[126],
         cfg->disable[127]
         );

  return 0;
}

int load_all(AlphaTTCX* ttcx, int frcFirst, int frcLast, bool verify_only, int test_on, int vthrn, int vthrp, int ta_enable)
{
   int status = 0;
   int numTTC = ttcx->GetNumTTC();

   bool presentFromOdb = true;
   if (frcFirst >= 0)
      presentFromOdb = false;
   if (frcLast >= 0)
      presentFromOdb = false;
   
   if (frcFirst < 0)
      frcFirst = 0;
   if (frcLast < 0)
      frcLast = 16*numTTC - 1;
   
   int hadErrors = 0;

   //VaConfig* configMap = new VaConfig("../va1taConfigFiles/va1taConfig_base.txt", 
   //                                   "../va1taConfigFiles/va1taConfig_trimDac.txt",
   //                                   "../va1taConfigFiles/va1taConfig_strips.txt");
   
   ttcx->enableTrigIn(false);
   ttcx->enableTrigOut(false);

   std::string okstr;
   std::string errstr;

   Va1taConfig cfg;
   memset(&cfg, 0, sizeof(cfg));

   cfg.obi      = getValue("OBI",  0);
   cfg.ibuf     = getValue("IBUF", 0);
   cfg.pre_bias = getValue("PRE_BIAS", 3);
   cfg.sbi      = getValue("SBI",  3);
   cfg.vrc      = getValue("VRC",  0);
   cfg.ifp      = getValue("IFP",  0);
   cfg.ifsf     = getValue("IFSF", 0);
   cfg.ifss     = getValue("IFSS", 0);
   cfg.sha_bias = getValue("SHA_BIAS", 3);
   cfg.vthr     = getValue("VTHR", 31);
   cfg.twbi     = getValue("TWBI", 0);
   //cfg.trim_dac[128] = 0;
   //cfg.disable[128] = 0;
   cfg.test_on  = test_on;
   cfg.r2       = getValue("R2", 0);
   cfg.r1       = getValue("R1", 0);
   cfg.nside    = 0;
   cfg.tp300    = getValue("TP300", 0);

   //if (vthr >= 0)
   //cfg.vthr = vthr;

   Va1taConfig cfg_nside = cfg;
   Va1taConfig cfg_pside = cfg;

   cfg_nside.ifss = getValue("IFSS_nside", 6);
   cfg_pside.ifss = getValue("IFSS_pside", 2);

   cfg_nside.nside = 1;

   if (vthrn < 0)
      vthrn = getValue("VTHR_nside", 31);

   if (vthrp < 0)
      vthrp = getValue("VTHR_pside", 15);

   if (vthrn >= 0)
      cfg_nside.vthr = vthrn;

   if (vthrp >= 0)
      cfg_pside.vthr = vthrp;

   if (verify_only)
      cm_msg(MINFO, frontend_name, "Verifying VA1TA configuration: test_on: %d, vthrn: %d, vthrp: %d", test_on, vthrn, vthrp);
   else
      cm_msg(MINFO, frontend_name, "Loading VA1TA configuration: test_on: %d, vthrn: %d, vthrp: %d", test_on, vthrn, vthrp);

   int countFrcTotal = 0;
   int countHybridOk = 0;
   int countHybridErrors = 0;

   for (int ifrc=frcFirst; ifrc<=frcLast; ifrc++)
      {
         int isPresentMask = 0xF; // 4 ports on the FRC
         if (presentFromOdb) {
            isPresentMask = getHybridPresentMask(ifrc);
            isPresentMask &= 0xF; // only 4 bits
            if (!isPresentMask)
               continue;
         }

         countFrcTotal ++;

         bool allok = true;
         bool allbad = true;
         std::string xokstr;
         std::string xerrstr;

         if (isPresentMask != 0xF) {
            allok = false;
            allbad = false;
         }
      
         for (int ifrcport=0; ifrcport<4; ifrcport++)
            {
               if ((isPresentMask & (1<<ifrcport)) == 0)
                  continue;

               HybridConfig hybCfg;
               memset(&hybCfg, 0, sizeof(hybCfg));

               hybCfg.cfg[0] = cfg_nside;
               hybCfg.cfg[1] = cfg_nside;
               hybCfg.cfg[2] = cfg_pside;
               hybCfg.cfg[3] = cfg_pside;
         
               if (0) {
                  for (int i=0; i<4; i++) {
                     hybCfg.cfg[i].disable[0] = 1;     // disable=1.enabled=0.
                     hybCfg.cfg[i].disable[1] = 1;     // disable=1.enabled=0.
                     hybCfg.cfg[i].disable[126] = 1;   // disable=1.enabled=0.
                     hybCfg.cfg[i].disable[127] = 1;   // disable=1.enabled=0.
                  }
               }
               
               if (ta_enable >= 0) {
                  for (int i=0; i<4; i++)
                     {
                        for (int j=0; j<128; j++)
                           hybCfg.cfg[i].disable[j] = !ta_enable; // disable=1.enabled=0.
                     }
               }
         
               if (!verify_only)
                  {
                     if (1)
                        {
                           printf("Loading FRC %d, port %d:\n", ifrc, ifrcport);
                           printHybridConfig(&hybCfg.cfg[0]); printf("\n");
                           printHybridConfig(&hybCfg.cfg[1]); printf("\n");
                           printHybridConfig(&hybCfg.cfg[2]); printf("\n");
                           printHybridConfig(&hybCfg.cfg[3]); printf("\n");
                        }
                     
                     char s[265];
                     sprintf(s, "%d-%d ", ifrc, ifrcport);

                     status = ttcx->writeHybridConfig(ifrc, ifrcport, &hybCfg);

                     if (status != 0) {
                        countHybridErrors++;
                        hadErrors ++;
                        xerrstr += s;
                        allok = false;
                     } else {
                        countHybridOk++;
                        xokstr += s;
                        allbad = false;
                     }
                  }
               else
                  {
                     const int kNumBits = 680;

                     char wbits[4][680];
                     memset(wbits, 0, sizeof(wbits));

                     int numBits;

                     // observe that the 4 ASICs are read in reversed order.

                     numBits = AlphaTTC::EncodeBits(kNumBits, &wbits[3][0], hybCfg.cfg[0]);
                     numBits = AlphaTTC::EncodeBits(kNumBits, &wbits[2][0], hybCfg.cfg[1]);
                     numBits = AlphaTTC::EncodeBits(kNumBits, &wbits[1][0], hybCfg.cfg[2]);
                     numBits = AlphaTTC::EncodeBits(kNumBits, &wbits[0][0], hybCfg.cfg[3]);
  
                     char rbits[4][680];
                     memset(rbits, 0, sizeof(rbits));

                     status = ttcx->readConfigurationBits(ifrc, ifrcport, rbits);

                     bool stuck0[4];
                     bool stuck1[4];
                     int  cmp[4];
                  
                     bool allSuccess = true;

                     bool noPower = true;
                     bool noHybrid = true;
                  
                     for (int c=0; c<4; c++)
                        {
                           stuck0[c] = true;
                           stuck1[c] = true;
                           cmp[c] = 0;
                        
                           int rc = c;
                        
                           for (int i=0; i<680; i++)
                              {
                                 stuck0[c] &= (rbits[rc][i] == 0);
                                 stuck1[c] &= (rbits[rc][i] != 0);
                              
                                 if (rbits[rc][i] != wbits[c][i])
                                    {
                                       //printf("chip %d, bit %d, data mismatch: %d should be %d\n", c, i, rbits[rc][i], wbits[c][i]);
                                       cmp[c]++;
                                    }
                              }
                        
                           if (stuck0[c])
                              {
                                 //printf("BOOT ERROR: FRC %d, port %d, chip %d: data is stuck at zero\n", ifrc, ifrcport, c+1);
                                 allSuccess = false;
                              }
                           else if (stuck1[c])
                              {
                                 //printf("BOOT ERROR: FRC %d, port %d, chip %d: data is stuck at one\n", ifrc, ifrcport, c+1);
                                 allSuccess = false;
                              }
                           else if (cmp[c])
                              {
                                 printf("VERIFY ERROR: FRC %d, port %d, chip %d: %d bit errors:\n", ifrc, ifrcport, c+1, cmp[c]);
                                 for (int i=0; i<680; i++)
                                    if (rbits[rc][i] != wbits[c][i])
                                       printf("bit %d, data mismatch: %d should be %d\n", i, rbits[rc][i], wbits[c][i]);
                                 allSuccess = false;
                              }
                           else
                              {
                                 //printf("BOOT SUCCESS: FRC %d, port %d, chip %d.\n", ifrc, ifrcport, c+1);
                              }
                        
                           noHybrid &= stuck1[c];
                           noPower  &= stuck0[c];
                        }
                  
                     if (noHybrid)
                        {
                           printf("VERIFY ERROR:   FRC %d, port %d, all data stuck at 1, FRC turned off or no hybrid connected to this port?\n", ifrc, ifrcport);
                        }
                     else if (noPower)
                        {
                           printf("VERIFY ERROR:   FRC %d, port %d, all data stuck at 0, no power?\n", ifrc, ifrcport);
                        }
                     else if (allSuccess)
                        {
                           printf("VERIFY SUCCESS: FRC %d, port %d.\n", ifrc, ifrcport);
                        }
                     else
                        {
                           for (int c=0; c<4; c++)
                              {
                                 if (stuck0[c])
                                    printf("VERIFY ERROR:   FRC %d, port %d, chip %d: data is stuck at zero\n", ifrc, ifrcport, c+1);
                                 else if (stuck1[c])
                                    printf("VERIFY ERROR:   FRC %d, port %d, chip %d: data is stuck at one\n", ifrc, ifrcport, c+1);
                                 else if (cmp[c])
                                    printf("VERIFY ERROR:   FRC %d, port %d, chip %d: %d bit errors (see above)\n", ifrc, ifrcport, c+1, cmp[c]);
                                 else
                                    printf("VERIFY SUCCESS: FRC %d, port %d, chip %d.\n", ifrc, ifrcport, c+1);
                              }
                        }

                     char s[265];
                     sprintf(s, "%d-%d ", ifrc, ifrcport);

                     if (!allSuccess) {
                        countHybridErrors++;
                        hadErrors ++;
                        xerrstr += s;
                        allok = false;
                     } else {
                        countHybridOk++;
                        xokstr += s;
                        allbad = false;
                     }
                  }
            } // loop over ifrcports
         
         if (allok) {
            char s[256];
            sprintf(s, "%d ", ifrc);
            okstr += s;
         } else if (allbad) {
            char s[256];
            sprintf(s, "%d ", ifrc);
            errstr += s;
         } else {
            okstr += xokstr;
            errstr += xerrstr;
         }
         
      } // loop over ifrc addresses
   
   printf("----------------------------------------\n");
   printf("load_all status:\n");
   printf("ok: %s\n", okstr.c_str());
   printf("errors: %s\n", errstr.c_str());
   printf("----------------------------------------\n");

   if (verify_only) {
      cm_msg(MINFO, frontend_name, "Verified ok: %s", okstr.c_str());
      cm_msg(MINFO, frontend_name, "Verify errors: %s", errstr.c_str());
      cm_msg(MINFO, frontend_name, "Check of VA1TA configuration: Si modules: %d successful, %d errors, FRC: %d total", countHybridOk, countHybridErrors, countFrcTotal);
   } else {
      cm_msg(MINFO, frontend_name, "Loaded ok: %s", okstr.c_str());
      cm_msg(MINFO, frontend_name, "Load errors: %s", errstr.c_str());
      cm_msg(MINFO, frontend_name, "Load of VA1TA configuration: Si modules: %d successful, %d errors, FRC: %d total", countHybridOk, countHybridErrors, countFrcTotal);
   }
      
   return hadErrors;
}

void help()
{
   printf("Help:\n");
   printf(" --help: print this message\n");
   printf("\n");
   printf(" --load: load VA1TA registers from ODB TTC settings\n");
   printf(" --verify: read VA1TA registers and compare against ODB TTC settings\n");
   printf("\n");
   printf(" --ttc1 --ttc2: select TTC1 or TTC2, default is to use both\n");
   printf(" --init: initialize the TTC\n");
   printf(" --status: report TTC status\n");
   printf(" --loadLoop: run in an infinite loop (never exit)\n");
   printf(" --exit: exit after processing all preceeding command line switches\n");
   printf("\n");
   printf(" --test: set VA1TA test_on bit\n");
   printf(" --enableTA: set all VA1TA TA enable bits\n");
   printf(" --disableTA: clear all VA1TA TA enable bits\n");
   printf(" --vthr N: set all TA thresholds to N\n");
   printf(" --vthrp N: set all P-side TA thresholds to N\n");
   printf(" --vthrn N: set all N-side TA thresholds to N\n");
   printf(" --frcRange M N: only operate on FRC addresses from M to N inclusive\n");

   exit(1);
}

int main(int argc, char*argv[])
{
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
  
  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
 
  MVME_INTERFACE *vme = 0;

  int status = mvme_open(&vme,0);
  assert(status == SUCCESS);

  AlphaTTCX* ttcx = new AlphaTTCX();

  if (AlphaTTC::isPresent(vme, 0x41000000))
     ttcx->AddTTC(new AlphaTTC(vme, 0x41000000));

  if (AlphaTTC::isPresent(vme, 0x42000000))
     ttcx->AddTTC(new AlphaTTC(vme, 0x42000000));

  bool doLoad = false;
  bool doVerifyOnly = false;
  bool doLoadLoop = false;
  
  int frcFirst = -1;
  int frcLast  = -1;

  int vthrp = -1;
  int vthrn = -1;
  int test_on = 0;
  int ta_enable = -1;

  if (argc == 1)
     help(); // DOES NOT RETURN

  if (1)
    {
      for (int i=1; i<argc; i++)
        if (0)
          {
            // nothing
          }
        else if (strcmp(argv[i],"--ttc1")==0)
          {
             ttcx->ClearTTC();
             AlphaTTC* ttc = new AlphaTTC(vme, 0x41000000);
             ttcx->AddTTC(ttc);
          }
        else if (strcmp(argv[i],"--ttc2")==0)
          {
             ttcx->ClearTTC();
             AlphaTTC* ttc = new AlphaTTC(vme, 0x42000000);
             ttcx->AddTTC(ttc);
          }
        else if (strcmp(argv[i],"--load")==0)
          {
             doLoad = true;
          }
        else if (strcmp(argv[i],"--verify")==0)
          {
             doVerifyOnly = true;
          }
        else if (strcmp(argv[i],"--test")==0)
          {
            test_on = 1;
          }
        else if (strcmp(argv[i],"--enableTA")==0)
          {
            ta_enable = 1;
          }
        else if (strcmp(argv[i],"--disableTA")==0)
          {
            ta_enable = 0;
          }
        else if (strcmp(argv[i],"--vthr")==0)
          {
            int vthr = strtoul(argv[i+1], NULL, 0);
            printf("Set TA threshold %d\n", vthr);
            i++;

            vthrp = vthr;
            vthrn = vthr;
          }
        else if (strcmp(argv[i],"--vthrp")==0)
          {
            vthrp = strtoul(argv[i+1], NULL, 0);
            printf("Set P-side TA threshold %d\n", vthrp);
            i++;
          }
        else if (strcmp(argv[i],"--vthrn")==0)
          {
            vthrn = strtoul(argv[i+1], NULL, 0);
            printf("Set N-side TA threshold %d\n", vthrn);
            i++;
          }
        else if (strcmp(argv[i],"--frcRange")==0)
          {
            frcFirst = strtoul(argv[i+1], NULL, 0);
            i++;
            frcLast = strtoul(argv[i+1], NULL, 0);
            i++;
            printf("Set FRC address range from %d to %d\n", frcFirst, frcLast);
          }
        else if (strcmp(argv[i],"--init")==0)
          {
            printf("Initialize the TTC\n");
            ttcx->init();
          }
        else if (strcmp(argv[i],"--loadLoop")==0)
          {
            doLoadLoop = true;
          }
        else if (strcmp(argv[i],"--status")==0)
          {
            ttcx->status();
          }
        else if (strcmp(argv[i],"--help")==0)
          {
            help(); // DOES NOT RETURN
          }
        else if (strcmp(argv[i],"--exit")==0)
          {
            exit(0);
          }
        else
          {
            printf("Unknown switch %s\n", argv[i]);
            exit(1);
          }
    }

  ttcx->status();

  if (1)
     {
        // connect to MIDAS

        char host_name[NAME_LENGTH];
        char expt_name[NAME_LENGTH];
        
        host_name[0] = 0;
        expt_name[0] = 0;
        
        char* s = getenv("MIDAS_SERVER_HOST");
        if (s)
           strlcat(host_name, s, sizeof(host_name));
        
        char* e = getenv("MIDAS_EXPT_NAME");
        if (e)
           strlcat(expt_name, e, sizeof(expt_name));
        
        status = cm_connect_experiment(host_name, expt_name, frontend_name, NULL);
        assert(status == SUCCESS);
        
        status = cm_get_experiment_database(&hDB, NULL);
        assert(status == SUCCESS);
     }
        
  while (1) {
     
     load_all(ttcx, frcFirst, frcLast, doVerifyOnly, test_on, vthrn, vthrp, ta_enable);
  
     if (!doLoadLoop)
        break;
  }

  mvme_close(vme);

  cm_disconnect_experiment();

  return 0;
}

/* emacs
 * Local Variables:
 * mode:c++
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

// end
